# Spam filtering

> *What can we learn from spam filtering?  Can we make something useful for it in AxeSMTP?*

There are various indications that an email is spam, and all are probabilistic:

 1. Statistical text analysis, comparison to spam/good databases
 2. SPF, to check direct delivery (can benefit from known-good intermediates)
 3. DKIM, to check a signature by the original domain
 4. Did the sender authenticate?

How these judgements are combined is generally a fixed setting.  Adding some learning logic to how these factors combine may however be beneficial.  Some support for adding new kinds of these "hints" could be possible by adding it as a learning dimension, with initial weights set to something neutral, in the middle of the unsure zone.

## Learning to Combine Hints

It might however benefit from being a simpe AI, that is some matrix multiplication with feedback learning.  The output should have both a spam and good value; neither or both indicate a problem of under- or overlearning, respectively.  Based on this information, earlier phases may need to be trained or untrained, respectively.

The number of layers is determined by the number of hint combinations that are desired.  One layer can combine them linearly, which is already quite good because the inputs can be considered linear.  Adding a second layer enables a few more intermediate patterns; it might help to treat one hint as optional to others, which indeed does seem to add refinement; but this now also calls for random initialisation, which is not necessary for a single layer.

Newly added hints would add one or more inputs to the initial matrix.  These values should be setup with such weights that neutral values are not of influence to the output, preferrably throughout the range.  So, the range defined as spam is the only one to trigger the spam output, and the range defined as good is the only one to trigger the good output, but both can retain the edge that already existed.  Given an initial weight, that would shift the bias to oppose the extra input.  The initial weight may be set to accommodate the numeric ranges and weights of others, or it may be simply set to the reciprocal of the distance between the tipping points.

Upon removal of hint inputs, their flipping-point weight, multiplied by the scale, should be added to the bias.  This remedies the missing information.  This is generally the best idea when an input is absent, because it should not impair the decision too much.  The information is lost however, and it may prove insufficient.

A hints combiner would collect numeric information from predefined email headers, pass them through the hint combiner and decide what to do, including a routing decision, header insertion, message rejection or bouncing.  The headers would do well to mention the hints that were used and the level of both good and spam conclusions.


## Text Analysis with CRM114

We can wrap around the mailreaver.crm script from
[crm114](https://sourceforge.net/projects/crm114/).
This scores an email with a floating point number; spam is < -5.0 and good is > +5.0 by default, and there is no reason to deviate from this.  The range is 10.0, which is the "loudness" of its signaling.

  - The weight for this tool would be 1 / 10.0 = 0.1 initially, with a sign dependent on the direction.
  - The initial weights would be +0.1 for good and -0.1 for spam.
  - The non-good information could raise the good by +5.0 * +0.1, so the bias ought to be 0.5 lower than before.
  - The non-spam information could raise the spam by -5.0 * -0.1, so the bias ought to be 0.5 lower than before.

The destination address guides the CRM114 working directory (holding the `spam.css` and `nonspam.css` files).  For groups, these settings would be shared.  It might in theory be an indea to split spam profiles between aliases, but it also takes more training effort.  In general, it would be useful to only treat an alias separately when the guiding directory has been created.  Likewise, a domain might use a shared directory as a fallback for new users.  The CRM114 toolkit even comes with css utilities to differentiate and merge and distribute ideas of spam among users.


## Sender Analysis with SPF

The SPF idea looks at the direct sender address, and may have problems with forwarding or email lists.  This is a serious restriction.

It is possible to look at the `Received:` headers before it, and whether these translate to a permitted sender for the domain.  If this is the case, the remaining concern is the reliability of the path of delivery.  That path may be a standard path for one's mailbox forwarding, or a subscribed email list.  This can be learnt to be a trustworthy path to locate an originator of an email.

Such paths would be specific to the recipient.  So, when SPF fails to approve the direct mail sender, the delivery path can be tried (one or two `Received:` headers should be more than enough to hit the origin sender) and combined with the recipient to form lookup data in a learning structure.

The output of SPF analysis should be amplified if the SPF records signal being earnest.

Example output:

  - Unknown SPF for the sender could be 0.0
  - When SPF detects proper direct handling, signal +10.0
  - When SPF detects faulty direct handling and it is hard, signal -25.0 or bounce
  - When SPF detects faulty direct handling and it is soft, then
      - if intermediates are known, signal path reliability between -10.0 and +10.0
      - if intermediates are not known, signal -10.0

So that:

  - Do not account for the hard value, it is deliberately out of the linear range
  - Initial weights would be 1 / 20.0 = 0.05 initially (with + or - sign)
  - The initial weights would be +0.05 for good and -0.05 for spam.
  - The non-good information could raise the good by +10 * +0.05, so the bias ought to be 0.5 lower than before.
  - The non-spam information could raise the spam by -10 * -0.05, so the bias ought to be 0.5 lower than before.

This setup can use hard SPF rejection in the incoming MTA and continue with the `Received:` headers in a later phase, precisely because the hard setting is the only one bouncing directly.  When the SPF state is reported in the headers, as is normal for mailbox signaling, it can be processed in an SPF second stage that looks into the delivery path.


## Origin Analysis with DKIM

Like SPF, DKIM also comes in hard and soft flavours.  The status is also commonly reported in headers for internal consumption, also because it may be combined into DMARC policies.

Example output:

  - Unknown DKIM for the sender could be 0.0
  - When DKIM   validates the signature, signal +10.0
  - When DKIM invalidates the signature and it is hard, signal -25.0
  - When DKIM invalidates the signature and it is soft, signal -10.0

So that:

  - Do not account for the hard value, it is deliberately out of the linear range
  - Initial weights would be 1 / 20.0 = 0.05 initially (with + or - sign)
  - The initial weights would be +0.05 for good and -0.05 for spam.
  - The non-good information could raise the good by +10 * +0.05, so the bias ought to be 0.5 lower than before.
  - The non-spam information could raise the spam by -10 * -0.05, so the bias ought to be 0.5 lower than before.


## Sender Authentication

It is possible to use SASL login during SMTP, and this can even be done for port 25.  As it is commonly protected by TLS, the usual precautions apply.

It is not common to do this during crossover between mail domains, but this is precisely why we made Realm Crossover possible.  It is possible for any domain to make a callback to a sender domain for authentication, using a Diameter connection.  We devised a DiaSASL protocol that can be built into any tool depending on this, which then connects to a network-central client for this procedure.

Common MTAs are operated for domains, not users.  So this means that authentication should be any service under a domain.  We would use `+smtp@exmaple.com` for instance.  The pivotal point is that a domain name is supplied.  The authenticated username can be logged, and may possibly be learnt in a spam-protective approach.  In general however, the need for a login should shield off most network abusive practices.  It is also a challenge-response interaction with the sending domain that is part of the normal mail flow, and does not involve user interaction.

Scoring voluntary SMTP login as a spam reduction factor helps to make people adopt it, as it can add much and subtracts nothing.  It may be a way to gently push out spam from a protocol that is too important to have to face it so massively.

