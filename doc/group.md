# Group Iteration and Transactional Reliability

> *When one email is split over many, there are problems assuring the
> transactional properties of email.  There are two solutions, namely
> a queue (the approach of an MTA) or reporting back over individual
> recipients (the approach of LMTP).  The LMTP approach keeps filters
> simple by pushing transactional responsibility back on the sender.*

There are several levels for delivery iteration:

  * One `MAIL FROM` sender may request multiple `RCPT TO` desintation
    addresses.  These can each be answered separately.  The degree of
    control is limited; one may use a 450 error to request a later new
    attempt of delivery, which incurs a delay, but one cannot say a
    thing like "try this address separately".  Ideally, all the
    destination addresses should always be provided.

  * One `RCPT TO` with an ARPA2 Group as a destination address unfolds
    to multiple recipients.  Some of these might fail delivery, and
    that would need to be bounced back to the sender.  The easy way is
    to push this back over LMTP, a more advanced whay is to collect
    the addresses into a sender report or (collected) error report
    that lists the failed recipients and their individual reasons.


## LMTP Example

The LMTP protocol reports back for every single recipient, and this
allows the MTA to tick off some, but possibly not all delivery
addresses.  It means that the LMTP protocol can make separate
delivery choices.  LMTP was designed for local delivery to mailboxes,
but it is quite possible to deliver to a filter and have it inject
email back into the MTA over LMTP or SMTP.

Following example is taken from
[RFC2033](https://datatracker.ietf.org/doc/html/rfc2033)
which defines LMTP:

```
   S: 220 foo.edu LMTP server ready
   C: LHLO foo.edu
   S: 250-foo.edu
   S: 250-PIPELINING
   S: 250 SIZE
   C: MAIL FROM:<chris@bar.com>
   S: 250 OK
   C: RCPT TO:<pat@foo.edu>
   S: 250 OK
   C: RCPT TO:<jones@foo.edu>
   S: 550 No such user here
   C: RCPT TO:<green@foo.edu>
   S: 250 OK
   C: DATA
   S: 354 Start mail input; end with <CRLF>.<CRLF>
   C: Blah blah blah...
   C: ...etc. etc. etc.
   C: .
   S: 250 OK
   S: 452 <green@foo.edu> is temporarily over quota
   C: QUIT
   S: 221 foo.edu closing connection)
```

The difference lies in the response to the `DATA` command,
which iterates over the `RCPT TO` lines.  As the RFC says,

> The change is that after the final ".", the server returns one reply
> for each previously successful RCPT command in the mail transaction,
> in the order that the RCPT commands were issued.  Even if there were
> multiple successful RCPT commands giving the same forward-path, there
> must be one reply for each successful RCPT command.

Note how this does not help with Group Iteration, because the reported
lines indicate the destinations offered by the sending MTA in the form
of `RCPT TO` lines, not in the form of eventual recipients.  A direct
response is therefore needed (and may be an error log entry or a bounce
message listing failed deliveries to group members).

It may be possible to use a "maildrop" concept to trigger bounces from
an MTA, so by sending traffic via a program such as `sendmail` on the
commandline, but that is also likely to reveal delivery addresses for
otherwise private members in error-mail bodies, where they are difficult
to replace.


## One Group Policy

The `-group-` daemon runs inside an MTA, and so it can use LMTP.  It
is not a stretch to consider ARPA2 Groups a local delivery facility.
This does not change because email happens to loop back in after
Group Iteration.  The use of "maildrop" facilities is not a problem
either, though a new Message-Id might then be desired.

The simplest approach is when only one group is treated at a time,
namely the group of the sender.  So, `RCPT TO` is a member address,
meaning a group with one alias added for the member, and any
`RCPT TO` adresses should start with the same group.  This is the
"one-group policy", and it greatly simplifies matters.

  * Recipients with another pattern can be rejected, deferred or
    might be siphoned off via a non-group delivery channel.

  * The `-in-` daemon replaces the sender address with an
    Actor Identity while delivering to a group.  Every group uses
    a different sender identity.  This causes a split between
    email to different groups, as well as between group email and
    non-group email.  An exception to the latter might be when
    the sender is a group member who sends to addresses outside
    of the group, which could be easily detected and siphoned
    into a non-group delivery channel.  Cross-posting would not
    cause problems, because those would cause separate `MAIL FROM`
    addresses and so be delivered separately.

  * An email configuration may define "transport" drivers for the
    addresses that constitute a group, and deliver only those to
    the `-group-` daemon.  This would be useful when the `-in-`
    daemon is not used.

Using the `-in-` filter before the `-group-` involves on integral
configuration via `a2rule`, and saves mail-specific configuration
in the MTA.


## Group Member Delivery Failures

What happens when delivery to a group member fails?  That is a
matter of semantics, or what the group means.  If it is intended
as guaranteed delivery to multiple individuals, it would call
for bounces.  When it is considered a discussion group for those
who are interested, then members may subscribe and unsubscribe
at will, and a mailing list generally unsubscribes those members
that are bouncing consistently.  In other words, it is the
member's responsibility to pickup on email.  To guarantee delivery,
it might help to store email, in an IMAP account that can be
easily seached, so recipients can retrieve missed email at any
later time.

Under the first semantics, bounce emails would need to be sent
for those recipients that failed to take in delivery.  With the
second semantics, a report would be prudent when all delivery
failed.


## Backend Connections

The backends to the `-group-` daemon cover two purposes:

  * Delivery to non-group destinations.  This may require
    different backend processing, for instance via the
    `-out-` daemon that is undesirable for group members.
    (Perhaps not for other sender Actor Identities either.)
    When this backend is not setup, such destinations
    would be rejected.

  * Delivery to group members.  Under the assumption that
    the `MAIL FROM` sender address is set to a member of
    the destination group (as a result of an Access Rule)
    there will only be one group to deliver to.  Different
    connections may still deliver to different groups,
    but given a connection there would be one group only.

Generally, email for non-group destinations can be
recognised when at least one of these conditions holds:

  * A `RCPT TO` destination address does not hold the same
    group address as the `MAIL FROM` sender address.  The
    sender is the group with one member alias, whereas the
    destination is the group with zero or more aliases, to
    be interpreted as a filtering with `group_iterate()`.

  * An attempt to `group_iterate()` delivers to no recipients
    at all.  This is less-than-perfect; it would be more
    informative if `group_iterate()` reported an error for
    not having found the group, because members may be
    counted but several may block delivery.  The difference
    is that a non-group relays as a non-group, while a
    non-listening group should send back an error response.


## Reliable Testing

Another call was added to ARPA2 Common to break the problem.
During `MAIL FROM`, it is now possible to test if the sender,
parsed as an Actor Identity with 1 alias level, is registered
as a group and supports Group Iteration.  At the same time,
this check assures early on that the sender is indeed a
Group Member.

After this, the splitting of `RCPT TO` over the member and
plain backends is straightforward.  Everything meant for
the plain backend is passed on directly, and receives the
fully reliable feedback that SMTP offers.  LMTP might add
only very little here.  Everything meant for the member
backend is stacked up, and rewound during Group Iteration.
Complete inability to deliver, as well as finding no
addresses to deliver to, are now reported as errors, but
partial failures on individual delivery addresses are
not.  This is the weaker semantics for group delivery.

It may still be a useful idea to (later) add an email
that reports on inability to deliver to group members,
as that helps the self-control over the group by members
instead of an admin.  Until this is implemented, only a
log entry about failed delivery will be made, leaving
this work to postmaster or the recipient who notices
missing email.  This may however lead to many attempts
at failing delivery, without an end in ight.

