# AxeSMTP tools and Postfix

The Postfix MTA can be used together with AxeSMTP, namely as a
[Before-Queue Content Filter](http://www.postfix.org/SMTPD_PROXY_README.html)
or as an
[After-Queue Content Filter](http://www.postfix.org/FILTER_README.html).
Which is the most apt varies with the application.


## AxeSMTP arpa2in

This before-queue filter does the following:

  * MAIL FROM is parsed as a Remote Identity, RCPT TO is an ARPA2 Identity
  * RCPT TO may hold a signature pattern; if it does, it is validated
  * The combination of MAIL FROM and RCPT TO is subject to Communication Access
  * The MAIL FROM address may be replaced with an Actor Identity
  * **TODO:** It would be useful to also map addresses in the email in passing

The use of signatures and aliases all assume that
[Postfix handles aliases](https://fvdm.com/code/plus-address-forwarding-in-postfix)
by redirecting to the beginning of the email address.  The receiving IMAP daemon
is assumed to do that to; for instance Dovecot can do this.


## AxeSMTP arpa2out

This after-queue filter does the following:

  * MAIL FROM is parsed as an ARPA2 Identity, RCPT TO is a Remote Identity
  * MAIL FROM may hold a signature recipe or pattern; if it does, it is re-signed
  * **TODO:** A receiving Actor Identity might be 1:1 mapped to the Remote Identity
  * **TODO:** It would be useful to also map addresses in the email in passing

This work will usually be done on the way out, but before transport signatures
such as DKIM are attached to the email.  It should only follow for submission port
handling, but when email relaying is avoided this may be done on all outgoing mail.

Email sent between local user would pass through `arpa2out` and back in through
`arpa2in`, so as to avoid the complexities of separate configuration for relaying.


## AxeSMTP arpa2group

This after-queue filter does the following:

  * MAIL FROM is tested to be a group member, and otherwise the mail passes through
  * After removal of the last alias, a group member address is a group address
  * The group record allows iteration over group members and their delivery addresses
  * Delivery addresses are used as RCPT TO, while MAIL FROM remains a member
  * Do not sequence through `arpa2out` if it maps Actor Identity to Remote Identity

The `arpa2group` filter may benefit from some, but not all current filtering; it
benefits from added signatures but not from another mapping of destination
addresses.  

Since the sender in MAIL FROM is a member's Actor Identity, even when it delivers
to a foreign domain, many things simplify.  DKIM signatures and SPF checks will
not fail on these emails.  Simplistic forwarding of email to another domain will
continue to fail if the secondary address validates either, but that can be
remedied with an Actor Identity that maps the forwarded address to one that
sits under the relaying mail server domain.  (**TODO:** Consider a special
AxeSMTP tool for that.)

