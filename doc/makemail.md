# The makemail.cgi script

> *Get an access-constrained email address, and use it to
> allow only certain classes of incoming traffic.*

This is based on:

  * [Signed ARPA2 Identity](http://common.arpa2.net/md_doc_IDENTITY.html)
  * [`A2ID_SIGDATA_xxx` bits](https://gitlab.com/arpa2/arpa2common/-/blob/master/include/arpa2/identity.h)
  * [AxeSMTP input filtering](https://gitlab.com/arpa2/AxeSMTP/-/blob/master/README.MD#axe-smtp-input-filtering)

## How to make it work

First, assume an email address (possibly forwarded) on which you
can receive email.

Second, impose on it a front-end filter.  Require signing in
the ACL so the filter cannot possibly be dropped.

Third, setup variations in HTTP script parameters, where absense or
empty strings cause no constraint of this kind:

  * `?mailbox=userid` defines the incoming mailbox (possibly append `@domain.name`)
  * `&sender=domain.name` restricts the sender address (possibly prepend `userid@`)
  * `&days=30` for a number of days from now
  * `&topic=news` for things like a `[news]` topic in email **TODO**
  * `&subject=How+to+learn+the+quickstep` for a human email topic **TODO**
  * `&sessionid=az8123kz90` with a session identifier for the communication **TODO**

Fourth, a web page producing:

  * Signed email address in a textbox, with a `Copy to Clipboard`
    button next to it.

Fifth, a web form that offers:

  * Fields for editing HTTP script parameters
  * A submission button to `Update` the page

## Authentication Required

These are interesting websites to target in spam!
Use authentication to grant access.
*And please refrain from public service with an are-you-a-bot checks;
instead, let's grow up and host our own infrastructure.*

The authentication is assumed to deliver an
[ARPA2 Identity](http://common.arpa2.net/md_doc_IDENTITY.html)
in the `REMOTE_USER` environment variable.

## JSON output

Usually, this program produces HTML in response to `text/html` or `text/*` inquiries.
This value should be set in the `HTTP_ACCEPT` environment variable, usually derived
from the `Content-Type` HTTP header.

When the `HTTP_ACCEPT` is set to `application/json`, this program returns a JSON
object.  Errors are reported as `null` (and HTTP status code 500) and successful
output consists of a structure with the desired variables.

```
{
   "days": 30,
   "recipient": "john+XXXYYYZZZ+@example.com"
}
```

