# Form Mail in the ARPA2 style

> *There are many form mailers that have been used on the web.
> Most of them are spam magnets.  ARPA2 may change this problem,
> and bring back email forms for easy mail/web integration.*

Web-based form-to-mail transmitters are simple, but they usually
attracts spam, which has caused more cluttered interfaces to "prove"
that the sender is a human being.

The sender address is even less verifiable than for plain email,
so it can easily be sent from forged addresses (or addresses may 
accidentally be filled in wrongly).

ARPA2 introduces authentication to websites, including Realm Crossover,
so it is possible to trust the sender to a much better degree.  The
authenticated identity of a sender helps to make Access Control decisions.
This even allows shielding of protected groups, or fending off known
offenders.


## FastCGI Environment Variables

There are a few variables that need to be passed over FastCGI:

  * `REMOTE_USER` is required.  This is the identity of the authenticated
     web user, which will be used as a sender email address.  It takes the
     form of an ARPA2 Remote Identity.  This means that no special meaning
     is assigned to the local-part but that an `@` is required, which must
     be followed by a fully qualified domain name.

  * `SERVER_NAME` is required.  It is the web server hostname.  It is
    used in the `Received:` email header.

  * `QUERY_STRING` is required.  It is described below.

  * `ARPA2_DOMAIN` is required.  It is set to the domain for which
    the email is destined.  This is difficult to derive from host names
    of a web or mail server, but it is trivial to setup while configuring.
    *Note that this is not a standard CGI variable.*


## Query Parameters in the URL

The web form submits with the GET method, and supplies the following
form fields in a notation like `?mailbox=...&subject=...&body=...` where

  * `mailbox` is required.  It is the local-part of the user being
    sent to.  The domain for the recipient is taken from the
    `ARPA2_DOMAIN` FastCGI variable.

  * `access` is optional.  It is a string of digits and uppercase letters
    that will be surrounded by `+` characters and appended to the mailbox
    before the `@` and domain follow.  If present, it can be used for
    Access Control.

  * `subject` is required.  It is used for the `Subject:` email header.

  * `topic` is optional.  It may be surrounded by square brackets and
    prefixed to the `Subject:` email header (with a separating space).
    This can be used to select a topic from a menu and further direct
    the email to a more specific recipient.

  * `body` is required.  It contains the email body in plain text.


## Test Run

It is useful to see a simple test run for this program and later connect
that to the mail server.

Set the following environment variables:

```
export ARPA2_DOMAIN=example.com
export REMOTE_USER=mary@example.net
export SERVER_NAME=web0.example.com
export QUERY_STRING='mailbox=john&subject=Some+new+idea&body=WowThisIsGreat'
```

Then run `nc -l ::1 25` in another terminal and start `formmail.fcgi`.
The other terminal will receive (and send) the following:

```
220 Welcome
EHLO example.com
200 Hello
MAIL FROM:<mary@example.net>
200 Ok
RCPT TO:<john@example.com>
200 Ok
DATA
350 Proceed
Received: from web0.example.com by myhostname for example.com
From: WebForm Mailer <mary@example.net>
To: <john@example.com>
Subject: Some new idea

WowThisIsGreat
.
200 Ok
QUIT
250 Ok
```

A somewhat larger test replaces the query string with

```
export QUERY_STRING='mailbox=john&subject=Some+new+idea&body=WowThisIsGreat&topic=patents&access=B4WEFO46JXW2AVZ77N7CFCEKZQEAZCZMXFVGKDUZRURMZIQ4RM5I4NMMTW'
```

Restarting `nc -l ::1 25` on the other terminal, it will give

```
220 Welcome
EHLO example.com
200 Hello
MAIL FROM:<mary@example.net>
200 Ok
RCPT TO:<john+B4WEFO46JXW2AVZ77N7CFCEKZQEAZCZMXFVGKDUZRURMZIQ4RM5I4NMMTW+@example.com>
200 Ok
DATA
350 Proceed
Received: from web0.example.com by myhostname for example.com
From: WebForm Mailer <mary@example.net>
To: <john+B4WEFO46JXW2AVZ77N7CFCEKZQEAZCZMXFVGKDUZRURMZIQ4RM5I4NMMTW+@example.com>
Subject: [patents] Some new idea

WowThisIsGreat
.
200 Ok
QUIT
250 Bye-bye
```

**Tip:**
You can quickly respond to any command with (a looped version of)

```
yes 200 ok | nc -l ::1 25
```

More advanced (test) scenarios can be constructed with
[Pavlov](https://gitlab.com/arpa2/pavlov)
which supports more general stimulus-response pattern sequences.


## Mail Server Requirements

The mail server is expected to run on address ::1 and TCP port 25.
It should not enforce TLS on that port.  It should not bypass ARPA2 Access Control.

Of course, SMTP compliance is assumed, and served by `formmail.fcgi` but
to be on the safe side, it also enforces 30 second timeouts for both sending
to and receiving from the MTA.  This is exercised for each send and each
receive, so a downright slow MTA might take minutes before the user receives
timeout failure.  This is not remedied because such MTA slowness is uncommon.


## Error Handling

There are a few classes of errors, reported differently.

**Success.**
This is reported when the email submission worked well.  Its JSON representation is
marked by an `ok` key with `true` value,

```
{
  "ok": true
}
```

**Web failure.**
These are caused by the way the browser approaches the `formmail.fcgi` component.
That is, there may be user-caused problems which they can fix.  All errors report
`false` for the `ok` key and add keys `error` and `time` with a description and
timestamp.
An example is

```
{
  "ok": false,
  "error": "Some required parameters missing; need ?mailbox=...&subject=...&body=...",
  "time": "Wed 2022-04-20 around 16:02:49"
}
```

**Backend failure.**
The connection to the MTA may be failing, and this kind of information provides
some idea of what is wrong and why things are not working, without allowing any
change by the web client.  There may however be reason to contact an administrator.
An example is

```
{
  "ok": false,
  "error": "Failed to connect to MTA",
  "time": "Wed 2022-04-20 around 16:03:21"
}
```


**Configuration error.**
These errors should not occur in a properly configured system.  They report in
log files (either on the web server or mail server) and it may be useful to
match for values nearby the `time` reported in the error.  Since this kind of
action is well beyond the web client's reach, the only suggestion produced is
to contact an administrator.
The typical form is

```
{
  "ok": false,
  "error": "Configuration Error (the webmaster may help if you tell them the time)",
  "time": "Wed 2022-04-20 around 16:06:47"
}
```

The output is always a JSON value, and the `ok` key is always present to help
quickly detect success or failure.  The `error` and `time` keys are helpful
in problem solving, where the `error` is intended for consumption by the
user or administrator.  It is strongly suggested to report `time` alongside
for reporting to administrators.

