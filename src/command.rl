/* command.rl -- SMTP grammar and parsing of commandlines
 *
 * This Ragel source file parses command lines, including addresses.
 * It can iterate over parameters.  The non-terminals below are named
 * as in the RFC literature, but some receive a broader interpretation
 * to avoid building a full SMTP implementation.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl>
 */


#include <stdbool.h>
#include <string.h>

#include <arpa2/except.h>
#include <arpa2/identity.h>
#include <arpa2/quick-mem.h>

#include "axe.h"


%%{
machine smtp;


# Formal CR-LF and leniency to allow preceding space, allow lone LF
#
# The formal CR-LF is required for DATA end parsing, commands may
# opt for leniency if this could not be non-deterministic.  It is
# probably best to not be lenient with DATA, so as to remind the
# user of the requirement to end line properly.
#
cr_lf = '\r' . '\n' ;
cr_lf_lenient = ' '? . '\r'? . '\n' ;

# Very lenient definition of a domain
#
domain = [a-zA-Z0-9\-.]+ ;

# Arbitrary string without space or CR or LF
#
# This _may_ be an address, so it makes sense to try parsing it
# without fussing over the response, just passing success.
#
string = [^ \r\n]+ ;

# Mailbox has a rather lenient (and vague!) definition...
#
mailbox = [^<>]+ & graph+ ;

# Path may start with source route, which is to be ignored
#
path = '<' . ( '@' . domain . ':' )?
		. mailbox
			>{ addrbuf = p; }
			%{ addrlen = p - addrbuf; }
		. '>' ;

# Reverse paths may be denied by the sender (e.g. error mail)
#
reverse_path = "<>" | path ;

# Forward paths are required to be able to route the mail
#
forward_path = path ;

# ESMTP parameters can be supplied as a whole, or iterated over
#
# The opt_esmtp_parameters matches for MAIL FROM and RCPT TO
# and starts with the space that makes it exist.
#
esmtp_keyword = [a-zA-Z0-9] . [a-zA-Z0-9\-]* ;
esmtp_value = ( 33..60 | 62..126 )+ ;
esmtp_param = esmtp_keyword ( "=" . esmtp_value ) ;
#
opt_esmtp_parameters = ( " " . esmtp_param )*
	>{ front->param0 = p; } ;


# HELO command, RFC 5321
#
# In handling this, we will want to orchestrate our own answer.
# This means that we need not connect to those backends yet.
#
helo = "HELO "i . domain . cr_lf_lenient
	%{
		if (front->got_helo_1st != '\x00') {
			resp = "503 You already introduced yourself\r\n";
		} else {
			bool ok = true;
			if (front->conf->cb_helo != NULL) {
				ok = front->conf->cb_helo (front, addrbuf, addrlen);
			}
			if (ok) {
				resp = "250 axe-smtp.example.com\r\n";
				front->got_helo_1st = 'H';
			}
		}
	} ;

# EHLO command, RFC 5321
#
# In handling this, we will want to orchestrate our own answer.
# This frees us from combining the results from multiple backends.
# It also means that we need not connect to those backends yet.
# We probably don't want to include PIPELINING, but should be open
# to 8BITMIME and SMTPUTP8, for instance.
#
ehlo = "EHLO "i . domain . cr_lf_lenient
	%{
		if (front->got_helo_1st != '\x00') {
			resp = "503 You already introduced yourself\r\n";
		} else if (!mem_alloc (front, front->cmdlen, (void **) &front->ehloptr)) {
			resp = "452 Out of memory trying to store EHLO\r\n";
		} else {
			front->ehlolen = front->cmdlen;
			memcpy (front->ehloptr, front->cmdbuf, front->ehlolen);
			bool ok = true;
			if (front->conf->cb_ehlo != NULL) {
				ok = front->conf->cb_ehlo (front, addrbuf, addrlen);
			}
			if (ok) {
				resp = "250 axe-smtp.example.com\r\n";
				front->got_helo_1st = 'E';
			}
		}
	} ;

# LHLO command, RFC 2033
lhlo = "LHLO "i . domain . cr_lf_lenient
	%{
		resp = "502 This is not an LMTP server\r\n";
		   /* front->got_helo_1st = 'L'; */
	} ;

# MAIL FROM command, RFC 5321
#
mail_from = "MAIL FROM:"i . reverse_path . opt_esmtp_parameters . cr_lf_lenient
	%{
		if (front->got_helo_1st == '\x00') {
			//
			// We did not see HELO, EHLO or LHLO yet
			resp = "503 Polite servers say hello first\r\n";
		} else if (front->got_mail_from) {
			//
			// We already parsed a MAIL FROM command
			resp = "503 One sender will suffice\r\n";
		} else {
			//
			// Default handling: Address storage into front->from
			bool ok = true;
			if (front->conf->cb_mail_from != NULL) {
				//
				// The callback handles parsing
				ok = front->conf->cb_mail_from (front, addrbuf, addrlen);
			} else if (addrbuf == NULL) {
				memset (&front->from, 0, sizeof (front->from));
			} else {
				//
				// Default to laid-back ARPA2 Remote Identity parsing
				ok = a2id_parse_remote (&front->from, addrbuf, addrlen);
				if (!ok) {
					resp = "553 Sender must parse as an ARPA2 Remote Identity\r\n";
				}
			}
			if (ok) {
				//
				// We got a valid or intentionally invalid ARPA2 Identity
				resp = "205 OK\r\n";
				front->got_mail_from = true;
			}
		}
	} ;

# RCPT TO command, RFC 5321
#
rcpt_to = "RCPT TO:"i . forward_path . opt_esmtp_parameters . cr_lf_lenient
	%{
		a2id_t rcptid;
		if (!front->got_mail_from) {
			//
			// We did not see MAIL FROM yet
			resp = "503 You did not send a MAIL FROM command yet\r\n";
		} else {
			bool ok = true;
			if (front->conf->cb_rcpt_to != NULL) {
				ok = front->conf->cb_rcpt_to (front, addrbuf, addrlen);
			} else {
				ok = a2id_parse_remote (&rcptid, addrbuf, addrlen);
				if (!ok) {
					resp = "553 Recipient must parse as an ARPA2 Remote Identity\r\n";
				}
			}
			//TODO// Add or share a backend, send it [EHLO, MAIL], RCPT
			if (ok) {
				front->got_rcpt_to = true;
				resp = "250 OK\r\n";
			}
		}
	} ;

# DATA content, RFC 5321
#
# The trailing sequence breaks out of the data with expression priority.
# The :>> places a higher priority on the last transition of the second
# expression, causing a break out of the first expression which can handle
# all the other forms of data.
#
# data_content := any* :>> cr_lf . "." . cr_lf
# 	@{ front->intention_data = false; } ;
#
data_content_end = cr_lf . "." . cr_lf
	;
smtp_header = ( [a-zA-Z0-9\-_]+ . ":" . [^\r\n]* . ( cr_lf . [ \t] . [^\r\n]* )* . cr_lf )
	>{
		front->data_header = p;
	}
	%{
		if (front->conf->mailhdr_callbacks != NULL) {
			front_process_header (front, front->data_header, ((unsigned) (p - front->data_header)), true);
			front->data_header = NULL;
		}
	};
smtp_ignored_line = [^a-zA-Z0-9\-_.\r\n] . [^\n]* . cr_lf_lenient
	%{ log_debug ("%s", "DATA CONTENT: smtp_ignored_line"); }
	#TODO# We might signal rejection if we can disconnect from backends without "\r\n.\r\n"
	;
smtp_empty_line = cr_lf
	>{
		front->data_header = p;
	}
	%{
		if (front->conf->mailhdr_callbacks != NULL) {
			front_process_header (front, front->data_header, 2, true);
			front->data_header = NULL;
		}
		front->intention_body = false;
		front->data_split = p;
	};
smtp_content = ((smtp_ignored_line -- smtp_header) | smtp_header )* . smtp_empty_line . any*
	;
data_content := ( smtp_content -- data_content_end ) . data_content_end
	@{ front->intention_data = false; } ;

# DATA command, RFC 5321
#
data = "DATA"i . cr_lf
	%{
		resp = "354 Start mail input; end with <CRLF>.<CRLF>\r\n";
		front->intention_data = true;
		front->intention_body = false;
		front->ragel_cs = smtp_en_data_content;
		bool ok = true;
		if (front->conf->cb_data != NULL) {
			ok = front->conf->cb_data (front);
		} else {
			static const char *datacmd = "DATA\r\n";
			ok = back_send_cmd_all (EV_DEFAULT_ front, NULL, (uint8_t *) datacmd, strlen (datacmd));
		}
		if (!ok) {
			front->intention_data = false;
			resp = NULL;
		}
	} ;

# RSET command, RFC 5321
#TODO# Abort all backends with RSET, collect errors, done if all OK
#
rset = "RSET"i . cr_lf_lenient
	%{ front->got_mail_from = false;
	   front->got_rcpt_to   = false;
	   front->got_helo_1st  = '\x00';
	   front->backends = NULL;
	   resp = "250 OK\r\n"; } ;

# VRFY command, RFC 5321
#
vrfy = "VRFY "i . string . cr_lf_lenient
	%{ resp = "550 Access Denied for VRFY\r\n"; } ;

# EXPN command, RFC 5321
#
expn = "EXPN "i . string . cr_lf_lenient
	%{ resp = "550 Access Denied for EXPN\r\n"; } ;

# HELP command, RFC 5321
#
help = "HELP"i . ( " " . string )? . cr_lf_lenient
	%{ resp = "214 This is an AxeSMTP service\r\n"; } ;

# NOOP command, RFC 5321
#
noop = "NOOP"i . ( " " . string )? . cr_lf_lenient
	%{ resp = "250 OK\r\n"; } ;

# QUIT command, RFC 5321
#
quit = "QUIT"i . cr_lf_lenient
	%{
		static const char *quitcmd = "QUIT\r\n";
		(void) back_send_cmd_all (EV_DEFAULT_ front, NULL, (uint8_t *) quitcmd, strlen (quitcmd));
		resp = "221 axe-smtp.example.com Service closing transmission channel\r\n";
		front->intention_done = true;
	} ;

# BDAT command, RFC 3030
bdat = "BDAT "i . digit+ . " LAST"i ? . cr_lf_lenient
	%{ resp = "502 Chunking BDAT submission not supported\r\n"; } ;


# SMTP protocol command line
#
smtp_command := helo | ehlo | lhlo | mail_from | rcpt_to | data | bdat | rset | vrfy | expn | help | noop | quit ;


# Reply code
reply_code = [2-5][0-5][0-9] ;

# Reply text
reply_text = [\ -~\t]+ ;


# Reply line
reply_pre = reply_code . '-' . reply_text . cr_lf_lenient
	@{ cb_replyline (back, p+1, false); } ;
#
reply_end = reply_code . ' ' . reply_text . cr_lf_lenient
	@{ cb_replyline (back, p+1, true );
	   seenlast = true; } ;


# SMTP protocol response callbacks
smtp_response := reply_pre* . reply_end ;


}%%

%% write data ;


/** @brief Parse an SMTP command line and invoke the proper callback.
 *
 * @param front SMTP server control structure, doubles as memory pool.
 * @param cb Callback structure for the various commands.
 * @returns true on success, false with an error setup in cmdbuf on error.
 */
bool command_parse_callback (struct smtp_front *front) {
	//
	// Reset the iterator for command parameters
	front->param0 = NULL;
	//
	// Parameter for other state diagrams
	struct smtp_back *back = NULL;
	bool seenlast = false;
	cb_replyline_t *cb_replyline = NULL;
	//
	// Check the command length: RFC 5321 Section 4.5.3.1.4
	if (front->cmdlen > 512) {
		strcpy (front->cmdbuf, "500 Command Too Long\r\n");
		goto respond;
	}
	//
	// Run the Ragel parser for the SMTP machine
	int cs = smtp_en_smtp_command;
	char *p  = front->cmdbuf;
	const char *pe = p + front->cmdlen;
	const char *eof = pe;
	const char *resp = NULL;
	const char *addrbuf = NULL;
	int addrlen = 0;
	%%{
		write init nocs ;
		write exec ;
	}%%
	//
	// Process the response for an already-signaled error
	if (resp != NULL) {
		// Assume that the error string was setup in front->cmdbuf
		strcpy (front->cmdbuf, resp);
		goto respond;
	}
	//
	// Process incomplete parsing by Ragel
	if ((cs < smtp_first_final) || (p != pe)) {
		strcpy (front->cmdbuf, "500 Command Not Recognised\r\n");
		goto respond;
	}
	//
	// Return successful completion
	return true;
	//
	// Return the response that was stored in front->cmdbuf
respond:
	return (*front->cmdbuf == '2');
}


/** @brief Iterate over zero or more ESMTP command parameters.
 *
 * Given an ESMPT command parameter structure, use this function to
 * iterate over the various KEYWORD=VALUE pairs.  The iterator holds
 * internal fields for iteration, but when this call succceeds there
 * will be values setup in kwbuf, kwbuf for the keyword and valbuf,
 * vallen for the corresponding value; kwbuf and valbuf point into
 * the command line and kwlen and vallen gives the respective length.
 *
 * This routine assumes being setup by command_parse_callback() and
 * being used within the callback invocation made from it.
 *
 * @param front SMTP server with command that may have parameters.
 * @returns true when a new item was found, false if not.
 */
bool command_parameter_next (struct smtp_front *front) {
	//
	// Test if (another) parameter exists
	if (front->param0 == NULL) {
		return false;
	}
	if (*front->param0 == ' ') {
		return false;
	}
	//
	// Setup the kwbuf, valbuf and new param0
	front->kwbuf  = front->param0 + 1;
	front->valbuf = strchr (front->param0, '=') + 1;
	front->param0 = strspn (front->valbuf, " \r\n") + front->valbuf;
	//
	// Compute the kwlen and vallen
	front->kwlen  = front->valbuf - front->kwbuf - 1;
	front->vallen = front->param0 - front->valbuf;
	//
	// Return success
	return true;
}


/** @brief Parse and pass DATA content, up to and including "\r\n.\r\n".
 *
 * When the final part has been processed, it resets front->intention_data
 * that was set in response to the DATA command.  This is an incremental
 * processor, with front->ragel_cs keeping state between calls.  This is
 * also setup in response to the DATA command.
 *
 * The DATA portion is split into the SMTP header (with an empty line)
 * and body poitions.  The intention_body is set for the latter.
 * The value front->data_split is positioned after the empty line that
 * splits SMTP headers from the body; when headers continue, it is set
 * at the end of the data buffer; when they were processed before, it
 * is set at the start of the data buffer.
 *
 * @param front SMTP server control structure.
 * @param cb Callback structure, including the content data routine.
 * @returns true on success, false with error setup in cmdbuf on error.
 */
bool command_data_content (struct smtp_front *front) {
	//
	// Variables for other state diagrams
	struct smtp_back *back = NULL;
	bool seenlast = false;
	cb_replyline_t *cb_replyline = NULL;
	//
	// Continue header data; partial data was delivered in previous call
	if (front->intention_body) {
		front->data_split = 0;
	} else {
		front->data_split = front->datalen;
		if (front->data_header != NULL) {
			front->data_header = front->databuf;
		}
	}
	//
	// Parse data content
	log_debug ("Parsing data content \"%.*s\" for CR-LF-DOT-CR-LF from state %d", front->datalen, front->databuf, front->ragel_cs);
	int cs = front->ragel_cs;
	char *p  = front->databuf;
	const char *pe = p + front->datalen;
	const char *eof = pe;
	const char *addrbuf;
	unsigned    addrlen;
	const char *resp;
	%%{
		machine smtp ;
		write init nocs ;
		write exec ;
	}%%
	front->ragel_cs = cs;
	log_debug ("Ragel new state is %d and intent is %d", cs, front->intention_data);
	//
	// Deliver header data; do not signal "last"; continue on next call
	if ((!front->intention_body) && (front->data_header != NULL) && (front->conf->mailhdr_callbacks != NULL)) {
		front_process_header (front, front->data_header, ((unsigned) (pe - front->data_header)), false);
	}
	return true;
}


/** @brief Reset the SMTP response parser.
 *
 * Call this routine after sending a full command, and before invoking the
 * incremental reply_parse_callback() routine for the first time.
 */
void reply_parse_reset (struct smtp_back *back) {
	//
	// Start in the beginning of the parsing process
	back->reply_cs = smtp_en_smtp_response;
}


/** @brief Parse SMTP response text and invoke the proper callback.
 *
 * @param back SMTP client control structure
 * @param buf Buffer holding (the next bit of) the reply
 * @param buflen Lenght of the buffer in @a buf
 * @param cb_replyline Callback routine for every response line, with a
 *		pointer beyond the response, and a flag to signal if this
 *		is the last response line.
 *
 * @returns true on success, false with an error setup in cmdbuf on error.
 *		When more parsing is required, then false is returned with
 *		EAGAIN in errno.
 */
bool reply_parse_callback (struct smtp_back *back, char *buf, unsigned buflen, cb_replyline_t *cb_replyline) {
	//
	// Declare variable for other state diagrams
	const char *addrbuf;
	unsigned    addrlen;
	const char *resp;
	struct smtp_front *front = back->frontend;
	//
	// Run the Ragel parser incrementally for the SMTP response
	int cs = back->reply_cs;
printf ("reply_parse_callback() sets cs to %d\n", cs);
	char *p  = buf;
	const char *pe = p + buflen;
	const char *eof = NULL;
	bool seenlast = false;
	%%{
		write init nocs ;
		write exec ;
	}%%
	back->reply_cs = cs;
printf ("reply_parse_callback() sets cs to %d (final from %d)\n", cs, smtp_first_final);
printf ("p=%p, pe=%p, seenlast=%d\n", p, pe, seenlast);
	//
	// Recognise incomplete parsing
	if (cs < smtp_first_final) {
		errno = EAGAIN;
		return false;
	} else if ((p != pe) || (!seenlast)) {
		errno = EINVAL;
		return false;
	}
	//
	// Return successful and complete parsing
printf ("Parsed happily\n");
	return true;
}

