/* Axe SMTP: server code
 *
 * The SMTP server, responding to I/O events EV_READ and EV_WRITE.
 * 
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <errno.h>   /* TODO: Response codes */

#include <ev.h>

#include <arpa2/com_err.h>
#include <arpa2/except.h>
#include <arpa2/socket.h>
#include <arpa2/quick-mem.h>
#include <arpa2/identity.h>

#include "axe.h"



/** @brief Create a new backend under a given frontend.
 *
 * Backends represent a single pair of MAIL FROM and RCPT TO values.
 * As a result of filtering logic, it is possible that the MAIL FROM
 * value is altered before passing on, even to different value for
 * different forwarded emails.  Note that such differences can only
 * be reliably implemented with LMTP, as that reports back separately
 * for each recipient address.
 *
 * This creates a backend structure connected to the frontend structure,
 * ready to be connected and then used for further actions.  The mail_from
 * and rcpt_to addresses are copied in from and to fields and used to
 * setup the backend.
 *
 * Later calls either share an existing backend connection or start a new
 * one.  Once connected, the EHLO, MAIL FROM and RCPT TO commands have
 * been run; for a shared connection, only the additional RCPT TO is run.
 *
 * @param back The output parameter for the newly created backend.
 *
 * @param front The frontend to which the backend is connected.
 *
 * @param ska Index into the socket addresses front->skas if non-negative.
 *	These addresses define an IPv6 or IPv4 address along with a TCP
 *	port number, and are usually filled with socket_parse() or DNS.
 *	Negative ska values will never match during sharing attempts.
 *
 * @param mail_from The MAIL FROM address to setup.
 *
 * @param rcpt_to The RCPT TO address to setup.
 *
 * @returns True on success, otherwise false with a com_err code in TODO:errno.
 */
bool back_new (struct smtp_back **back, struct smtp_front *front, int ska, const a2id_t *mail_from, const a2id_t *rcpt_to) {
	//
	// Allocate zeroed memory
	struct smtp_back *bk = NULL;
	if (!mem_alloc (front, sizeof (struct smtp_back), (void **) &bk         )) {
		goto fail;
	}
	if (!mem_alloc (front, front->conf->back_appsize, (void **) &bk->appdata)) {
		goto fail;
	}
	//
	// Setup data structures
	memcpy (&bk->from, mail_from, sizeof (bk->from));
	memcpy (&bk->to,   rcpt_to,   sizeof (bk->to  ));
	bk->state = PRE_CONNECT;
	bk->sox = -1;
	bk->ska = ska;
	bk->frontend = front;
	bk->next = front->backends;
	front->backends = bk;
	//
	// Initialise appdata for this backend
	if (front->conf->cb_init_back != NULL) {
		front->conf->cb_init_back (bk);
	}
	//
	// Report success
	*back = bk;
	return true;
	//
	// Return failure with a given TODO:errno
fail:
	*back = NULL;
	return false;
}


/** @brief Connect a backend to a server over a new connection.
 *
 * This is used after back_add() and before it is possible to
 * relay data; it has alternatives in back_connect_share() and
 * back_connect_match().
 *
 * When this procedure fails, the backend structure is unchanged.
 * This facilitates DNS-style iteration, such as over MX records
 * and/or AAAA/A records, in search for an online alternative.
 *
 * @param sa IPv6 or IPv4 ocket address with filled in port number,
 *	usually filled with socket_parse() or with DNS data.
 *
 * @returns True on success, otherwise false with a com_err code in TODO:errno.
 */
bool back_connect_new (struct smtp_back *back, const struct sockaddr_storage *sa) {
	void back_recv_resp (EV_P_ ev_io *back_io, int revents);
	assertxt (back->master == NULL, "Backend already connected");
	if (sa->ss_family == 0) {
		//TODO// errno
		return false;
	}
	back->ska = -1;
	int sox;
	if (socket_client (sa, SOCK_STREAM, &sox)) {
		back->sox = sox;
		back->master = back;
		back->cmdlen = 0;
		back->cmdofs = 0;
		back->state  = GET_WELCOME;
		reply_parse_reset (back);
		back->sox_io.data = back;
		ev_io_init (&back->sox_io, back_recv_resp, sox, EV_READ);
		ev_io_start (EV_DEFAULT_ &back->sox_io);
		return true;
	} else {
		return false;
	}
}


/** @brief Setup a backend to share the connection of another.
 *
 * This is used after back_new() and is an alternative to
 * back_connect_new().  This is part of back_connect_match().
 *
 * The backend to share from does not have to be a master itself.
 * When this procedure fails, the backend structure is unchanged.
 *
 * This backend does not become a master, because it is not in control
 * of the socket.  Shared backends will skip on various actions,
 * because the master is assumed to handle those; think of DATA
 * statements that are only needed once.  What is needed however,
 * is a RCPT TO statement.  This is sent here, and will lock the
 * frontend.
 *
 * @returns True on success, otherwise false with a com_err code in TODO:errno.
 *	This function however, does not fail.
 */
bool back_connect_share (struct smtp_back *back, struct smtp_back *sharefrom) {
	void back_send_rcpt_to (EV_P_ struct smtp_back *back);
	assertxt (back->master == NULL, "Backend already connected");
	assertxt (sharefrom->master != NULL, "Sharing from an unconnected backend");
	back->master = sharefrom->master;
	back->sox    = sharefrom->sox;
	back->ska    = sharefrom->ska;
	back->state  = SEND_RCPTTO;
	back->sox_io.data = back;
	ev_io_init (&back->sox_io, NULL, back->sox, EV_READ);
	back_send_rcpt_to (EV_DEFAULT_ back);
	return true;
}


/** @brief Try to share with a suitable backend.
 *
 * Given a backend structure, iterate over the other backends
 * under the same frontend, and look for one that matches the
 * MAIL FROM address and the RCPT TO domain, as well as the
 * IPv6 or IPv4 address and the port number.
 *
 * When a matching entry is found, use back_connect_share()
 * to share, and report success.  Otherwise, deliver a fresh
 * connection through back_connect_new().
 *
 * This procedure uses the ska value stored during back_new()
 * for comparison between backends.  Negative values cannot be
 * addressed at all, and will return a failure.  You may then
 * use alternative procedures, such as DNS traversal, to look
 * for a suitable address and call back_connect_new() with it.
 *
 * @param back The backend that wants to connect, and is hoping
 *	to find a matching backend to share.
 *
 * @returns True on success, otherwise false with a com_err code in TODO:errno.
 */
bool back_connect_match (struct smtp_back *back) {
	//
	// We currently fail on negative ska for back->frontend->skas [ska]
	if (back->ska < 0) {
		/* Won't share custom addresses.  This is current practice,
		 * but flexibility could be imagined.  We just cannot make
		 * the back_connect_new() call without skas[ska], but we
		 * the application could manage negative ska values which
		 * we could then share if possible.
		 */
		return false;
	}
log_debug ("back_connect_match() tries to find a suitable backend\n");
	//
	// Iterate over backends for the same frontend
	for (struct smtp_back *other = back->frontend->backends;
			other != NULL;
			other = other->next) {
		//
		// Ignore the entry for the searching soul
log_debug (" - requiring difference between other %p and new %p", (void *) other, (void *) back);
		if (other == back) {
			continue;
		}
		//
		// Only compare against master backends
		if (other->master != other) {
log_debug (" - requiring master of other %p to match other %p", (void *) other->master, (void *) other);
			continue;
		}
		//
		// Only match with the same ska index
log_debug (" - requiring ska index of other %d and me %d to match", other->ska, back->ska);
		if (other->ska != back->ska) {
			continue;
		}
		//
		// Match the MAIL FROM address (it must be the same)
log_debug (" - requiring my id %s to match the other %s", back->from.txt, other->from.txt);
		if (0 != strcmp (back->from.txt, other->from.txt)) {
			continue;
		}
		//
		// Match the RCPT TO address (domain must be the same)
log_debug (" - requiring my domain %s to match the other %s", back ->from.txt + back ->from.ofs [A2ID_OFS_DOMAIN], other->from.txt + other->from.ofs [A2ID_OFS_DOMAIN]);
		if (0 != strcmp (back ->from.txt + back ->from.ofs [A2ID_OFS_DOMAIN],
		                 other->from.txt + other->from.ofs [A2ID_OFS_DOMAIN])) {
			continue;
		}
		//
		// Jackpot: This is a matching entry, so share it
log_debug (" - WE FOUND A MATCHING BACKEND TO OUR NEW ONE");
		if (back_connect_share (back, other)) {
			log_debug ("Backend match succeeded with back_connect_share()");
			return true;
		} else {
			log_debug ("Backend match failed with back_connect_share()");
		}
	}
	//
	// We found no match, and resort to back_connect_new()
	// but we keep the ska to facilitate future matches
	log_debug ("Backend match falls back on back_connect_new()");
	int ska = back->ska;
	bool ok = back_connect_new (back, &back->frontend->skas [back->ska]);
	if (ok) {
		back->ska = ska;
	}
	return ok;
}


/* Internal: Callback for response lines found in cmdbuf.
 */
void back_cb_replyline (struct smtp_back *back, const char *beyond, bool lastline) {
	if (!lastline) {
		back->cmdofs = beyond - back->cmdbuf;
		log_debug ("Response offset is now %d", back->cmdofs);
	} else {
		back->cmdlen = beyond - back->cmdbuf;
		log_debug ("Response length is now %d", back->cmdlen);
	}
}


/* Internal: Do receive a response as sent by the backend.
 * After the last line has arrived, trigger the cb_sent()
 * callback for further processing by the application.
 *
 * This works by parsing the response incrementally, and
 * receiving callbacks for each line, with a flag saying
 * if it is the last.
 */
static void back_resp_to_front (struct smtp_back *back);
//
void back_recv_resp (EV_P_ ev_io *back_io, int revents) {
	void back_send_rcpt_to (EV_P_ struct smtp_back *back);
	//
	// Initialise
	assertxt (revents != EV_ERROR, "Error event in back_send_resp()");
	struct smtp_back *back = back_io->data;
	//
	// Read bytes from the input stream
	int oldlen = back->cmdlen;
	ssize_t rcvlen = recv (back_io->fd, back->cmdbuf + oldlen, 512 - oldlen, 0);
	if (rcvlen < 0) {
		//
		// Receiving error: Act as if an error message was received
		log_errno ("Error receiving SMTP response from backend");
		strcpy (back->cmdbuf, "500 Error in backend response\r\n");
		oldlen = back->cmdlen = back->cmdofs = 0;
		rcvlen = strlen  (back->cmdbuf);
	} else if (rcvlen == 0) {
		//
		// Connection closed: Act as if an error message was received
		//TODO// Need to avoid infinite looping
		log_info ("Backend disconnected before delivery of SMTP response");
		strcpy (back->cmdbuf, "500 Backend disconnected unexpectedly\r\n");
		oldlen = back->cmdlen = back->cmdofs = 0;
		rcvlen = strlen  (back->cmdbuf);
	}
	back->cmdlen = oldlen + rcvlen;
	assertxt (oldlen + rcvlen <= 512, "Response buffer too large");
	//
	// Parse the additional bytes in the input stream
	bool done = reply_parse_callback (back, back->cmdbuf + oldlen, rcvlen, back_cb_replyline);
	if ((!done) && (errno != EAGAIN)) {
		//
		// Parsing error: Act as if an error message was received
		log_errno ("Response parsing error in backend");
		strcpy (back->cmdbuf, "500 Backend response parsing error\r\n");
		back->cmdofs = 0;
		back->cmdlen = strlen (back->cmdbuf);
	}
	//
	// Remove initial characters from lines that are not the last
	if ((back->cmdofs > 0) && (back->cmdofs < back->cmdlen)) {
		memmove (back->cmdbuf + back->cmdofs, back->cmdbuf, back->cmdlen - back->cmdofs);
	}
	back->cmdlen -= back->cmdofs;
	back->cmdofs = 0;
	back->cmdbuf [back->cmdlen] = '\0';
	//
	// Stop now if the response is not yet complete
	if (!done) {
		return;
	}
	//
	// Stop processing backend events
	ev_io_stop (EV_A_ back_io);
	//
	// After the welcome status, relay the front-end's EHLO command
	if (back->state == GET_WELCOME) {
		if (0 == memcmp (back->cmdbuf, "220 ", 4)) {
			//
			// We got a proper greeting and will now send EHLO
			if (back_send_cmd_single (EV_DEFAULT_ back, NULL, back->frontend->ehloptr, back->frontend->ehlolen)) {
				back->state = SEND_EHLO;
				//
				// We got a new lock; release the one we had
				front_unlock (back->frontend, NULL, 0);
				//
				// Return sending control to the event loop
				return;
			} else {
				//
				// We failed to start the new command, complete below
				strcpy (back->cmdbuf, "450 Failed to relay EHLO command to backend\r\n");
				back->cmdlen = strlen (back->cmdbuf);
			}
		} else {
			//
			// We did not get a welcoming greeting
			strcpy (back->cmdbuf, "450 Backend did not send an SMTP greeting\r\n");
			back->cmdlen = strlen (back->cmdbuf);
		}
	}
	//
	// After the EHLO response, construct and send MAIL FROM
	if (back->state == GET_EHLO) {
		if (back->cmdbuf [0] == '2') {
			//
			// Our EHLO was welcomed and we proceed to MAIL FROM
			int cmdlen = 10 + 1 + back->from.ofs [A2ID_OFS_END] + 3;
			char cmdbuf [cmdlen];
			strcpy (cmdbuf, "MAIL FROM:<");
			strcpy (cmdbuf + 10 + 1, back->from.txt);
			memcpy (cmdbuf + cmdlen - 3, ">\r\n", 3);
			if (back_send_cmd_single (EV_DEFAULT_ back, NULL, cmdbuf, cmdlen)) {
				back->state = SEND_MAILFROM;
				//
				// We got a new lock; release the one we had
				front_unlock (back->frontend, NULL, 0);
				//
				// Return sending control to the event loop
				return;
			} else {
				//
				// We failed to start the new command, complete below
				strcpy (back->cmdbuf, "450 Failed to send MAIL FROM command to backend\r\n");
				back->cmdlen = strlen (back->cmdbuf);
			}
		} else {
			//
			// We did not get a positive reply to EHLO
			/* --proceed with the response code-- */ ;
		}
	}
	//
	// After the MAIL FROM response, construct and send RCPT TO
	if (back->state == GET_MAILFROM) {
		back_send_rcpt_to (EV_A_ back);
		return;
	}
	//
	// After the RCPT TO response, and if intention_data, then send DATA
	if ((back->state == GET_RCPTTO) && (back->frontend->intention_data)) {
		static const char *datacmd = "DATA\r\n";
		if (back_send_cmd_single (EV_DEFAULT_ back, NULL, datacmd, strlen (datacmd))) {
			back->state = SEND_DATACMD;
			//
			// We got a new lock; release the one we had
			front_unlock (back->frontend, NULL, 0);
			//
			// Return sending control to the event loop
			return;
		} else {
			//
			// We failed to start the new command, complete below
			strcpy (back->cmdbuf, "450 Failed to send DATA command to backend\r\n");
			back->cmdlen = strlen (back->cmdbuf);
		}
		return;
	}
	//
	// Trigger the backend callback if defined
	if (back->cb_sent != NULL) {
		back->cb_sent (back);
		return;
	}
	//
	// Report to the frontend, releasing this backend's lock on it
	back_resp_to_front (back);
}


/* Internal: Do send a command as stored in the backend.
 * Then wait for the response, to be collected until the
 * conclusive last line.
 */
void back_send_cmd (EV_P_ ev_io *back_io, int revents) {
	void back_disconnect (struct smtp_back *back);
	//
	// Initialise
	assertxt (revents != EV_ERROR, "Error event in back_send_cmd()");
	struct smtp_back *back = back_io->data;
	//
	// See how much should still be written
	int cmdleft = back->cmdlen - back->cmdofs;
	log_debug ("Hoping to send [\"%.*s\"] + \"%.*s\"", back->cmdofs, back->cmdbuf, cmdleft, back->cmdbuf + back->cmdofs);
	ssize_t sntlen = send (back_io->fd, back->cmdbuf + back->cmdofs, cmdleft, 0);
	if (sntlen < 0) {
		log_errno ("Error sending SMTP command");
		static const char *errmsg = "550 Backend connection did not accept SMTP command\r\n";
		front_unlock (back->frontend, errmsg, strlen (errmsg));
		goto disconnect;
	}
	cmdleft -= sntlen;
	back->cmdofs += sntlen;
	assertxt (cmdleft >= 0, "Command buffer over-reached in back_send_cmd()");
	//
	// Test if the status line was completely sent
	if (cmdleft == 0) {
		ev_io_stop (EV_A_ back_io);
		back->cmdlen = 0;
		back->cmdofs = 0;
		switch (back->state) {
		case SEND_EHLO:
			back->state = GET_EHLO;
			break;
		case SEND_MAILFROM:
			back->state = GET_MAILFROM;
			break;
		case SEND_RCPTTO:
			back->state = GET_RCPTTO;
			break;
		case SEND_DATACMD:
			back->state = GET_DATACMD;
			break;
		default:
			back->state = GET_RESPONSE;
			break;
		}
		reply_parse_reset (back);
		ev_set_cb (back_io, back_recv_resp);
		ev_io_set (&back->sox_io, back->sox_io.fd, EV_READ);
		ev_io_start (EV_A_ &back->sox_io);
	}
	//
	// Return happily
	return;
	//
	// Cleanup on errors
disconnect:
	back_disconnect (back);
}


/* Internal: Do send a data buffer as stored in the backend.
 * Then wait for the response, to be collected until the
 * conclusive last line.
 */
void back_send_data (EV_P_ ev_io *back_io, int revents) {
	void back_disconnect (struct smtp_back *back);
	//
	// Initialise
	assertxt (revents != EV_ERROR, "Error event in back_send_data()");
	struct smtp_back *back = back_io->data;
	//
	// See how much should still be written
	int dataleft = back->datalen - back->dataofs;
	log_debug ("Hoping to send [\"%.*s\"] + \"%.*s\"", back->dataofs, back->databuf, dataleft, back->databuf + back->dataofs);
	ssize_t sntlen = send (back_io->fd, back->databuf + back->dataofs, dataleft, 0);
	log_debug ("Sent length %d", (int) sntlen);
	if (sntlen < 0) {
		log_errno ("Error sending SMTP data");
		static const char *errmsg = "550 Backend connection did not accept SMTP data\r\n";
		front_unlock (back->frontend, errmsg, strlen (errmsg));
		goto disconnect;
	}
	dataleft -= sntlen;
	log_debug ("Backend dataofs == %d", back->dataofs);
	back->dataofs += sntlen;
	log_debug ("Backend dataofs := %d", back->dataofs);
	//
	// Test if the status line was completely sent
	assertxt (dataleft >= 0, "Command buffer over-reached in back_send_data()");
	if (dataleft == 0) {
		ev_io_stop (EV_A_ back_io);
		back->datalen = 0;
		back->dataofs = 0;
		static const char *okay = "250 Ok\r\n";
		front_unlock (back->frontend, okay, strlen (okay));
		if (back->dataend) {
			back->state = WAIT_CMDLINE;
		}
	}
	//
	// Return happily
	return;
	//
	// Cleanup on errors
disconnect:
	back_disconnect (back);
}


/* Internal: Add a recipient to a connected backend.
 *
 * The backend may be prepared with one of back_connect_new(),
 * back_connect_share() or back_connect_match().  The recipient
 * is stored in the backend as a result of the back_new() call.
 */
void back_send_rcpt_to (EV_P_ struct smtp_back *back) {
	//
	// Construct the SMTP command string
	int cmdlen = 8 + 1 + back->to.ofs [A2ID_OFS_END] + 3;
	char cmdbuf [cmdlen];
	strcpy (cmdbuf, "RCPT TO:<");
	strcpy (cmdbuf + 8 + 1, back->to.txt);
	memcpy (cmdbuf + cmdlen - 3, ">\r\n", 3);
	if (back_send_cmd_single (EV_A_ back, NULL, cmdbuf, cmdlen)) {
		back->state = SEND_RCPTTO;
		//
		// We got a new lock; release the one we had
		front_unlock (back->frontend, NULL, 0);
		//
		// Return sending control to the event loop
		return;
	} else {
		//
		// We failed to start the new command, complete below
		strcpy (back->cmdbuf, "450 Failed to send RCPT TO command to backend\r\n");
		back->cmdlen = strlen (back->cmdbuf);
		//
		// Report to the frontend, releasing this backend's lock on it
		back_resp_to_front (back);
	}
}


/** @brief Setup a single backend to asynchronously send a command.
 *
 * Send a command to a single backend.  The backend does not
 * have to be a master, but you should mind about calling this
 * before other work on the same master has finished.
 *
 * You should mind SMTP rules for the command, including its
 * length and the trailing "\r\n" characters.
 *
 * To submit a command to all backends, use back_send_cmd_all()
 * instead.
 *
 * @param back The backend through which the data is to be sent.
 *	May be a master, but does not have to be.
 *
 * @param cb Callback routine to call with this backend and a
 *	result string in its cmdbuf.
 *
 * @param ptr Pointer to the data region to send.  The data must
 *	be stable until this returns.
 *
 * @param len Length of the data to send at @a ptr.
 *
 * @returns True when the command was setup, false with a value
 *	in TODO:errno on failure.
 */
bool back_send_cmd_single (EV_P_ struct smtp_back *back, back_cb_command *cb, const uint8_t *ptr, size_t len) {
	//
	// Test if the command can be setup
	if ((len > sizeof (back->cmdbuf)) || (len <= 0)) {
		errno = ENOBUFS;
		return false;
	}
	//
	// Log the command being sent
	log_debug ("Backend %p command \"%.*s\"", (void *) back, (int) len, ptr);
	//
	// Setup the command
	memcpy (back->cmdbuf, ptr, len);
	back->cmdlen = len;
	back->cmdofs = 0;
	//
	// Lock the frontend until this backend finishes
	front_lock (back->frontend);
	//
	// Setup the callback after which the front will unlock
	if (cb != NULL) {
		back->cb_sent = cb;
	}
	//
	// Set the backend state to general command sending
	back->state = SEND_CMDLINE;
	//
	// Start asynchronous writing
	ev_io_set (&back->sox_io, back->sox_io.fd, EV_WRITE);
	ev_set_cb (&back->sox_io, back_send_cmd);
	ev_io_start (EV_A_ &back->sox_io);
	//
	// Return success
	return true;
}


/** @brief Setup all backends to asynchronously send a command.
 *
 * Send this command asynchronously to all master backends,
 * where you should mind about calling this before any other
 * work on the masters has finished.
 *
 * You should mind SMTP rules for the command, including its
 * length and the trailing "\r\n" characters.
 *
 * To submit a command to one backend only, use the function
 * back_send_cmd_single() instead.
 *
 * @param front The front whose backends should all receive
 *	the command.
 *
 * @param cb Callback routine to call for the frontend, while
 *	providing a result string in its cmdbuf.  Note that
 *	this routine can be called at any time during or after
 *	this setup.
 *
 * @param ptr Pointer to the data region to send.  The data must
 *	be stable until this returns, and it must not be stored
 *	in the front end's cmdbuf.
 *
 * @param len Length of the data to send at @a ptr.
 *
 * @returns True when the command was setup, or false with a response
 *	in the backend's cmdbuf on failure.
 */
static void back_resp_to_front (struct smtp_back *back) {
	//
	// Make a callback before restarting the frontend
	if (front_lastlock (back->frontend)) {
		if (back->frontend->cb_workdone != NULL) {
			back->frontend->cb_workdone (back->frontend);
		}
	}
	//
	// Release the lock of this backend on the frontend
	front_unlock (back->frontend, back->cmdbuf, back->cmdlen);
}
//
bool back_send_cmd_all (EV_P_ struct smtp_front *front, front_cb_command *cb, uint8_t *ptr, size_t len) {
	assertxt (ptr != (uint8_t *) front->cmdbuf, "You must not send the front cmdbuf");
	//
	// Setup the cmdbuf with a default acceptance response
	strcpy (front->cmdbuf, "211 No backends\r\n");
	//
	// Register the callback routine in the frontend
	front->cb_workdone = cb;
	//
	// Iterate over all backends as potential targets
	for (struct smtp_back *back = front->backends;
			back != NULL;
			back = back->next) {
		//
		// Skip over shared backends
		if (back->master != back) {
			continue;
		}
		//
		// Setup for sending to the master backend
		if (!back_send_cmd_single (EV_A_ back, back_resp_to_front, ptr, len)) {
			snprintf (back->cmdbuf, sizeof (back->cmdbuf)-1,
				"412 %s backend unable to handle command\r\n",
				back->to.txt + back->to.ofs [A2ID_OFS_DOMAIN]);
			//
			// Lock and immediately unlock the frontend
			back_resp_to_front (back);
		}
	}
	//
	// Return success -- even if no backend showed an interest
	return true;
}



/** @brief Setup all backends to asynchronously send content data.
 *
 * Send this content data asynchronously to all master backends,
 * where you should mind about calling this before any other
 * work on the masters has finished.
 *
 * The data content may be an intermediate chunk of data, or it
 * may be the final chunk including the "\r\n.\r\n" terminator.
 * The flag lastdata is true when the last block is passed.
 *
 * This function should store either the data buffer and length
 * or the data stored in it for asynchronous relaying.  To avoid
 * changes to the data buffer being passed, a frontend lock is
 * possible.  Such a lock also avoids new blocks from being passed
 * into this call.
 *
 * @param front The front whose backends should all receive
 *	the content data.
 *
 * @param databuf Pointer to the data buffer to send.  The data will
 *	be stable until this returns, but while the frontend is
 *	locked it will also be kept stable, to benefit backends
 *	that do asynchronous processing.
 *
 * @param datalen Length of the data to send at @a databuf.
 *
 * @param lastdata Set to true if this is the last data block,
 *	which ends in the "\r\n.\r\n" sequence.
 *
 * @returns True when the data was setup, or false with a response
 *	in the backend's cmdbuf on failure.
 */
bool back_send_data_all (struct smtp_front *front, const uint8_t *databuf, size_t datalen, bool lastdata) {
	//
	// Iterate over all backends as potential targets
	for (struct smtp_back *back = front->backends;
			back != NULL;
			back = back->next) {
		//
		// Skip over shared backends
		if (back->master != back) {
			continue;
		}
		//
		// Lock the front to fixate the buffer
		front_lock (front);
		//
		// Setup for sending to the master backend
		back->databuf = databuf;
		back->datalen = datalen;
		back->dataend = lastdata;
		back->dataofs = 0;
		//
		// Setup the callback for the backend
		ev_set_cb (&back->sox_io, back_send_data);
		ev_io_set (&back->sox_io, back->sox_io.fd, EV_WRITE);
		ev_io_start (EV_DEFAULT_ &back->sox_io);
	}
	//
	// Return success -- even if no backend showed an interest
	return true;
}


/** @brief Disconnect a backend, including its socket.
 *
 * The backend is kept for administration, such as retrieval of the
 * success or failure of its task.  Only when the backend was setup
 * with back_connect() has it been setup as a master and will the
 * socket be closed.
 *
 * This function does not fail.
 */
void back_disconnect (struct smtp_back *back) {
	if (back->master == NULL) {
		return;
	}
	ev_io_stop (EV_DEFAULT_ &back->sox_io);
	if (back->sox >= 0) {
		if (back->master == back) {
			log_debug ("Disconnecting master socket %d or %d", back->sox, back->sox_io.fd);
			socket_close (back->sox);
		}
		back->sox = -1;
	}
	back->master = NULL;
}

