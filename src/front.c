/* Axe SMTP: server code
 *
 * The SMTP server, responding to I/O events EV_READ and EV_WRITE.
 * 
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <string.h>

#include <ev.h>

#include <arpa2/com_err.h>
#include <arpa2/except.h>
#include <arpa2/socket.h>
#include <arpa2/quick-mem.h>
#include <arpa2/identity.h>

#include "axe.h"



/** @brief Disconnect a server.
 *
 * This is called after an I/O failure or QUIT command.
 *
 * This takes down all resources, including for backends,
 * for a given frontend connection, but it does not stop
 * the daemon from listening for new frontend connections.
 */
void front_disconnect (EV_P_ struct smtp_front *front) {
	//
	// Iterate over backends, closing those
	for (struct smtp_back *cli = front->backends;
				cli != NULL;
				cli = cli->next) {
		back_disconnect (cli);
	}
	//
	// Stop the front-end
	ev_io_stop (EV_A_ &front->cnx_io);
	//
	// Close the socket for the server
	log_debug ("Closing front-end connection");
	close (front->cnx_io.fd);
	//
	// Free the memory pool for the server
	mem_close ((void **) &front);
}


/** @brief Lock the frontend for a backend that is processing.
 *
 * Event-driven locking is easy.  This is just a counter of the
 * number of backends that are processing.  Locks can nest, and
 * should be unwound with front_unlock().  Whether a frontend
 * is about to unlock can be tested with front_lastlock().
 *
 * This blocks the sending of a response from a frontend until
 * all backends involved have had a chance to add their work.
 * Backends must use it to avoid concurrent progression of the
 * frontend.
 *
 * This function does not fail.
 */
void front_lock (struct smtp_front *front) {
	//
	// Stop event processing if it is still running
	if (front->backwork == 0) {
		ev_io_stop (EV_DEFAULT_ &front->cnx_io);
	}
	//
	// Increment the nesting counter
	front->backwork++;
	log_debug ("Frontend lock incremented to %d", front->backwork);
}


/** @brief Unwind a previously obtained lock on the frontend.
 *
 * Event-driven locking is easy.  This is just a counter of the
 * number of backends that are still processing.  Locks can be
 * nested, and must be unwound to undo the work in front_lock().
 *
 * When a buffer with at least 3 characters is provided, it is
 * considered to be a candidate response.  The numeric value in
 * the first three characters decides whether the current
 * response is replaced by the new one; the highest value will
 * prevail.
 *
 * This may unblock the sending of a response from a frontend
 * when it is the last responder.  Use front_lastlock() if an
 * intended call to front_unlock() is the last to restart the
 * interaction in the front.
 *
 * This function does not fail.
 */
void front_unlock (struct smtp_front *front, const char *cmdbuf, unsigned cmdlen) {
	//
	// Reduce the lock nesting counter
	log_debug ("Frontend lock decrement from %d", front->backwork);
	front->backwork--;
	//
	//TODO// Ugly preparation for response codes in cmdbuf
	if (front->cmdbuf [0] > '9') {
		strcpy (front->cmdbuf, "250 Ok\r\n");
	}
	//
	// Adopt the response if it has a higher status than the current
	if ((cmdbuf != NULL) && (cmdlen >= 4) && (cmdbuf [3] == ' ') && (memcmp (cmdbuf, front->cmdbuf, 3) > 0)) {
		assertxt (cmdlen <= sizeof (front->cmdbuf), "Response is too long for an SMTP line");
		memcpy (front->cmdbuf, cmdbuf, cmdlen);
		front->cmdlen = cmdlen;
	}
	//
	// Act if the lock is now released
	if (front->backwork == 0) {
		//
		// If this is DATA, check the code of the response
		if (front->intention_data && (0 != memcmp (front->cmdbuf, "354", 3))) {
			front->intention_data = false;
		}
		//
		// Start response sending from the backend
		ev_io_set (&front->cnx_io, front->cnx_io.fd, EV_WRITE);
		ev_io_start (EV_DEFAULT_ &front->cnx_io);
	}
}


/** @brief Test if only one more lock remains on a frontend.
 *
 * Event-driven locking is easy.  This is just a counter of the
 * number of backends that are still processing.  Locks can be
 * nested, and it may be useful to know that the last is about
 * to be released.
 *
 * This function tests whether the next call to front_unlock()
 * will trigger the frontend to send a response.  In that case,
 * it returns true, and otherwise it returns false.
 *
 * This function does not fail.
 */
bool front_lastlock (struct smtp_front *front) {
	return (front->backwork == 1);
}


/** @brief Process a received complete SMTP command.
 *
 * There is no currently registered callback; we should add one.
 *
 * Process the cmdbuf [0..cmdlen-1] with Ragel, and respond appropriately.
 * This may involve setting up backends, initiating responses, etc.
 * Backends are likely to lock the frontend, so that its response sending
 * gets delayed until the backends have all completed (and reported their
 * individual response codes).
 */
void front_process_cmd (EV_P_ struct smtp_front *front) {
	//
	// Lock the frontend, stopping its I/O processing
	front_lock (front);
	//
	// Parse the command and invoke callbacks
	log_debug ("Received SMTP command: %.*s", (int) front->cmdlen, front->cmdbuf);
	command_parse_callback (front);
	//
	// Send the response line and after that process intention_ flags
	front->cmdlen = strlen (front->cmdbuf);
	front->cmdofs = 0;
	front->state  = PUT_RESP;
	//
	// Unlock the frontend, possibly starting retransmission
	front_unlock (front, "250 Ok\r\n", 8);
}


/** @brief Let the server send a status from the command buffer.
 *
 * This is a callback from the server's event loop.
 * The event fd is the socket to use.
 * The event data points at the smtp_front.
 *
 * Write bytes from the command buffer.  Aim to send all that remains.
 * Learn if this is not completed, or if the socket is closed.
 */
void front_send_resp (EV_P_ ev_io *srv_io, int revents) {
	void front_recv_data (EV_P_ ev_io *srv_io, int revents);
	void front_recv_cmd (EV_P_ ev_io *srv_io, int revents);
	//
	// Initialise
	assertxt (revents != EV_ERROR, "Error event in front_send_resp()");
	struct smtp_front *front = srv_io->data;
	//
	// See how much should still be written
	int cmdleft = front->cmdlen - front->cmdofs;
	ssize_t sntlen = send (srv_io->fd, front->cmdbuf + front->cmdofs, cmdleft, 0);
	if (sntlen < 0) {
		log_errno ("Error sending SMTP response");
		goto disconnect;
	} else if (sntlen == 0) {
		log_info ("Frontend disconnected before SMTP response");
		goto disconnect;
	}
	cmdleft -= sntlen;
	front->cmdofs += sntlen;
	assertxt (cmdleft >= 0, "Command buffer over-reached in front_send_resp()");
	//
	// Test if the status line was completely sent
	if (cmdleft == 0) {
		ev_io_stop (EV_A_ srv_io);
		front->cmdlen = 0;
		if (front->intention_done) {
			goto disconnect;
		} else if (front->intention_data) {
			front->state = GET_DATABUF;
			front->datalen = 0;
			front->datafree = sizeof (front->databuf);
			ev_set_cb (srv_io, front_recv_data);
		} else {
			front->state  = GET_CMDLINE;
			ev_set_cb (srv_io, front_recv_cmd);
		}
		ev_io_set (&front->cnx_io, front->cnx_io.fd, EV_READ);
		ev_io_start (EV_A_ &front->cnx_io);
	}
	//
	// Return happily
	return;
	//
	// Cleanup on errors
disconnect:
	front_disconnect (EV_A_ front);
}


/** @brief Let the server receive an SMTP command.
 *
 * This is a callback from the server's event loop.
 * The event fd is the socket to use.
 * The event data points at the smtp_front.
 *
 * Receive bytes into the command buffer.  Check for CR-LF.
 * Process with Ragel when a full line was received.
 */
void front_recv_cmd (EV_P_ ev_io *srv_io, int revents) {
	//
	// Initialise
	assertxt (revents != EV_ERROR, "Error event in front_recv_cmd()");
	struct smtp_front *front = srv_io->data;
	//
	// Try to read the remainder of the cmdbuf
	int cmdlen = front->cmdlen;
	ssize_t rcvlen = recv (srv_io->fd, front->cmdbuf + cmdlen, 512 - cmdlen, 0);
	if (rcvlen < 0) {
		log_errno ("Error receiving SMTP command");
		goto disconnect;
	} else if (rcvlen == 0) {
		log_info ("Frontend disconnected before SMTP command");
		goto disconnect;
	}
	cmdlen += rcvlen;
	front->cmdlen = cmdlen;
	assertxt (cmdlen <= 512, "Command buffer too large");
	//
	// Test if the command is complete
	if ((cmdlen > 0) && (front->cmdbuf [cmdlen-1] == '\n')) {
		ev_io_stop (EV_A_ srv_io);
		ev_set_cb (srv_io, front_send_resp);
		front_process_cmd (EV_A_ front);
	}
	//
	// Return happily
	return;
	//
	// Cleanup on errors
disconnect:
	front_disconnect (EV_A_ front);
}


/** @brief Receive a header and see if it needs special handling.
 *
 * This looks at the app_conf array of header callback structures.
 * If that exists, build a memory buffer holding the headers, and
 * support changes to that buffer.
 *
 * The "last" flag is set when this is the last portion of a header
 * that possibly got split.
 */
void front_process_header (struct smtp_front *front, char *hdrptr, unsigned hdrlen, bool last) {
	//
	// Open the buffer if it is not yet allocated
	if (mem_isnull (&front->hdrbuf)) {
		if (!mem_buffer_open (front, (2000 | hdrlen), &front->hdrbuf)) {
			//TODO// MemoryError
			exit (1);
		}
		front->hdrnew = 0;
		front->hdrall = 0;
	}
	//
	// Append the header chunk to the memory buffer
	if (!mem_buffer_resize (front, front->hdrall + hdrlen, &front->hdrbuf)) {
		//TODO// MemoryError
		exit (1);
	}
	memcpy (front->hdrbuf.bufptr + front->hdrall, hdrptr, hdrlen);
	front->hdrall += hdrlen;
	//
	// Await completion if this was not the last chunk for the header
	if (!last) {
		return;
	}
	//
	// Do not continue if no headers are needed in the application
	struct app_conf_mailhdr *mh = front->conf->mailhdr_callbacks;
	if (mh == NULL) {
		return;
	}
	//
	// Search if the application wants to handle this header
	while (mh->hdrname != NULL) {
		if (0 == strncasecmp (front->hdrbuf.bufptr + front->hdrnew, mh->hdrname, strlen (mh->hdrname))) {
			break;
		}
	}
	//
	// Possibly check if the "header" is the empty line after the headers
	if (mh->hdrname == NULL) {
		if ((hdrlen != 2) || (0 != strncmp (hdrptr, "\r\n", 2))) {
			mh = NULL;
		}
	}
	//
	// Quietly add the header if no callback was, um, called for
	if ((mh == NULL) || (mh->cb_header == NULL)) {
		front->hdrnew = front->hdrall;
		return;
	}
	//
	// Ask the callback to handle the header and return a new length
	int newhdrlen = mh->cb_header (front, &front->hdrbuf, front->hdrnew);
	//
	// If the new header length is <= 0 then move it over the original
	if (newhdrlen < 0) {
		newhdrlen = -newhdrlen;
		memmove (front->hdrbuf.bufptr + front->hdrnew,
		         front->hdrbuf.bufptr + front->hdrall,
			 newhdrlen);
	}
	//
	// Set the new header to the returned size; set hdrall/hdrnew after it
	front->hdrall = front->hdrnew += newhdrlen;
}


/** @brief Let the server receive data content for the DATA command.
 *
 * This is a callback from the server's event loop.
 * The event fd is the socket to use.
 * The event data points at the smtp_front.
 *
 * Receive bytes into the command buffer.  Check for CR-LF, DOT, CR-LF.
 * Relay content to backends, which are assumed to be listening for it.
 */
void front_recv_data (EV_P_ ev_io *srv_io, int revents) {
	//
	// Initialise
	assertxt (revents != EV_ERROR, "Error event in front_recv_data()");
	struct smtp_front *front = srv_io->data;
	//
	// Try to read the remainder of the databuf
	unsigned datalen = front->datalen;
	ssize_t rcvlen = recv (srv_io->fd, front->databuf + datalen, front->datafree - datalen, 0);
	if (rcvlen < 0) {
		log_errno ("Error receiving SMTP content data");
		goto disconnect;
	} else if (rcvlen == 0) {
		log_info ("Frontend disconnected before content DATA");
		goto disconnect;
	}
	datalen += rcvlen;
	front->datalen = datalen;
	assertxt (datalen <= front->datafree, "Data buffer too large");
	//
	// Test if the content data is complete
	command_data_content (front);
	//
	// Decide if the next action will be different
	if (!front->intention_data) {
		//
		// We found the "\r\n.\r\n" trailer
		log_debug ("Data content arrival finished");
		static const char *resp = "250 Ok\r\n";
		strcpy (front->cmdbuf, resp);
		front->cmdlen = strlen (resp);
		front->cmdofs = 0;
		front->state  = PUT_RESP;
		ev_set_cb (srv_io, front_send_resp);
	}
	//
	// Lock our frontend, to be released below
	front_lock (front);
	//
	// Pass the content data to all backends
	// but only when header & body DATA callbacks are NULL
	if ((front->conf->mailhdr_callbacks == NULL) && (front->conf->cb_data_body == NULL)) {
		back_send_data_all (front, front->databuf, front->datalen, !front->intention_data);
	}
	//
	// Reset the buffer for future reads
	//TODO// This might at some day increase concurrency if deferred til unlock?
	front->datalen = 0;
	//
	// Unlock the frontend to allow processing, perhaps now
	// or perhaps later when a backend holds a lock
	static const char *defresp = "211 No backends\r\n";
	front_unlock (front, defresp, strlen (defresp));
	//
	// Return happily
	return;
	//
	// Cleanup on errors
disconnect:
	front_disconnect (EV_A_ front);
}
