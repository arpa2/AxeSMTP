/** @defgroup axe-smtp AxeSMTP for Chopping and Splitting your Mail
 * @{
 *
 * AxeSMTP allows manipulation of envelope data and, if so desired,
 * of message headers or data passing through.  This facilitates
 * advanced email handling, such as Signed Identities, Access Control
 * and Address Translation.  These may take the MAIL FROM identity
 * into account as well as the RCPT TO identity.
 *
 * This command runs as a front end SMTP server with any number of
 * SMTP clients to pass the traffic out for varying MAIL FROM names.
 * This is done because the MAIL FROM may be changed depending on
 * the RCPT TO, but SMTP does not support this.  So we need to chop
 * and split our email and scatter the remnants over backends.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdint.h>
#include <stdbool.h>

#include <ev.h>

#include <arpa2/socket.h>
#include <arpa2/identity.h>



/** @brief States for the SMTP server side (for the single frontend)
 *
 * @def GET_CMDLINE  Load command into cmdbuf, cmdlen gives incomplete length
 * @def PUT_CMDLINE  Waiting until command has returned from all backends
 * @def PUT_RESP     Waiting until the response has been sent back
 * @def GET_DATABUF  Load DATA content into databuf, datalen is current length
 * @def PUT_DATABUF  Waiting for some shared buffer space free in all backends
 * @def SRV_CLOSED   Front-end or SMTP server socket has been closed
 */
typedef enum smtp_srvstate {
	GET_CMDLINE,
	PUT_CMDLINE,
	PUT_RESP,
	GET_DATABUF,
	PUT_DATABUF,
	SRV_CLOSED,
} smtp_srvstate;


struct smtp_front;
typedef void front_cb_command (struct smtp_front *);

struct smtp_back;
typedef void back_cb_command (struct smtp_back *);

struct appdata_front;
struct appdata_back;


/** @brief State stored for the SMTP server side, or the front end.
 *
 * A new front is created for each accept() on the server socket.
 * The structure is a Quick-MEM master structure.  It is contained in
 * the data element of the socket's event data.
 */
struct smtp_front {
	// Linked list of smtp_back client backends
	struct smtp_back *backends;
	// The number of backends that are working
	unsigned backwork;
	// The socket address to use in backends
	struct sockaddr_storage *client_sockaddr;
	// Callback to report that work is done
	front_cb_command *cb_workdone;
	// Event handler for this server
	ev_io cnx_io;
	// Application Configuration, including callbacks
	const struct cmdparser *args;
	const struct sockaddr_storage *skas;
	const struct app_conf *conf;
	struct appdata_front *appdata;
	// Current protocol state
	smtp_srvstate state;
	// Command buffer [RFC 5321, 4.5.3.1.4] of 512 bytes including CR-LF
	char cmdbuf [520];
	unsigned cmdlen;
	unsigned cmdofs;
	// Copy of the original EHLO command
	char *ehloptr;
	unsigned ehlolen;
	// Data buffer -- a locally chosen size
	char databuf [1200];
	unsigned datalen, datafree;
	// Pointer to the start of header text (or continued portion)
	char *data_header;
	// Split between the DATA for SMTP headers/empty and the body
	unsigned data_split;
	// Memory buffer for headers, used only if the app_conf calls for it
	membuf hdrbuf;
	// Offset to the new header, and after all headers
	unsigned hdrnew, hdrall;
	// Administration for Ragel progress
	int ragel_cs;
	// Next optional parameter, and last kw=val iterator output
	const char *param0;
	const char *kwbuf, *valbuf;
	unsigned    kwlen,  vallen;
	// Intentions: Want to be done, want to pass data
	bool intention_done;
	bool intention_data;
	bool intention_body;
	// Parsed MAIL FROM identity
	char got_helo_1st;
	bool got_mail_from;
	bool got_rcpt_to;
a2id_t from;
};


/** @brief States for the SMTP client side (one of the backends)
 *
 * @def PRE_CONNECT   Not yet connected to a backend server
 * @def GET_WELCOME   Parse welcome and store it in cmdbuf
 * @def SEND_EHLO     Relay the EHLO command from the frontend
 * @def GET_EHLO      Parse response from the relayed HELO/EHLO command
 * @def SEND_MAILFROM Construct a MAIL FROM command based on the frontend
 * @def GET_MAILFROM  Parse response from the MAIL FROM command
 * @def SEND_RCPTTO   Construct a RCPT TO command based on the frontend
 * @def GET_RCPTTO    Parse response from the RCPT TO command
 * @def WAIT_CMDLINE  Wait until a complete command is available for sending
 * @def SEND_CMDLINE  Busy sending cmdbuf, self.cmdofs is what's locally sent
 * @def GET_RESPONSE  Parse response and store it in cmdbuf
 * @def SEND_BUFDATA  Send buffer data, self.bufofs is what's locally sent
 * @def SENT_BUFDATA  Done sending buffer data, not sure yet how to continue
 * @def CLI_CLOSED    Back-end or SMTP client socket has been closed
 */
typedef enum smtp_clistate {
	PRE_CONNECT,
	GET_WELCOME,
	SEND_EHLO,
	GET_EHLO,
	SEND_MAILFROM,
	GET_MAILFROM,
	SEND_RCPTTO,
	GET_RCPTTO,
	SEND_DATACMD,
	GET_DATACMD,
	WAIT_CMDLINE,
	SEND_CMDLINE,
	GET_RESPONSE,
	SEND_BUFDATA,
	SENT_BUFDATA,
	CLI_CLOSED
} smtp_clistate;


/** @brief State stored for the SMTP client side.
 *
 * A new smtp_back is created for each new MAIL FROM address.
 * They form a linked list, and point back to the smtp_front master.
 * This is contained in the data element of the socket's event data.
 */
struct smtp_back {
	// Links to the environment
	struct smtp_back *next;
	struct smtp_front *frontend;
	// The origin of the socket
	struct smtp_back *master;
	// Application-specific data
	struct appdata_back *appdata;
	// Callback to report that something was sent and a final response received
	back_cb_command *cb_sent;
	// Local MAIL FROM identity
	a2id_t from;
	// Local RCPT TO identity
	a2id_t to;
	// Socket address index into frontend->skas, or negative for "custom"
	int ska;
	// Socket for this backend, so we can close it
	int sox;
	// Event handler for this backend
	ev_io sox_io;
	// Current protocol state
	smtp_clistate state;
	// Command buffer [RFC 5321, 4.5.3.1.4] of 512 bytes including CR-LF
	char cmdbuf [520];
	unsigned cmdlen;
	unsigned cmdofs;
	int reply_cs;
	// Data buffer -- assumed to be stored elsewhere
	const char *databuf;
	unsigned datalen;
	unsigned dataofs;
	bool dataend;
};


/** @brief Application configuration with callbacks for specific headers.
 *
 * This structure is used as an array, with a NULL in the last name.
 * If it is present, then headers and one empty line are collected in
 * a memory buffer.  Note that all lines end in CR-LF.
 *
 * The whole buffer must be shipped before any body data.
 */
struct app_conf_mailhdr {
	/** The name of the header, including the follow colon.
	 * The last entry is NULL, and matches the empty line after headers.
	 * For example "From:".
	 */
	const char *hdrname;
	/** Headers without an entry in app_conf_headers are quietly added,
	 * mentioned headers get their callback invoked, with the header last,
	 * and may change the header or remove it.  When the header name is
	 * defined with a NULL callback, then the header will be removed.
	 * Returns a new header size, negated to move it back over the old.
	 * That is, size >=0 is in place of the old, <0 wrote after the old.
	 */
	int (*cb_header) (struct smtp_front *srv_pool, struct membuf *headers, unsigned lastofs);
};


/** @brief Application configuration, such as callbacks and flags.
 *
 * The app_conf structure defines callback functions to be called
 * when a command (or a fragment of content data) passes through.
 * Callbacks may take control, by returning false as an indication
 * that they wrote a response into cmdbuf to be delivered.  When
 * they return true, default control ensues.  Callbacks set to
 * NULL also trigger default handling, which varies per command.
 *
 * The app_conf structure defines flags that set and clear optional
 * behaviour in the AxeSMTP generic structures.
 *
 * This structure is treated by AxeSMTP as static and constant, so it
 * is usually a constant data definition in any backend.
 *
 * The assumption is that applications compile together with AxeSMTP,
 * so the structure is automatically aligned.
 */
struct app_conf {
	const char *name;
	const char *descr;
	const uint32_t flags;
	// If not 0, then appdata is allocated for front and back structures
	unsigned front_appsize, back_appsize;
	// INIT callback can setup and initialise frontends and backends
	void (*cb_init_front)   (struct smtp_front *srv_pool);
	void (*cb_init_back )   (struct smtp_back  *back    );
	// FINI callback can teardown and cleanup frontends and backends
	void (*cb_fini_front)   (struct smtp_front *srv_pool);
	void (*cb_fini_back )   (struct smtp_back  *back    );
	// HELO callback can add response lines to databuf
	bool (*cb_helo)         (struct smtp_front *srv_pool, const char *dombuf, uint8_t domlen);
	// EHLO callback can add response lines to databuf
	bool (*cb_ehlo)         (struct smtp_front *srv_pool, const char *dombuf, uint8_t domlen);
	// LHLO callback can add response lines to databuf
	bool (*cb_lhlo)         (struct smtp_front *srv_pool, const char *dombuf, uint8_t domlen);
	// MAIL FROM callback may parse the address and process options
	bool (*cb_mail_from)    (struct smtp_front *srv_pool, const char *addrbuf, unsigned addrlen);
	// RCPT TO callback may parse the address and process options (TODO: add backend?)
	bool (*cb_rcpt_to)      (struct smtp_front *srv_pool, const char *addrbuf, unsigned addrlen);
	// DATA callback announces the upcoming transfer of actual data
	bool (*cb_data)         (struct smtp_front *srv_pool);
	// DATA CONTENT callbacks for treatable headers, to modify values or
	// presence, while non-mentioned headers quietly collect in the same
	// memory buffer, preparing for backend DATA sending
	struct app_conf_mailhdr *mailhdr_callbacks;
	// DATA CONTENT callback to transfer body data, which may be chunked;
	// the "last" flag is set on the final DATA body chunk delivered.
	bool (*cb_data_body) (struct smtp_front *srv_pool, const uint8_t *databuf, unsigned datalen, bool last);
	// RSET callback may cleanup resources during a connection reset
	bool (*cb_rset)         (struct smtp_front *srv_pool);
	// VRFY callback may add response data in databuf
	bool (*cb_vrfy)         (struct smtp_front *srv_pool, const char *strbuf, unsigned strlen);
	// EXPN callback may add response data in databuf
	bool (*cb_expn)         (struct smtp_front *srv_pool, const char *strbuf, unsigned strlen);
	// HELP callback may add response data in databuf
	bool (*cb_help)         (struct smtp_front *srv_pool, const char *opt_strbuf, unsigned strlen);
	// NOOP callback is a silly facility, but conforms to common structure
	bool (*cb_noop)         (struct smtp_front *srv_pool, const char *opt_strbuf, unsigned strlen);
	// QUIT callback may behave nicely during orderly teardown
	bool (*cb_quit)         (struct smtp_front *srv_pool);
	// BDAT callback is for a command not currently in use
	bool (*cb_bdat)         (struct smtp_front *srv_bdat);
};


/** @brief Application call to initialise a server structure.
 *
 * This call sets up the application-specific configuration pointer.
 */
void axesmtp_init (struct smtp_front *srv);


/** @brief Lock the frontend for a backend that is processing.
 *
 * Event-driven locking is easy.  This is just a counter of the
 * number of backends that are processing.  Locks can nest, and
 * should be unwound with front_unlock().  Whether a frontend
 * is about to unlock can be tested with front_lastlock().
 *
 * This blocks the sending of a response from a frontend until
 * all backends involved have had a chance to add their work.
 * Backends must use it to avoid concurrent progression of the
 * frontend.
 *
 * This function does not fail.
 */
void front_lock (struct smtp_front *front);


/** @brief Unwind a previously obtained lock on the frontend.
 *
 * Event-driven locking is easy.  This is just a counter of the
 * number of backends that are still processing.  Locks can be
 * nested, and must be unwound to undo the work in front_lock().
 *
 * When a buffer with at least 3 characters is provided, it is
 * considered to be a candidate response.  The numeric value in
 * the first three characters decides whether the current
 * response is replaced by the new one; the highest value will
 * prevail.
 *
 * This may unblock the sending of a response from a frontend
 * when it is the last responder.  Use front_lastlock() if an
 * intended call to front_unlock() is the last to restart the
 * interaction in the front.
 *
 * This function does not fail.
 */
void front_unlock (struct smtp_front *front, const char *cmdbuf, unsigned cmdlen);


/** @brief Test if only one more lock remains on a frontend.
 *
 * Event-driven locking is easy.  This is just a counter of the
 * number of backends that are still processing.  Locks can be
 * nested, and it may be useful to know that the last is about
 * to be released.
 *
 * This function tests whether the next call to front_unlock()
 * will trigger the frontend to send a response.  In that case,
 * it returns true, and otherwise it returns false.
 *
 * This function does not fail.
 */
bool front_lastlock (struct smtp_front *front);


/** @brief Let the server send a status from the command buffer.
 *
 * This is a callback from the server's event loop.
 * The event fd is the socket to use.
 * The event data points at the smtp_front.
 *
 * Write bytes from the command buffer.  Aim to send all that remains.
 * Learn if this is not completed, or if the socket is closed.
 */
void front_send_resp (EV_P_ ev_io *srv_io, int revents);


/** @brief Test if a backend is defined with a socket address.
 *
 * Given the "ska" for a backend, test if it has a socket address,
 * which means that it would be able to connect.  Optional backends
 * may return false, while required ones always return true.
 */
static inline bool front_defines_back (struct smtp_front *front, int ska) {
	return (front->skas [ska].ss_family != 0);
}


/** @brief Receive a header and see if it needs special handling.
 *
 * This looks at the app_conf array of header callback structures.
 * If that exists, build a memory buffer holding the headers, and
 * support changes to that buffer.
 *
 * The "last" flag is set when this is the last portion of a header
 * that possibly got split.
 */
void front_process_header (struct smtp_front *front, char *hdrptr, unsigned hdrlen, bool last);



/** @brief Create a new backend under a given frontend.
 *
 * Backends represent a single pair of MAIL FROM and RCPT TO values.
 * As a result of filtering logic, it is possible that the MAIL FROM
 * value is altered before passing on, even to different value for
 * different forwarded emails.  Note that such differences can only
 * be reliably implemented with LMTP, as that reports back separately
 * for each recipient address.
 *
 * This creates a backend structure connected to the frontend structure,
 * ready to be connected and then used for further actions.  The mail_from
 * and rcpt_to addresses are copied in from and to fields and used to
 * setup the backend.
 *
 * Later calls either share an existing backend connection or start a new
 * one.  Once connected, the EHLO, MAIL FROM and RCPT TO commands have
 * been run; for a shared connection, only the additional RCPT TO is run.
 *
 * @param back The output parameter for the newly created backend.
 *
 * @param front The frontend to which the backend is connected.
 *
 * @param ska Index into the socket addresses front->skas if non-negative.
 *	These addresses define an IPv6 or IPv4 address along with a TCP
 *	port number, and are usually filled with socket_parse() or DNS.
 *	Negative ska values will never match during sharing attempts.
 *
 * @param mail_from The MAIL FROM address to setup.
 *
 * @param rcpt_to The RCPT TO address to setup.
 *
 * @returns True on success, otherwise false with a com_err code in TODO:errno.
 */
bool back_new (struct smtp_back **back, struct smtp_front *front, int ska, const a2id_t *mail_from, const a2id_t *rcpt_to);


/** @brief Connect a backend to a server over a new connection.
 *
 * This is used after back_add() and before it is possible to
 * relay data; it has alternatives in back_connect_share() and
 * back_connect_match().
 *
 * When this procedure fails, the backend structure is unchanged.
 * This facilitates DNS-style iteration, such as over MX records
 * and/or AAAA/A records, in search for an online alternative.
 *
 * @param sa IPv6 or IPv4 ocket address with filled in port number,
 *	usually filled with socket_parse() or with DNS data.
 *
 * @returns True on success, otherwise false with a com_err code in TODO:errno.
 */
bool back_connect_new (struct smtp_back *back, const struct sockaddr_storage *sa);


/** @brief Setup a backend to share the connection of another.
 *
 * This is used after back_new() and is an alternative to
 * back_connect_new().  This is part of back_connect_match().
 *
 * The backend to share from does not have to be a master itself.
 * When this procedure fails, the backend structure is unchanged.
 *
 * This backend does not become a master, because it is not in control
 * of the socket.  Shared backends will skip on various actions,
 * because the master is assumed to handle those; think of DATA
 * statements that are only needed once.  What is needed however,
 * is a RCPT TO statement.  This is sent here, and will lock the
 * frontend.
 *
 * @returns True on success, otherwise false with a com_err code in TODO:errno.
 *	This function however, does not fail.
 */
bool back_connect_share (struct smtp_back *back, struct smtp_back *sharefrom);


/** @brief Try to share with a suitable backend.
 *
 * Given a backend structure, iterate over the other backends
 * under the same frontend, and look for one that matches the
 * MAIL FROM address and the RCPT TO domain, as well as the
 * IPv6 or IPv4 address and the port number.
 *
 * When a matching entry is found, use back_connect_share()
 * to share, and report success.  Otherwise, deliver a fresh
 * connection through back_connect_new().
 *
 * This procedure uses the ska value stored during back_new()
 * for comparison between backends.  Negative values cannot be
 * addressed at all, and will return a failure.  You may then
 * use alternative procedures, such as DNS traversal, to look
 * for a suitable address and call back_connect_new() with it.
 *
 * @param back The backend that wants to connect, and is hoping
 *	to find a matching backend to share.
 *
 * @returns True on success, otherwise false with a com_err code in TODO:errno.
 */
bool back_connect_match (struct smtp_back *back);


#if 0
/** @brief Close a backend, including disconnection from its server.
 *
 * The backend is kept for administration, such as retrieval of the
 * success or failure of its task.
 *
 * This function does not fail.
 */
void back_close (struct smtp_back *back);
#endif


/** @brief Disconnect a backend, including its socket.
 *
 * The backend is kept for administration, such as retrieval of the
 * success or failure of its task.  Only when the backend was setup
 * with back_connect() has it been setup as a master and will the
 * socket be closed.
 *
 * This function does not fail.
 */
void back_disconnect (struct smtp_back *back);


/** @brief Setup a single backend to asynchronously send a command.
 *
 * Send a command to a single backend.  The backend does not
 * have to be a master, but you should mind about calling this
 * before other work on the same master has finished.
 *
 * You should mind SMTP rules for the command, including its
 * length and the trailing "\r\n" characters.
 *
 * To submit a command to all backends, use back_send_cmd_all()
 * instead.
 *
 * @param back The backend through which the data is to be sent.
 *	May be a master, but does not have to be.
 *
 * @param cb Callback routine to call with this backend and a
 *	result string in its cmdbuf.
 *
 * @param ptr Pointer to the data region to send.  The data must
 *	be stable until this returns.
 *
 * @param len Length of the data to send at @a ptr.
 *
 * @returns True when the command was setup, false with a value
 *	in TODO:errno on failure.
 */
bool back_send_cmd_single (EV_P_ struct smtp_back *back, back_cb_command *cb, const uint8_t *ptr, size_t len);


/** @brief Setup all backends to asynchronously send a command.
 *
 * Send this command asynchronously to all master backends,
 * where you should mind about calling this before any other
 * work on the masters has finished.
 *
 * You should mind SMTP rules for the command, including its
 * length and the trailing "\r\n" characters.
 *
 * To submit a command to one backend only, use the function
 * back_send_cmd_single() instead.
 *
 * @param front The front whose backends should all receive
 *	the command.
 *
 * @param cb Callback routine to call for the frontend, while
 *	providing a result string in its cmdbuf.  Note that
 *	this routine can be called at any time during or after
 *	this setup.
 *
 * @param ptr Pointer to the data region to send.  The data must
 *	be stable until this returns, and it must not be stored
 *	in the front end's cmdbuf.
 *
 * @param len Length of the data to send at @a ptr.
 *
 * @returns True when the command was setup, or false with a response
 *	in the backend's cmdbuf on failure.
 */
bool back_send_cmd_all (EV_P_ struct smtp_front *front, front_cb_command *cb, uint8_t *ptr, size_t len);


/** @brief Setup all backends to asynchronously send content data.
 *
 * Send this content data asynchronously to all master backends,
 * where you should mind about calling this before any other
 * work on the masters has finished.
 *
 * The data content may be an intermediate chunk of data, or it
 * may be the final chunk including the "\r\n.\r\n" terminator.
 * The flag lastdata is true when the last block is passed.
 *
 * This function should store either the data buffer and length
 * or the data stored in it for asynchronous relaying.  To avoid
 * changes to the data buffer being passed, a frontend lock is
 * possible.  Such a lock also avoids new blocks from being passed
 * into this call.
 *
 * @param front The front whose backends should all receive
 *	the content data.
 *
 * @param cb Callback routine to call for the frontend, while
 *	providing a result string in its cmdbuf.  Note that
 *	this routine can be called at any time during or after
 *	this function, even before the last block.
 *
 * @param databuf Pointer to the data buffer to send.  The data will
 *	be stable until this returns, but while the frontend is
 *	locked it will also be kept stable, to benefit backends
 *	that do asynchronous processing.
 *
 * @param datalen Length of the data to send at @a databuf.
 *
 * @param lastdata Set to true if this is the last data block,
 *	which ends in the "\r\n.\r\n" sequence.
 *
 * @returns True when the data was setup, or false with a response
 *	in the backend's cmdbuf on failure.
 */
bool back_send_data_all (struct smtp_front *front, const uint8_t *databuf, size_t datalen, bool lastdata);


/** @brief Parse an SMTP command line and invoke the proper callback.
 *
 * @param srv SMTP server control structure, doubles as memory pool.
 * @returns true on success, false with an error setup in cmdbuf on error.
 */
bool command_parse_callback (struct smtp_front *srv);


/** @brief Iterate over zero or more ESMTP command parameters.
 *
 * Given an ESMPT command parameter structure, use this function to
 * iterate over the various KEYWORD=VALUE pairs.  The iterator holds
 * internal fields for iteration, but when this call succceeds there
 * will be values setup in kwbuf, kwbuf for the keyword and valbuf,
 * vallen for the corresponding value; kwbuf and valbuf point into
 * the command line and kwlen and vallen gives the respective length.
 *
 * This routine assumes being setup by command_parse_callback() and
 * being used within the callback invocation made from it.
 *
 * @param srv SMTP server with command that may have parameters.
 * @returns true when a new item was found, false if not.
 */
bool command_parameter_next (struct smtp_front *srv);


/** @brief Parse and pass DATA content, up to but excluding "\r\n.\r\n".
 *
 * @param srv SMTP server control structure.
 * @returns true on success, false with error setup in cmdbuf on error.
 */
bool command_data_content (struct smtp_front *srv);


/** @brief Reset the SMTP response parser.
 *
 * Call this routine after sending a full command, and before invoking the
 * incremental reply_parse_callback() routine for the first time.
 */
void reply_parse_reset (struct smtp_back *back);


/** @brief Callback type for reply_parse_callback().
 *
 * @param back SMTP client backend structure
 * @param beyond Pointer right after the line reported
 * @param lastline Set to true for the last response line
 */
typedef void cb_replyline_t (struct smtp_back *back, const char *beyond, bool lastline);


/** @brief Parse SMTP response text and invoke the proper callback.
 *
 * @param back SMTP client control structure
 * @param buf Buffer holding (the next bit of) the reply
 * @param buflen Lenght of the buffer in @a buf
 * @param cb_replyline Callback routine for every response line, with a
 *		pointer beyond the response, and a flag to signal if this
 *		is the last response line.
 * @param cbdata Is the callback data pointer passed during callbacks.
 *
 * @returns true on success, false with an error setup in cmdbuf on error.
 *		When more parsing is required, then false is returned with
 *		EAGAIN in errno.
 */
bool reply_parse_callback (struct smtp_back *back, char *buf, unsigned buflen, cb_replyline_t cb_replyline);


//TODO// Prototype is developing
int daemonsub (struct cmdparser *args, struct sockaddr_storage *socks, int argc, char *argv []);

/** @} */
