/* Axe SMTP: daemon code
 *
 * The daemon, starting and servicing network requests.
 * Includes the main command.
 * 
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <ev.h>

#include <arpa2/socket.h>
#include <arpa2/com_err.h>
#include <arpa2/except.h>
#include <arpa2/quick-mem.h>
#include <arpa2/identity.h>
#include <arpa2/util/cmdparse.h>


#include "axe.h"



/** TODO: TEMPORARY, WHILE TRANSITIONING ARGS HANDLING.
 */
struct TODO_tmpdata {
	struct sockaddr_storage *cli_sa;
	struct sockaddr_storage *skas;
	struct cmdparser *args;
};



/** Global event loop variable for the daemon.
 */
struct ev_loop *daemon_loop = NULL;


/** @brief Process a new connection to this server.
 *
 * This is a callback from the daemon's event loop.
 *
 * Process a new client by calling accept().  Beware that it
 * is non-blocking; if the client disappeared in the meantime
 * we would find accept() failed, which is quite alright.
 *
 * The main task here is to create a storage space, including
 * an I/O event handler structure, ready for service.
 */
void daemon_newserver (EV_P_ ev_io *srv_io, int revents) {
	//
	// Accept the client, discarding its address
	int cnx = accept (srv_io->fd, NULL, 0);
	if (cnx < 0) {
		goto fail_cnx;
	}
	//
	// Create a memory pool for the server
	struct smtp_front *front = NULL;
	if (!mem_open (sizeof (*front), (void **) &front)) {
		log_errno ("Failed to create new server");
		goto fail_pool;
	}
log_debug ("New server front %p", (void *) front);
	//
	// Application Configuration
	axesmtp_init (front);
	//
	// Application Data Allocation
	if (!mem_alloc (front, front->conf->front_appsize, (void **) &front->appdata)) {
		log_errno ("Failed to create new server appdata");
		goto fail_pool;
	}
	//
	// Initialise the server structure
	front->state = PUT_RESP;
	front->datafree = sizeof (front->databuf);
	//
	// Setup an event handler for connection I/O
	strcpy (front->cmdbuf, "220 axe-smtp.example.com ESPTM Axe SMTP\r\n");
	front->cmdlen = strlen (front->cmdbuf);
	ev_io_init (&front->cnx_io, front_send_resp, cnx, EV_WRITE);
	front->cnx_io.data = front;
	front->client_sockaddr = ((struct TODO_tmpdata *) srv_io->data)->cli_sa;
	front->args = ((struct TODO_tmpdata *) srv_io->data)->args;
	front->skas = ((struct TODO_tmpdata *) srv_io->data)->skas;
	ev_io_start (EV_A_ &front->cnx_io);
	//
	// Return successfully
	return;
	//
	// Cleanup and exit (error should already be logged)
fail_pool:
	close (cnx);
fail_cnx:
	return;
}


/** @brief Process an interrupt signal.
 *
 * This is a callback from the daemon's event loop.
 *
 * Break out of the daemon's event loop.
 */
void daemon_signal (EV_P_ ev_signal *sig, int revents) {
	ev_break (EV_A_ EVBREAK_ALL);
}


/** @brief Daemon subprogram.
 *
 * Parse commandline.  Setup sockets.  Run service loop.
 */
int daemonsub (struct cmdparser *args, struct sockaddr_storage *skas, int argc, char *argv []) {
	bool ok = true;
	//
	// Try to open &3 or if it is absent try the default key file
	int kf = 3;
	struct stat st;
	if (fstat (kf, &st) < 0) {
		kf = open ("/var/lib/arpa2/identity/default.key", O_RDONLY);
	}
	if (kf >= 0) {
		if (!a2id_addkey (kf)) {
			log_errno ("Non-fatal: Failed to load key file from &3 or default.key");
		}
		close (kf);
	}
	//
	// Commandline parsing (there is some room for more subtility)
	if (argc != 5) {
		fprintf (stderr, "Usage: %s listenIP listenPort nexthopIP nexthopPort\n", argv [0]);
		exit (1);
	}
	char *srv_ip   = argv [1];
	char *srv_port = argv [2];
	char *cli_ip   = argv [3];
	char *cli_port = argv [4];
	//
	// Parse the socket addresses
	struct sockaddr_storage srv_sa;
	struct sockaddr_storage cli_sa;
	struct TODO_tmpdata tmpdata;
	ok = ok && socket_parse (srv_ip, srv_port, &srv_sa);
	ok = ok && socket_parse (cli_ip, cli_port, &cli_sa);
	tmpdata.cli_sa = &cli_sa;
	tmpdata.skas   = skas;
	tmpdata.args   = args;
	//
	// Open the server socket
	socket_init ();
	int srv_sox;
	ok = ok && socket_server (&srv_sa, SOCK_STREAM, &srv_sox);
	bool got_srv_socket = ok;
	ok = ok && socket_nonblock (srv_sox);
	if (got_srv_socket) {
		printf ("--\n");
		fflush (stdout);
	} else {
		log_errno ("Server socket failure: address %s port %s", srv_ip, srv_port);
	}
	//
	// Initialise the event loop
	daemon_loop = EV_DEFAULT;
	//
	// Setup the interrupt signal handler
	ev_signal evintr, evterm;
	ev_signal_init (&evintr, daemon_signal, SIGINT );
	ev_signal_init (&evterm, daemon_signal, SIGTERM);
	ev_signal_start (daemon_loop, &evintr);
	ev_signal_start (daemon_loop, &evterm);
	//
	// Setup the event loop
	ev_io srv_io;
	memset (&srv_io, 0, sizeof (srv_io));
	if (got_srv_socket) {
		ev_io_init (&srv_io, daemon_newserver, srv_sox, EV_READ);
		srv_io.data = &tmpdata;
		ev_io_start (daemon_loop, &srv_io);
	}
	//
	// Create the server loop
	if (ok) {
		ev_run (daemon_loop, 0);
	}
	//
	// Cleanup and exit
	ev_signal_stop (daemon_loop, &evterm);
	ev_signal_stop (daemon_loop, &evintr);
	if (got_srv_socket) {
		ev_io_stop (daemon_loop, &srv_io);
		socket_close (srv_sox);
		srv_sox = -1;
	}
	a2id_dropkeys ();
	socket_fini ();
	exit (ok ? 0 : 1);
}
