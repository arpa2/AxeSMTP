/** @defgroup arpa2out AxeSMTP application for ARPA2 Communication Access.
 * @{
 *
 * This is an AxeSMTP application that makes ARPA2 Signed Identities
 * on outgoing email, inasfar as incomplete signatures are sent.
 * This allows the verification in the arpa2in AxeSMTP application
 * on reply email.  The incomplete signature indicates the flags to
 * incorporate into the signature.
 *
 * The key used for the signature should be available to the mailer,
 * but does not have to be stored by individual users.  Rather, users
 * tend to submit through a special port and/or after login to the
 * outgoing SMTP server.  It is this SMTP server that can then use
 * this application before passing out the email.
 *
 * This command runs as a front end SMTP server with any number of
 * SMTP clients to pass the traffic out for varying MAIL FROM names.
 * This is done because the MAIL FROM may be changed depending on
 * the RCPT TO, but SMTP does not support this.  So we need to chop
 * and split our email and scatter the remnants over backends.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl>
 */


#include <stdint.h>
#include <stdbool.h>

#include <errno.h>

#include <ev.h>

#include <arpa2/quick-mem.h>
#include <arpa2/identity.h>
#include <arpa2/rules.h>
#include <arpa2/rules_db.h>
#include <arpa2/access_comm.h>
#include <arpa2/socket.h>
#include <arpa2/util/cmdparse.h>

#include <com_err/arpa2identity.h>
#include <com_err/arpa2access.h>

#include "../axe.h"


/* Note: entries in the same order as HOST/PORT pairs */
enum skas {
	SKA_LISTEN,
	SKA_RELAY
};


bool arpa2out_mail_from (struct smtp_front *front, const char *addrbuf, unsigned addrlen) {
	if (addrbuf == NULL) {
		// Accept empty address
		memset (&front->from, 0, sizeof (front->from));
	} else {
		// if (!mem_alloc (front, sizeof (*from), (void **) &from)) {
		// 	strcpy (front->cmdbuf, "452 Memory exhausted\r\n");
		// 	return false;
		// }
		if (!a2id_parse (&front->from, addrbuf, addrlen)) {
			//
			// We got an address but it is not an ARPA2 Identity
			strcpy (front->cmdbuf, "553 Sender address must be an ARPA2 Identity\r\n");
			return false;
		}
	}
	return true;
}


bool arpa2out_rcpt_to (struct smtp_front *front, const char *addrbuf, unsigned addrlen) {
	//
	// Parse the recipient address as an ARPA2 Identity
	a2id_t to;
	if (!a2id_parse_remote (&to, addrbuf, addrlen)) {
		//
		// We got an address but it is not an ARPA2 Remote Identity
		strcpy (front->cmdbuf, "553 Recipient address must be an ARPA2 Remote Identity\r\n");
		return false;
	}
	//
	// Compute the service key for ARPA2 Communication Access
	// TODO: No database key support yet, that is set to <NONE,0>
	// The default is in access_comm() for ARPA2 Common _after_ 2.1.4
#ifdef TODO_PERMIT_IN_ACCESS_COMM
	//
	// Verify if access is granted by Communication Access
	// and allow updates to the local address &to
	access_comm_level level = access_comm_undefined;
	a2id_t from_actor;
	if (!access_comm (&front->from, &to, NULL, 0, NULL, 0, &from_actor, &level)) {
		//
		// Failed to compute Access Control
		log_errno ("ARPA2 Communication Access was undecisive from <%s> to <%s>", front->from.txt, to.txt);
		strcpy (front->cmdbuf, "451 Currently unable to decide on ARPA2 Communication Access\r\n");
		return false;
	}
	//
	// If Communication Access provided an empty Actor, set the MAIL FROM address
	if (a2act_empty (&from_actor)) {
		memcpy (&from_actor, &front->from, sizeof (from_actor));
	}
	//
	// Handle grey listing
	if (level == access_comm_greylist) {
		//
		// Grey listed response until Communication Access is updated
		//TODO// Pose a challenge of some sort?
		strcpy (front->cmdbuf, "450 Recipient currently greylists sender under ARPA2 Communication Access\r\n");
		return false;
	}
	//
	// Handle anything but white listing
	if (level != access_comm_whitelist) {
		//
		// Not white listed (black listed, honey potted, undefined)
		strcpy (front->cmdbuf, "553 Recipient blocks sender under ARPA2 Communication Access\r\n");
		return false;
	}
#endif
	//
	// Construct a signed version of the MAIL FROM address
	a2id_t from;
	memcpy (&from, &front->from, sizeof (from));
	if (!a2id_sign (&from, a2id_sigdata_base, &to)) {
		//
		// Failure during signature construction
		strcpy (front->cmdbuf, "553 Unable to complete ARPA2 Signed Identity\r\n");
		return false;
	}
	//
	// We got through, and so should this email
	struct smtp_back *back;
	if (!back_new (&back, front, SKA_RELAY, &from, &to)) {
		//
		// Unable to allocate the backend
		strcpy (front->cmdbuf, "452 Memory exhausted\r\n");
		return false;
	}
	if (!back_connect_match (back)) {
		//
		// Unable to setup connection
		strcpy (front->cmdbuf, "450 Connection to backend server failed\r\n");
		return false;
	}
	//
	// Stop processing events until the backend is called
	front_lock (front);
	//
	// Report success
	return true;
}


enum parse_variables {
	VAR_HOST_LISTEN, VAR_PORT_LISTEN,
	VAR_HOST_RELAY,  VAR_PORT_RELAY,
	/* End marker */
	VAR_ENDMARKER
};

#define HOSTPORT_PAIR_1ST VAR_HOST_LISTEN
#define HOSTPORT_PAIR_NUM 2

#define VAL_HOST_LISTEN values [VAR_HOST_LISTEN]
#define VAL_PORT_LISTEN values [VAR_PORT_LISTEN]
#define VAL_HOST_RELAY  values [VAR_HOST_RELAY]
#define VAL_PORT_RELAY  values [VAR_PORT_RELAY]

#define FLAG_HOST_LISTEN (1 << VAR_HOST_LISTEN)
#define FLAG_PORT_LISTEN (1 << VAR_PORT_LISTEN)
#define FLAG_HOST_RELAY  (1 << VAR_HOST_RELAY )
#define FLAG_PORT_RELAY  (1 << VAR_PORT_RELAY )

static const uint32_t combis [] = {
	/* Require port-listen and port-relay */
	FLAG_PORT_LISTEN | FLAG_PORT_RELAY,
	FLAG_PORT_LISTEN | FLAG_PORT_RELAY,
	/* End marker */
	0
};

/* Note: entries in the same order as SKA_ values */
static const struct cmdparse_grammar grammar = {
	.keywords = {
		"host-listen", "port-listen",
		"host-relay",  "port-relay",
	},
	.listwords = 0,
	.combinations = combis,
};

static const char *usage = "Usage: %s [[host-X IP] port-X PORT]...\n"
"  host-/port-listen  Defines the socket for incoming email\n"
"  host-/port-relay   Defines the target for the email delivery relay\n"
"Forwarding only to given ports.  Both port-listen and port-relay are required.\n"
"All host-X default to ::1.  Giving a host-X without its port-X would be silly.\n";


static const struct app_conf arpa2out_conf = {
	.name = "arpa2out",
	.descr = "Complete signature recipes in sender ARPA2 Identity, while passing email without signature recipe",
	.cb_mail_from = arpa2out_mail_from,
	.cb_rcpt_to = arpa2out_rcpt_to,
};


/** @brief Application call to initialise a server structure.
 *
 * This call sets up the application-specific configuration pointer.
 */
void axesmtp_init (struct smtp_front *front) {
	access_init ();
	front->conf = &arpa2out_conf;
}


int main (int argc, char *argv []) {
	//
	// Parse cmdline arguments
	struct cmdparser prs = NEW_CMDPARSER (&grammar);
	bool stxok = cmdparse_kwargs (&prs, argc - 1, argv + 1);
	if (!stxok) {
		fprintf (stderr, usage, argv [0]);
		exit (1);
	}
	//
	// Parse host/port pairs, where #0 is the only server
	bool ok;
	struct sockaddr_storage front2back [HOSTPORT_PAIR_NUM];
	for (int i = 0; i < HOSTPORT_PAIR_NUM; i++) {
		int host = HOSTPORT_PAIR_1ST + 2 * i + 0;
		int port = HOSTPORT_PAIR_1ST + 2 * i + 1;
		//
		// Parse the host/port pair, with suitable defaults
		if (prs.values [port] == NULL) {
			//
			// It is silly to name a host without a port
			if (prs.values [host] != NULL) {
				fprintf (stderr, "It is silly to give no \"%s\" and yet \"%s %s\"\n",
					grammar.keywords [port],
					grammar.keywords [host],
					prs.values [host]);
				exit (1);
			}
			//
			// Clear the address; ss_family 0 signals no address
			memset (&front2back [i], 0, sizeof (*front2back));
		} else {
			//
			// We shall need a host, default to ::1
			if (prs.values [host] == NULL) {
				prs.values [host] = "::1";
			}
			//
			// Parse the socket address and perhaps report errors
			if (!socket_parse (
					prs.values [host],
					prs.values [port],
					&front2back [i])) {
				fprintf (stderr, "Cannot parse \"%s %s %s %s\"\n",
					grammar.keywords [host], prs.values [host],
					grammar.keywords [port], prs.values [port]);
				exit (1);
			}
			log_debug ("Parsed \"%s %s %s %s\" into socket addres [%i] of family %d", grammar.keywords [host], prs.values [host], grammar.keywords [port], prs.values [port], i, front2back [i].ss_family);
		}
	}
	//
	//TODO// Forge the old-style cmdline
	char *argvsub [] = { argv [0],
			prs.VAL_HOST_LISTEN, prs.VAL_PORT_LISTEN,
			prs.VAL_HOST_RELAY,  prs.VAL_PORT_RELAY,
			NULL };
	int argcsub = 5;
	//
	// Invoke the daemon subprogram and never return
	exit (daemonsub (&prs, front2back, argcsub, argvsub));
	return 1;
}

/** @} */

