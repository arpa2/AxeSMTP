/** @defgroup arpa2in AxeSMTP application for ARPA2 Communication Access.
 * @{
 *
 * This is an AxeSMTP application that uses ARPA2 Communication Access
 * as a control mechanism of who may send email.  It involves parsing
 * of the sender as a ARPA2 Remote Identity, parsing recipients as a
 * full ARPA2 Identity.  Any receiving signature is validated against
 * the sender address (signatures involving content data will fail).
 * It returns an error response or simply allows AxeSMTP to relay to
 * the recipient address.
 *
 * This filter always rejects invalid signatures.  Signatures are not
 * always required, however.  Whether they are is expressed in the
 * Communication Access definitions.  This allows the definition of
 * special addresses that can only be addressed in a particular way,
 * such as from a particular sender, or only for some period.
 *
 * The perfect match for this is an outgoing filter in which signing
 * takes place.  This is done for addresses that pass through and
 * that have (at least) a recipe for a signature.
 *
 * This command runs as a front end SMTP server with any number of
 * SMTP clients to pass the traffic out for varying MAIL FROM names.
 * This is done because the MAIL FROM may be changed depending on
 * the RCPT TO, but SMTP does not support this.  So we need to chop
 * and split our email and scatter the remnants over backends.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <errno.h>

#include <ev.h>

#include <arpa2/quick-mem.h>
#include <arpa2/identity.h>
#include <arpa2/rules.h>
#include <arpa2/rules_db.h>
#include <arpa2/access_comm.h>
#include <arpa2/socket.h>
#include <arpa2/util/cmdparse.h>

#include <com_err/arpa2identity.h>
#include <com_err/arpa2access.h>

#include "../axe.h"


/* Note: entries in the same order as HOST/PORT pairs */
enum skas {
	SKA_LISTEN,
	SKA_WHITELIST,
	SKA_ACTORPASS,
	SKA_BLACKLIST,
	SKA_HONEYPOT,
	SKA_GREYLIST
};


bool arpa2in_mail_from (struct smtp_front *front, const char *addrbuf, unsigned addrlen) {
	if (addrbuf == NULL) {
		// Accept empty address
		memset (&front->from, 0, sizeof (front->from));
	} else {
		// if (!mem_alloc (front, sizeof (*from), (void **) &from)) {
		// 	strcpy (front->cmdbuf, "452 Memory exhausted\r\n");
		// 	return false;
		// }
		if (!a2id_parse_remote (&front->from, addrbuf, addrlen)) {
			//
			// We got an address but it is not an ARPA2 Remote Identity
			// Note: This basically covers any reasonable address
			strcpy (front->cmdbuf, "553 Sender address must be an ARPA2 Remote Identity\r\n");
			return false;
		}
	}
	return true;
}


bool arpa2in_rcpt_to (struct smtp_front *front, const char *addrbuf, unsigned addrlen) {
	//
	// Parse the recipient address as an ARPA2 Identity
	a2id_t to;
	if (!a2id_parse (&to, addrbuf, addrlen)) {
		//
		// We got an address but it is not an ARPA2 Identity
		strcpy (front->cmdbuf, "553 Recipient address must be an ARPA2 Identity\r\n");
		return false;
	}
	//
	// Compute the service key for ARPA2 Communication Access
	// TODO: No database key support yet, that is set to <NONE,0>
	// The default is in access_comm() for ARPA2 Common _after_ 2.1.4
	//
	// Verify signature in the recipient address -- without requiring any
	if (!a2id_verify (&to, a2id_sigdata_base, &front->from)) {
		//
		// Identity has an invalid signature
		char *resp = "553 Recipient address enforces another context\r\n";
		if (errno == A2ID_ERR_EXPIRED) {
			resp = "553 Recipient address is an expired ARPA2 Signed Identity\r\n";
		}
		strcpy (front->cmdbuf, resp);
		return false;
	}
	//
	// Verify if access is granted by Communication Access
	// and allow updates to the local address &to
	access_comm_level level = access_comm_undefined;
	a2id_t from_actor;
	log_debug ("ARPA2 Communication Access tested from <%s> to <%s>", front->from.txt, to.txt);
	if (!access_comm (&front->from, &to, NULL, 0, NULL, 0, &level, &from_actor)) {
		//
		// Failed to compute Access Control
		log_errno ("ARPA2 Communication Access was undecisive from <%s> to <%s>", front->from.txt, to.txt);
		strcpy (front->cmdbuf, "451 Currently unable to decide on ARPA2 Communication Access\r\n");
		return false;
	}
	//
	// If Communication Access provided an empty Actor, set the MAIL FROM address
	bool no_actor = a2act_isempty (&from_actor);
	if (no_actor) {
		memcpy (&from_actor, &front->from, sizeof (from_actor));
	}
	//
	// Choose a backend based on the provided listing
	int backska = -1;
	char *resp = NULL;
	switch (level) {
	case access_comm_greylist:
		//
		// Redirect to greylisting backend if one is defined
		backska = SKA_GREYLIST;
		//
		// Give a response when the backend is not defined
		resp = "450 Recipient currently greylists sender under ARPA2 Communication Access\r\n";
		break;
	case access_comm_blacklist:
		//
		// Redirect to blacklisting backend if one is defined
		backska = SKA_BLACKLIST;
		//
		// Usually there will be no black-listing backend
		// and the default message will be sent back
		break;
	case access_comm_whitelist:
		//
		// Redirect to whitelisting backend if one is defined
		// except when the actor backend is more appropriate
		if (no_actor) {
			backska = SKA_WHITELIST;
		} else if (!front_defines_back (front, SKA_ACTORPASS)) {
			backska = SKA_WHITELIST;
		} else {
			backska = SKA_ACTORPASS;
		}
		//
		// The whitelisted backend is always defined
		// so no reason to setup a message for otherwise
		break;
	case access_comm_honeypot:
		//
		// Redirect to honeypotting backend if one is defined
		backska = SKA_HONEYPOT;
		//
		// Give the default response when the backend is not defined
		// so there can be no distinction from blacklisting
		break;
	case access_comm_undefined:
		break;
	}
	if ((backska <= 0) || (!front_defines_back (front, backska))) {
		if (resp == NULL) {
			//
			// Use a default response for blacklist and honeypot
			resp = "553 Recipient blocks sender under ARPA2 Communication Access\r\n";
		}
		strcpy (front->cmdbuf, resp);
		return false;
	}
	//
	// We got through, and so should this email
	struct smtp_back *back;
	if (!back_new (&back, front, backska, &from_actor, &to)) {
		//
		// Unable to allocate the backend
		strcpy (front->cmdbuf, "452 Memory exhausted\r\n");
		return false;
	}
	if (!back_connect_match (back)) {
		//
		// Unable to setup connection, shared to the white list
		strcpy (front->cmdbuf, "451 Connection to backend server failed\r\n");
		return false;
	}
	//
	// Stop processing events until the backend is called
	front_lock (front);
	//
	// Report success
	return true;
}


enum parse_variables {
	VAR_HOST_LISTEN, VAR_PORT_LISTEN,
	VAR_HOST_WHITE,  VAR_PORT_WHITE,
	VAR_HOST_ACTOR,  VAR_PORT_ACTOR,
	VAR_HOST_BLACK,  VAR_PORT_BLACK,
	VAR_HOST_HONEY,  VAR_PORT_HONEY,
	VAR_HOST_GREY,   VAR_PORT_GREY,
	/* End marker */
	VAR_ENDMARKER
};

#define HOSTPORT_PAIR_1ST VAR_HOST_LISTEN
#define HOSTPORT_PAIR_NUM 6

#define VAL_HOST_LISTEN values [VAR_HOST_LISTEN]
#define VAL_PORT_LISTEN values [VAR_PORT_LISTEN]
#define VAL_HOST_WHITE  values [VAR_HOST_WHITE]
#define VAL_PORT_WHITE  values [VAR_PORT_WHITE]
#define VAL_HOST_ACTOR  values [VAR_HOST_ACTOR]
#define VAL_PORT_ACTOR  values [VAR_PORT_ACTOR]
#define VAL_HOST_BLACK  values [VAR_HOST_BLACK]
#define VAL_PORT_BLACK  values [VAR_PORT_BLACK]
#define VAL_HOST_HONEY  values [VAR_HOST_HONEY]
#define VAL_PORT_HONEY  values [VAR_PORT_HONEY]
#define VAL_HOST_GREY   values [VAR_HOST_GREY ]
#define VAL_PORT_GREY   values [VAR_PORT_GREY ]

#define FLAG_HOST_LISTEN (1 << VAR_HOST_LISTEN)
#define FLAG_PORT_LISTEN (1 << VAR_PORT_LISTEN)
#define FLAG_HOST_WHITE  (1 << VAR_HOST_WHITE )
#define FLAG_PORT_WHITE  (1 << VAR_PORT_WHITE )
#define FLAG_HOST_ACTOR  (1 << VAR_HOST_ACTOR )
#define FLAG_PORT_ACTOR  (1 << VAR_PORT_ACTOR )
#define FLAG_HOST_BLACK  (1 << VAR_HOST_BLACK )
#define FLAG_PORT_BLACK  (1 << VAR_PORT_BLACK )
#define FLAG_HOST_HONEY  (1 << VAR_HOST_HONEY )
#define FLAG_PORT_HONEY  (1 << VAR_PORT_HONEY )
#define FLAG_HOST_GREY   (1 << VAR_HOST_GREY  )
#define FLAG_PORT_GREY   (1 << VAR_PORT_GREY  )

static const uint32_t combis [] = {
	/* Require port-listen and port-white */
	FLAG_PORT_LISTEN | FLAG_PORT_WHITE,
	FLAG_PORT_LISTEN | FLAG_PORT_WHITE,
	/* End marker */
	0
};

/* Note: entries in the same order as SKA_ values */
static const struct cmdparse_grammar grammar = {
	.keywords = {
		"host-listen", "port-listen",
		"host-white",  "port-white",
		"host-actor",  "port-actor",
		"host-black",  "port-black",
		"host-honey",  "port-honey",
		"host-grey",   "port-grey",
	},
	.listwords = 0,
	.combinations = combis,
};

static const char *usage = "Usage: %s [[host-X IP] port-X PORT]...\n"
"  host-/port-listen  Defines the socket for incoming    email\n"
"  host-/port-white   Defines the target for whitelisted email\n"
"  host-/port-actor   Defines the target for whitelisted email from an actor\n"
"  host-/port-black   Defines the target for blacklisted email\n"
"  host-/port-honey   Defines the target for honeypotted email\n"
"  host-/port-grey    Defines the target for greylisted  email\n"
"Forwarding only to given ports.  Both port-listen and port-white are required.\n"
"All host-X default to ::1.  Giving a host-X without its port-X would be silly.\n";


static const struct app_conf arpa2in_conf = {
	.name = "arpa2in",
	.descr = "Validate recipient as an ARPA2 Identity, verify any recipient address signatures against the context of time and sender address, and apply Communication Access where a recipient may require sender patterns as well as signatures that assure certain parts of the context; depending on Access Control, deliver to a variety of backends",
	.cb_mail_from = arpa2in_mail_from,
	.cb_rcpt_to = arpa2in_rcpt_to,
};


/** @brief Application call to initialise a server structure.
 *
 * This call sets up the application-specific configuration pointer.
 */
void axesmtp_init (struct smtp_front *front) {
	access_init ();
	front->conf = &arpa2in_conf;
}


int main (int argc, char *argv []) {
	//
	// Parse cmdline arguments
	struct cmdparser prs = NEW_CMDPARSER (&grammar);
	bool stxok = cmdparse_kwargs (&prs, argc - 1, argv + 1);
	if (!stxok) {
		fprintf (stderr, usage, argv [0]);
		exit (1);
	}
	//
	// Parse host/port pairs, where #0 is the only server
	bool ok;
	struct sockaddr_storage front2back [HOSTPORT_PAIR_NUM];
	for (int i = 0; i < HOSTPORT_PAIR_NUM; i++) {
		int host = HOSTPORT_PAIR_1ST + 2 * i + 0;
		int port = HOSTPORT_PAIR_1ST + 2 * i + 1;
		//
		// Parse the host/port pair, with suitable defaults
		if (prs.values [port] == NULL) {
			//
			// It is silly to name a host without a port
			if (prs.values [host] != NULL) {
				fprintf (stderr, "It is silly to give no \"%s\" and yet \"%s %s\"\n",
					grammar.keywords [port],
					grammar.keywords [host],
					prs.values [host]);
				exit (1);
			}
			//
			// Clear the address; ss_family 0 signals no address
			memset (&front2back [i], 0, sizeof (*front2back));
		} else {
			//
			// We shall need a host, default to ::1
			if (prs.values [host] == NULL) {
				prs.values [host] = "::1";
			}
			//
			// Parse the socket address and perhaps report errors
			if (!socket_parse (
					prs.values [host],
					prs.values [port],
					&front2back [i])) {
				fprintf (stderr, "Cannot parse \"%s %s %s %s\"\n",
					grammar.keywords [host], prs.values [host],
					grammar.keywords [port], prs.values [port]);
				exit (1);
			}
			log_debug ("Parsed \"%s %s %s %s\" into socket addres [%i] of family %d", grammar.keywords [host], prs.values [host], grammar.keywords [port], prs.values [port], i, front2back [i].ss_family);
		}
	}
	//
	//TODO// Forge the old-style cmdline
	char *argvsub [] = { argv [0],
			prs.VAL_HOST_LISTEN, prs.VAL_PORT_LISTEN,
			prs.VAL_HOST_WHITE,  prs.VAL_PORT_WHITE,
			NULL };
	int argcsub = 5;
	//
	// Invoke the daemon subprogram and never return
	exit (daemonsub (&prs, front2back, argcsub, argvsub));
	return 1;
}

/** @} */

