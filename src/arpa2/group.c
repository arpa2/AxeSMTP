/** @defgroup arpa2group AxeSMTP application for ARPA2 Group Iteration.
 * @{
 *
 * This is an AxeSMTP application using ARPA2 Group logic to relay
 * an email to group members.  This is the behaviour when the sender
 * is a member of the group and for destinations describing all or
 * some members of a group.  When the sender (minus his member alias)
 * turns out not to be a group, then the traffic is rejected or, if
 * a plain backend is defined, forwarded to that backend.  Recipient
 * addresses that are not group members are also passed through that
 * same plain backend or rejected when it is not defined.
 *
 * Normally, the traffic entering group handling has been prepared
 * by the -in- application of AxeSMTP, and the sender address has
 * been replaced with the group member address in response to the
 * Access Rule that passes the email to the group.  The -in- daemon
 * will then make a decision to relay traffic through this -group-
 * process.  If the sender turns out to be a different kind of actor
 * than a group member, then passing on the address through the
 * plain backend should be the proper response.
 *
 * Groups have an email address like any plain user.  Single aliases
 * are added to form an Actor Identity for the various Group Members,
 * both for members who send to a group and in recipient addresses.
 * So, a <group>@<domain> has any number of <group>+<member>@<domain>.
 * As a side-effect of access_comm() in the arpa2in component, there
 * will be an Actor Identity that is specific for the recipient, and
 * if that recipient is a group address then the Actor Identity would
 * provide the Group Member name.  To that end, an attribute provided
 * "=g<group>+<member>" is setup in the Rule in Communication Access.
 *
 * Given that the MAIL FROM address is now an Actor of a Group Member,
 * it is possible to derive both the <group> and <member> value, and
 * to test if the same <group> and @<domain> are used in the RCPT TO
 * address; if so, then communication to the group is found (this is
 * the only sensible situation for a group, but it helps to recognise
 * potential group traffic; a later database lookup with confirm this
 * more definitively).
 *
 * Knowing that the <member> holds no '+' symbol and through that
 * learning the only possible <group> name, it is now possible to split
 * the RCPT TO as <group><filter>@<domain> where <filter> may be empty
 * to address all listening <group>@<domain> members, or it may start
 * with a '+' symbol and continue to name members, and/or '-' switches
 * between additive and subtractive modes.  This material is used in
 * the group_iterate() call as filters.  It is generally useful to
 * collect all filters for all recipients, so as to avoid sending the
 * same information more than once.  When all RCPT TO addresses from
 * to same MAIL FROM and destined for the same group are combined,
 * and their <filter> strings supplied in one call to group_iterate(),
 * then the least risk remains of duplicate emails at any recipient.
 * This is due to the iteration over the group members and matching
 * them against <filter> strings, rather than unfolding the latter
 * and than pushing out emails independently.
 *
 * The group_iterate() call triggers a callback for any message to
 * be sent, and in doing so it provides the Actor Identities, so
 * the Member Group Identities, of the sender and recipient.  It
 * also provides a delivery address of the form <user>@<domain>
 * that can be used for email delivery, and so as the envelope in
 * outgoing RCPT TO invocations.
 *
 * Group members are setup with %MARKS that flag how they should be
 * used in a number of uses.  For an email list, the following marks
 * matter:
 *
 *   - GROUP_RECV marks members to receive mail from other members
 *   - GROUP_VISITOR marks members to receive mail from non-members
 *
 * Note that visitors should use a dynamic address in the MAIL FROM
 * address, so they too ue an Actor Identity that is a Group Member.
 * Considering the RCPT TO address as a group address with an empty
 * <filter> part allows the lookup and iteration of a group.  It is
 * up to Communication Access who may visit a group.
 *
 * Groups are stored in the Rules DB with their own Rule Type and,
 * as a result, with their own Domain/Service Keys.  As a result it
 * is possible to keep it separately owned from Communication Access.
 * This serves the privacy of the iteration, and makes it only
 * available to a tool like this.  The Access Name used in the
 * lookup is the <group> name and the "a2rule group" subcommand
 * manages the Rule DB from this logic.
 *
 * This command runs as a post-queue SMTP/LMTP server with any number of
 * SMTP/LMTP clients to pass the traffic out for varying RCPT TO names.
 * Note that LMTP is able to report success or failure for each of the
 * addressed group members separately.  It will not return their delivery
 * address but their Actor Identity, so the Group Member name.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl>
 */


#include <stdint.h>
#include <stdbool.h>

#include <errno.h>

#include <ev.h>

#include <arpa2/quick-mem.h>
#include <arpa2/identity.h>
#include <arpa2/rules.h>
#include <arpa2/rules_db.h>
// #include <arpa2/access_comm.h>
#include <arpa2/group.h>
#include <arpa2/socket.h>
#include <arpa2/util/cmdparse.h>

#include <com_err/arpa2identity.h>
#include <com_err/arpa2access.h>

#include "../axe.h"


/* Note: entries in the same order as HOST/PORT pairs */
enum skas {
	SKA_LISTEN,
	SKA_MEMBER,
	SKA_PLAIN
};


struct memberfilter {
	struct memberfilter *next;
	a2act_t member;
};

struct appdata_front {
	struct memberfilter *members;
	membuf deliverybuf;
	unsigned deliverycnt;
	unsigned deliverycrs;
	bool   deliveryoom;
	bool support_group, support_plain;
};



/** @brief Process MAIL FROM and determine if it is a group member address.
 *
 * The task of this -group- daemon is to iterate groups; anything that is
 * not group traffic is passed through the plain backend or rejected if it
 * is undefined.  Group traffic is detected by looking up if the MAIL FROM
 * address is a group member with the ARPA2 Common call group_hasmember().
 *
 * The group_hasmember() call returns GROUP_MARKS, and they can be used
 * to provide feedback when insufficient rights exist to send to a group.
 *
 * Once the MAIL FROM address is established as a group address, further
 * RCPT TO addresses can be compared to contain the group address with
 * zero or more aliases.  If this is the case, then they will be collected
 * for group iteration during the DATA phase; otherwise, traffic will be
 * considered non-group traffic, and passed to the plain backend.
 *
 * The assumption underlying this logic is that the MAIL FROM address is
 * already a Group Member address, not the delivery address of a member.
 * This is normally assured through a preceding -in- daemon that finds a
 * rule in Communication Access that sets an Actor Identity that hints at
 * relaying through this daemon.  If only an Actor Identity can be used as
 * the MAIL FROM address to this -group- daemon, then the plain backend
 * would receive all non-group actor identities, and be able to treat those
 * cleverly.
 *
 * This phase sets a support_group flag when the MAIL FROM address was
 * found to be an ARPA2 Group, and it sets a support_plain flag when the
 * backend for plain is installed.
 */
bool arpa2group_mail_from (struct smtp_front *front, const char *addrbuf, unsigned addrlen) {
	bool maybe_group = false;
	//
	// Parse to find hints that this may be a Group Identity
	if (addrbuf == NULL) {
		/* Accept empty address, but it is certainly no group */
		memset (&front->from, 0, sizeof (front->from));
	} else if (a2act_parse (&front->from, addrbuf, addrlen, 1)) {
		/* This may indeed be a group, investigate further */
		maybe_group = true;
	}
	//
	// Given a hint that this may be a group member, look it up
	group_marks sender_marks;
	if (!group_hasmember (&front->from,
			NULL, 0,	/* default service key for domain */
			NULL, 0,	/* no contextually defined rules */
			&sender_marks)) {
		//
		// The group suspected in the sender could not be found
		maybe_group = false;
		if (errno != A2XS_ERR_MISSING_RULESET) {
			log_errno ("ARPA2 Group exists but not the Member claimed in MAIL FROM:<%.*s>", addrlen, addrbuf);
		}
	} else if ((sender_marks & GROUP_SEND) == 0) {
		//
		// Member not known to Group, or with insufficient permissions
		log_debug ("Group Member %s only has rights %x", front->from.txt, sender_marks);
		strcpy (front->cmdbuf, "553 You do not have the rights to send to this ARPA2 Group\r\n");
		return false;
	}
	//
	// Decide whether non-group traffic is permitted
	if (front_defines_back (front, SKA_PLAIN)) {
		front->appdata->support_plain = true;
	} else if (!maybe_group) {
		strcpy (front->cmdbuf, "553 Sender address must be an ARPA2 Group Member\r\n");
		return false;
	}
	//
	// This looked like a group and did not fail, so conclude it is a group
	front->appdata->support_group = maybe_group;
	//
	// Continue working
	return true;
}


/** @brief Process RCPT TO as messages for the group or the plain backen.d
 *
 * When the sender is a group, the flag support_group is set, and a
 * comparison is made if the group identity is in the recipient address.
 * If this is the case, the address is saved for group iteration during
 * the DATA phase.  This allows iterations over all RCPT TO addresses
 * aimed at the group at the same time, to avoid multiple deliveries.
 *
 * If group iteration is not scheduled, then the recipient address is
 * treated as plain traffic.  This is refused when support_plain was
 * not flagged during the MAIL TO phase.  Otherwise, one shared backend
 * connection to the plain backend is made, and each of the RCPT TO is
 * subsequently added there, as a content relay for the DATA phase.
 */
bool arpa2group_rcpt_to (struct smtp_front *front, const char *addrbuf, unsigned addrlen) {
	//
	// Parse the recipient address as minimal ARPA2 Actor which we will later
	// correct by copying A2ID_OFS_PLUS_ALIASES and A2ID_OFS_ALIASES if it
	// turns out to be more extended than the mere group address.
	a2act_t to;
	if (!a2act_parse (&to, addrbuf, addrlen, 0)) {
		//
		// We got an address but it is not an ARPA2 Identity
		strcpy (front->cmdbuf, "553 Recipient address must be an ARPA2 Group Member\r\n");
		return false;
	}
	//
	// Check that the group and domain are the same as in the sender
	bool same = true;
	same = same && (0 == strcmp (
			front->from.txt + front->from.ofs [A2ID_OFS_AT_DOMAIN],
			to         .txt + to         .ofs [A2ID_OFS_AT_DOMAIN]));
	unsigned aliofs = front->from.ofs [A2ID_OFS_PLUS_ALIASES];
	same = same && (0 == strncmp (front->from.txt, to.txt, aliofs));
	same = same && ((to.txt [aliofs] == '+') || (to.txt [aliofs] == '@'));
	//
	// If not targeted for the group, consider the plain backend
	if (!same) {
		//
		// Refuse non-group traffic without a plain backend
		if (!front->appdata->support_plain) {
			/* Reject plain traffic */
			strcpy (front->cmdbuf, "553 Sender and recipient address must be in the same ARPA2 Group\r\n");
			return false;
		}
		//
		// Setup or share the plain backend connection
		struct smtp_back *back;
		if (!back_new (&back, front, SKA_PLAIN, &front->from, &to)) {
			//
			// Unable to allocate the backend
			strcpy (front->cmdbuf, "452 Memory exhausted\r\n");
			return false;
		}
		if (!back_connect_match (back)) {
			//
			// Unable to setup connection, shared to the white list
			strcpy (front->cmdbuf, "451 Connection to backend server failed\r\n");
			return false;
		}
		//
		// Stop processing events until the backend is called
		front_lock (front);
		//
		// Report success
		return true;
	}
	//
	// Allocate memory and store the recipient for later
	struct memberfilter *newmem = NULL;
	if (!mem_alloc (front, sizeof (struct memberfilter), (void **) &newmem)) {
		strcpy (front->cmdbuf, "452 Insufficient storage for recipient\r\n");
		return false;
	}
	memcpy (&newmem->member, &to, sizeof (a2act_t));
	newmem->next = front->appdata->members;
	front->appdata->members = newmem;
	//
	// Report success
	strcpy (front->cmdbuf, "250 Recipient enlisted for ARPA2 Group handling\r\n");
	return false;
}


/** @brief Callback made for every Group Member to deliver to.
 *
 * Group Iteration yields members along with their delivery address.
 * We need to add a RCPT TO for each of these delivery addresses.
 *
 * We cannot stay asynchronous with the current synchronous ARPA2 Common.
 * Store every delivery address, so we can trigger asynchronous delivery
 * of RCPT TO commands to the backend after iteration completes.
 */
bool arpa2group_member_cb (void *updata, group_marks marks,
				const a2act_t *sender, const a2act_t *recipient,
				const char *delivery, unsigned deliverylen) {
	//
	// Initialise
	struct appdata_front *appdata = ((struct smtp_front *) updata)->appdata;
	//
	// Skip when already out of memory
	if (appdata->deliveryoom) {
		//
		// Continue iteration
		return true;
	}
	//
	// Report for logging
	log_debug ("ARPA2 Group Iteration sender=%s, recipient=%s",
			sender->txt, recipient->txt);
	//
	// Have enough storage space for the new delivery address + NUL char
	// Note: The buffer may move to another address to find more space
	size_t newlen = sizeof (a2id_t) * (1 + appdata->deliverycnt);
	if (!mem_buffer_resize (updata, newlen, &appdata->deliverybuf)) {
		appdata->deliveryoom = true;
		//
		// Return to continue iteration
		return true;
	}
	//
	// Parse the delivery address syntax into the buffer space
	// Note: the buffer resize may have moved it to another address
	a2id_t *rcptto = &((a2id_t *) appdata->deliverybuf.bufptr) [appdata->deliverycnt++];
	if (!a2id_parse_remote (rcptto, delivery, deliverylen)) {
		//
		// Log an invalid delivery address, then continue Iteration
		log_errno ("Group Member %s setup with invalid delivery address %.*s",
			recipient->txt, deliverylen, delivery);
		return true;
	}
	//
	// Return to continue iteration
	return true;
}


/** @brief Iterate over delivery addresses in the pace allowed by the
 * member backend.
 *
 * The member backend is a single connection over which all delivery
 * as RCPT TO addresses occurs.  The addresses were stored as delivery
 * addresses before, as a result of Group Iteration.
 *
 * The backend is asynchronously sent RCPT TO commands and responses
 * are processed.  Individual failures are not considered harmful,
 * and logged but not cause for failure of the overall transaction;
 * this implements a more relaxed transactional model for ARPA2 Groups
 * than for individual delivery.  A better implementation might report
 * back on the members that got no successfully delivery, and why, in
 * a submission report email (but only when something went wrong).
 *
 * Until the last RCPT TO command has reported back, the frontend
 * remains locked.  It is locked before this call is made, and will
 * be unlocked by this call when it finally returns.  It is assumed
 * part of an infinite loop until it reaches this final return.
 */
void send_delivery_cursor (struct smtp_front *front) {
	//
	// Find the last cursor not sent (we send in reverse order)
	unsigned newcrs = front->appdata->deliverycrs++;
	//
	// Create a new backend for delivery to group members
	a2id_t *rcptto = &((a2id_t *) front->appdata->deliverybuf.bufptr) [newcrs];
	struct smtp_back *back = NULL;
	if (!back_new (&back, front, SKA_MEMBER, &front->from, rcptto)) {
		//
		// Unable to allocate memory
		static const char *backfail = "452 Memory exhausted while adding member\r\n";
		front_unlock (front, backfail, strlen (backfail));
		return;
	}
	//
	// Invoke the application callback after RCPT TO was sent and replied
	void sent_delivery_cursor (struct smtp_back *back);
	back->cb_sent = sent_delivery_cursor;
	//
	// Connect the backend -- a first/fresh or later/shared connection
	if (!back_connect_match (back)) {
		//
		// Unable to setup connection
		static const char *backfail = "450 Connection to backend server failed\r\n";
		front_unlock (front, backfail, strlen (backfail));
		return;
	}
	//
	// Done; keep the frontend locked for continued iterations
}


/** @brief Callback after RCPT TO was sent.  Next, send another RCPT TO
 * or continue into the DATA phase.
 */
void sent_delivery_cursor (struct smtp_back *back) {
	//
	// Initialise
	struct smtp_front *front = back->frontend;
	//
	// Do not trigger another upcall after DATA for this backend
	back->cb_sent = NULL;
	//
	// Send another RCPT TO if the stack is not empty yet
	if (front->appdata->deliverycrs < front->appdata->deliverycnt) {
		send_delivery_cursor (front);
		return;
	}
	//
	// From now on, we intend to send the DATA command
	// Note: This includes the plain backend, if it is connected
	front->intention_data = true;
	//
	// Send a DATA command to connected backends, member and plain
	static uint8_t *datacmd = "DATA\r\n";
	if (back_send_cmd_all (EV_DEFAULT_ front, NULL, datacmd, strlen (datacmd))) {
		back->state = SEND_DATACMD;
		//
		// We got a new lock; release the one we had
		front_unlock (front, NULL, 0);
		//
		// Return sending control to the event loop
	} else {
		//
		// We failed to start the new command, report to the frontend
		const char *msg = "544 Failed to send DATA command to member and optional plain backends\r\n";
		front_unlock (front, msg, strlen (msg));
	}
}


/** @brief Relay DATA into backends, after preparing one for the sender's
 * ARPA2 Group.
 *
 * This combines all filter strings and makes one call to group_iterate().
 * The callbacks from this iteration then launch backends for the various
 * delivery addresses.  When all this succeeds, DATA returns the customary
 * "354 Mail Input", but it may also return "554 Transaction Failed" now.
 */
bool arpa2group_data (struct smtp_front *front) {
	//
	// Count the number of filters
	unsigned numflt = 0;
	for (struct memberfilter *mf = front->appdata->members;
			mf != NULL; mf = mf->next) {
		numflt++;
	}
	//
	// Create the filter list
	const char *filters [numflt + 1];
	numflt = 0;
	for (struct memberfilter *mf = front->appdata->members;
			mf != NULL; mf = mf->next) {
		filters [numflt++] = mf->member.txt;
	}
	filters [numflt] = NULL;
	//
	// Set the required and forbidden marks
	group_marks required  = GROUP_SEND;
	group_marks forbidden = 0;
	//
	// Prepare a buffer to collect delivery addresses
	front->appdata->deliverycnt = 0;
	bool gotbuf = mem_buffer_open (front, 100, &front->appdata->deliverybuf);
	front->appdata->deliveryoom = !gotbuf;
	//
	// Lock the frontend, to trigger the frontend when ready
	front_lock (front);
	//
	// Iterate over the group to collect delivery addresses
	if (!group_iterate (&front->from, filters,
			required, forbidden,
			NULL, 0,	/* default service key for domain */
			NULL, 0,	/* no contextually defined rules */
			arpa2group_member_cb, front)) {
		log_errno ("Group Iteration failed for sender <%s>", front->from.txt);
		strcpy (front->cmdbuf, "554 Failed during ARPA2 Group Iteration\r\n");
		front_unlock (front, NULL, 0);
		return false;
	}
	//
	// Close off the delivery buffer at its current size
	mem_buffer_close (front, sizeof (a2id_t) * front->appdata->deliverycnt, &front->appdata->deliverybuf);
	//
	// Give an error if Group Iteration was unsuccessful
	if (front->appdata->deliverycnt == 0) {
		strcpy (front->cmdbuf, "554 Recipient addressing did not reach any ARPA2 Group Members\r\n");
		front_unlock (front, NULL, 0);
		return false;
	} else if (front->appdata->deliveryoom) {
		strcpy (front->cmdbuf, "554 Out of memory while delivering to ARPA2 Group\r\n");
		front_unlock (front, NULL, 0);
		return false;
	}
	//
	// Suppress DATA command relaying to new backends until RCPT TO done
	front->intention_data = false;
	//
	// Iterate over the collected delivery addresses, to send recipients
	// The frontend will be locked until the last recipient was delivered
	front->appdata->deliverycrs = 0;
	send_delivery_cursor (front);
	//
	// Return success -- locked frontend until the last delivery address
	return true;
}


enum parse_variables {
	VAR_HOST_LISTEN, VAR_PORT_LISTEN,
	VAR_HOST_MEMBER, VAR_PORT_MEMBER,
	VAR_HOST_PLAIN,  VAR_PORT_PLAIN,
	/* End marker */
	VAR_ENDMARKER
};

#define HOSTPORT_PAIR_1ST VAR_HOST_LISTEN
#define HOSTPORT_PAIR_NUM 3

#define VAL_HOST_LISTEN values [VAR_HOST_LISTEN]
#define VAL_PORT_LISTEN values [VAR_PORT_LISTEN]
#define VAL_HOST_MEMBER values [VAR_HOST_MEMBER]
#define VAL_PORT_MEMBER values [VAR_PORT_MEMBER]
#define VAL_HOST_PLAIN  values [VAR_HOST_PLAIN]
#define VAL_PORT_PLAIN  values [VAR_PORT_PLAIN]

#define FLAG_HOST_LISTEN (1 << VAR_HOST_LISTEN)
#define FLAG_PORT_LISTEN (1 << VAR_PORT_LISTEN)
#define FLAG_HOST_MEMBER (1 << VAR_HOST_MEMBER)
#define FLAG_PORT_MEMBER (1 << VAR_PORT_MEMBER)
#define FLAG_HOST_PLAIN  (1 << VAR_HOST_PLAIN )
#define FLAG_PORT_PLAIN  (1 << VAR_PORT_PLAIN )

static const uint32_t combis [] = {
	/* Require port-listen and port-member */
	FLAG_PORT_LISTEN | FLAG_PORT_MEMBER,
	FLAG_PORT_LISTEN | FLAG_PORT_MEMBER,
	/* End marker */
	0
};

/* Note: entries in the same order as SKA_ values */
static const struct cmdparse_grammar grammar = {
	.keywords = {
		"host-listen", "port-listen",
		"host-member", "port-member",
		"host-plain",  "port-plain",
	},
	.listwords = 0,
	.combinations = combis,
};

static const char *usage = "Usage: %s [[host-X IP] port-X PORT]...\n"
"  host-/port-listen  Defines the socket for incoming email\n"
"  host-/port-member  Defines the target for group member email\n"
"  host-/port-plain   Defines the target for non-member email\n"
"Forwarding only to given ports.  Both port-listen and port-member are required.\n"
"All host-X default to ::1.  Giving a host-X without its port-X would be silly.\n";


static const struct app_conf arpa2group_conf = {
	.name = "arpa2group",
	.descr = "When the recipient address is a group and the sender is a member of that group, then iterate over the group members and apply filters to distribute the email over group members; non-member email may be proxied via a separate backend",
	.cb_mail_from = arpa2group_mail_from,
	.cb_rcpt_to = arpa2group_rcpt_to,
	.cb_data = arpa2group_data,
	.front_appsize = sizeof (struct appdata_front),
};


/** @brief Application call to initialise a server structure.
 *
 * This call sets up the application-specific configuration pointer.
 */
void axesmtp_init (struct smtp_front *front) {
	access_init ();
	group_init ();
	front->conf = &arpa2group_conf;
}


int main (int argc, char *argv []) {
	//
	// Parse cmdline arguments
	struct cmdparser prs = NEW_CMDPARSER (&grammar);
	bool stxok = cmdparse_kwargs (&prs, argc - 1, argv + 1);
	if (!stxok) {
		fprintf (stderr, usage, argv [0]);
		exit (1);
	}
	//
	// Parse host/port pairs, where #0 is the only server
	bool ok;
	struct sockaddr_storage front2back [HOSTPORT_PAIR_NUM];
	for (int i = 0; i < HOSTPORT_PAIR_NUM; i++) {
		int host = HOSTPORT_PAIR_1ST + 2 * i + 0;
		int port = HOSTPORT_PAIR_1ST + 2 * i + 1;
		//
		// Parse the host/port pair, with suitable defaults
		if (prs.values [port] == NULL) {
			//
			// It is silly to name a host without a port
			if (prs.values [host] != NULL) {
				fprintf (stderr, "It is silly to give no \"%s\" and yet \"%s %s\"\n",
					grammar.keywords [port],
					grammar.keywords [host],
					prs.values [host]);
				exit (1);
			}
			//
			// Clear the address; ss_family 0 signals no address
			memset (&front2back [i], 0, sizeof (*front2back));
		} else {
			//
			// We shall need a host, default to ::1
			if (prs.values [host] == NULL) {
				prs.values [host] = "::1";
			}
			//
			// Parse the socket address and perhaps report errors
			if (!socket_parse (
					prs.values [host],
					prs.values [port],
					&front2back [i])) {
				fprintf (stderr, "Cannot parse \"%s %s %s %s\"\n",
					grammar.keywords [host], prs.values [host],
					grammar.keywords [port], prs.values [port]);
				exit (1);
			}
			log_debug ("Parsed \"%s %s %s %s\" into socket addres [%i] of family %d", grammar.keywords [host], prs.values [host], grammar.keywords [port], prs.values [port], i, front2back [i].ss_family);
		}
	}
	//
	//TODO// Forge the old-style cmdline
	char *argvsub [] = { argv [0],
			prs.VAL_HOST_LISTEN, prs.VAL_PORT_LISTEN,
			prs.VAL_HOST_MEMBER, prs.VAL_PORT_MEMBER,
			NULL };
	int argcsub = 5;
	//
	// Invoke the daemon subprogram and never return
	exit (daemonsub (&prs, front2back, argcsub, argvsub));
	return 1;
}


/** @} */

