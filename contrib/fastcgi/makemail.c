/* makemail.c -- FastCGI program to generate constrained email addresses.
 *
 * This program outputs a web form that constructs email addresses.
 * The email addresses form Signed ARPA2 Identities, are generated
 * with a2id_sign() and may be verified on arrival with a2id_verify().
 * Since this uses the mail server's secret, this takes the form of
 * a FastCGI program.  Once started as a daemon, it can produce the
 * desired information.
 *
 * The default output is text/html with a form that cycles back to
 * this same program.  It assumes Access Control via a web frontend.
 * Future versions may also produce JSON output for interactive
 * user interfaces, if so desired.  Future versions might be called
 * from a commandline.
 *
 * The input to the program is in the form of HTTP parameters:
 *
 * `?mailbox=userid` defines the incoming mailbox (possibly append `@domain.name`) to use instead of `REMOTE_USER` **TODO**
 * `&sender=domain.name` restricts the sender address (possibly prepend `userid@`)
 * `&days=30` for a number of days from now
 * `&topic=news` for things like a `[news]` topic in email **TODO**
 * `&subject=How+to+learn+the+quickstep` for a human email topic **TODO**
 * `&sessionid=az8123kz90` with a session identifier for the communication **TODO**
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl>
 */


#include <fcgi_stdio.h>

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <ctype.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#undef  LOG_STYLE
#define LOG_STYLE LOG_STYLE_SYSLOG

#include <arpa2/identity.h>
#include <arpa2/except.h>


/* Structure of parameters, can be initialised to zeroes because
 * signature flags are set here as well.
 */
typedef struct reqdata {
	uint32_t sigflags, expireday;
	char *daystr;
	a2id_t sender;
	char *senderstr;
} reqdata_t;


/* Parse a hexadecimal character to its value; return -1 if bad.
 */
int hex2value (char inchar) {
	if ((inchar >= '0') && (inchar <= '9')) {
		return inchar - '0';
	} else {
		inchar &= 0xdf;
		if ((inchar >= 'A') && (inchar <= 'F')) {
			return inchar - 'A' + 10;
		} else {
			return -1;
		}
	}
}


/* Parse the request_string to fill reqdata_t.  This will
 * overwrite characters in the query string, without making
 * it longer.
 */
bool parse_querystring (char *qstr, reqdata_t *out) {
	while (*qstr != '\0') {
		/* Find skip length (or the initial parameter length) */
		unsigned skiplen = 0;
		int atpos = -1;
		while ((qstr [skiplen] != '\0') && (qstr [skiplen] != '&')) {
			skiplen++;
		}
		/* Process %xx escape characters to ASCII characters */
		unsigned parmlen = 0;
		unsigned rdpos = 0;
		while (rdpos < skiplen) {
			int ch;
			if (qstr [rdpos] == '%') {
				int ch1 = hex2value (qstr [rdpos+1]);
				int ch2 = hex2value (qstr [rdpos+2]);
				ch  = (ch1 << 4) | ch2;   /* maybe negative */
				if ((ch < 0x20) || (ch >= 0x7f)) {
					log_error ("Funny hex escape %%%c%c", qstr [rdpos+1], qstr [rdpos+2]);
					return false;
				}
				rdpos += 3;
			} else {
				ch = qstr [rdpos++];
			}
			/* Find the position of '@' if any */
			if (ch == '@') {
				if (atpos != -1) {
					log_error ("Funny use of multiple @ in parameter");
					return false;
				}
				atpos = parmlen;
			}
			qstr [parmlen++] = ch;
		}
		/* Also skip any '&' found, then NUL-terminate the parameter */
		if (qstr [skiplen] == '&') {
			skiplen++;
		}
		qstr [parmlen] = '\0';
		/* Recognise parameter and handle appropriately */
		if (memcmp (qstr, "sender=", 7) == 0) {
			/* Parse sender domain or user@identity */
			char *pstr = qstr;
			unsigned plen = parmlen;
			if (atpos == -1) {
				/* Parse "sender@" in a2id_parse_remote() */
				qstr [6] = '@';
			} else {
				/* Skip beyond "sender=" */
				pstr += 7;
				plen -= 7;
			}
			if ((pstr == qstr) && (plen == 7)) {
				/* Quietly ignore empty string */
				out->senderstr = NULL;
			} else if (a2id_parse_remote (&out->sender, pstr, plen)) {
				out->senderstr = qstr + 7;
				out->sigflags |= A2ID_SIGFLAG_REMOTE_DOMAIN;
				if (atpos != -1) {
					out->sigflags |= A2ID_SIGFLAG_REMOTE_USERID;
				}
			} else {
				log_error ("Funny sender \"%.*s\", use domain.name or userid@domain.name", plen, pstr);
				return false;
			}
		} else if (memcmp (qstr, "days=", 5) == 0) {
			/* Parse integer number of days */
			char *endptr;
			unsigned long days_value =
				strtoul (qstr + 5, &endptr, 10);
			if (endptr != qstr + parmlen) {
				log_error ("Funny days, must be an unsigned decimal integer");
				return false;
			}
			out->daystr = qstr + 5;
			out->expireday = days_value;
			if (days_value != (unsigned long) out->expireday) {
				log_error ("Funny days, out of 32-bit range");
				return false;
			}
			if (days_value != 0) {
				out->sigflags |= A2ID_SIGFLAG_EXPIRATION;
			}
		} else {
			/* Quietly ignore unknown parameter */
			;
		}
		/* Skip beyond the current parameter */
		qstr += skiplen;
	}
	return true;
}


/* Process a request.  Check the input and produce an error or
 * a user/pass combination for the requesting host.
 */
bool process_request (void) {
	bool ok = true;
	//
	// Retrieve envvars
	char *http_accept  = getenv ("HTTP_ACCEPT");
	char *query_string = getenv ("QUERY_STRING");
	char *remote_user  = getenv ("REMOTE_USER");
	//
	// Parse the query_string
	reqdata_t rd;
	memset (&rd, 0, sizeof (rd));
	if (query_string == NULL) {
		/* Quietly accept missing QUERY_STRING variable */
		query_string = "";
	} else if (!parse_querystring (query_string, &rd)) {
		memset (&rd, 0, sizeof (rd));
		log_error ("Failed to parse query string");
		ok = false;
	}
	//
	// Setup default sigflags if nothing was selected
	if (rd.sigflags == 0) {
		rd.expireday = 30;
		rd.daystr = "30";
		rd.sigflags |= A2ID_SIGFLAG_EXPIRATION;
	}
	//
	// Parse the REMOTE_USER as an ARPA2 Identity
	a2id_t rcpt;
	if (remote_user == NULL) {
		log_error ("Missing REMOTE_USER variable");
		ok = false;
	} else if (!a2id_parse (&rcpt, remote_user, 0)) {
		log_errno ("Failed to parse REMOTE_USER \"%s\" as ARPA2 Identity", remote_user);
		ok = false;
	}
	//
	// Parse the content type somewhat (ignoring ;parameters and more)
	bool json = false;
	if (http_accept == NULL) {
		log_error ("Missing HTTP_ACCEPT variable");
		ok = false;
	} else if (strstr (http_accept, "text/html") != NULL) {
		json = false;
	} else if (strstr (http_accept, "application/json") != NULL) {
		json = true;
	} else if (strstr (http_accept, "text/*") != NULL) {
		json = false;
	} else {
		log_error ("HTTP_ACCEPT \"%s\" should contain \"text/*\", \"text/html\" or \"application/json\"", http_accept);
		ok = false;
	}
	//
	// Produce a signed email address based on rd
	if (ok) {
		rcpt.expiration = 0;
		rcpt.expireday  = rd.expireday;
		rcpt.sigflags   = rd.sigflags;
		if (!a2id_sign (&rcpt, a2id_sigdata_base, &rd.sender)) {
			log_errno ("Failed to sign email address");
		}
	}
	//
	// Send an error status code when not ok
	if (!ok) {
		FCGI_SetExitStatus (500);
	}
	//
	// Produce the output in JSON or HTML form
	printf ("Content-type: %s; charset=UTF-8\n", json ? "application/json" : "text/html");
	printf ("\n");
	if (!ok) {
		printf (json ? "null\n" : "<html>\n<head><title>Internal Server Error</title></head>\n<body>\n<h1>Internal Server Error</h1>\n<p>The program received illegal arguments, or required arguments were missing.\nAsk the server administrator to look into the respective log files.</p>\n</body>\n</html>\n");
	} else if (json) {
		printf ("{\n");
		char *comma = "";
		if (rd.daystr != NULL) {
			printf ("%s   \"days\": %s", comma, rd.daystr);
			comma = ",\n";
		}
		if (rd.senderstr != NULL) {
			printf ("%s   \"sender\": \"%s\"", comma, rd.senderstr);
			comma = ",\n";
		}
		printf ("%s   \"recipient\": \"%s\"\n}\n", comma, rcpt.txt);
	} else {
		printf ("<html>\n<head><title>Constrained email address generator (makemail)</title></head>\n<body>\n<h1>makemail: Constrained addresses</h1>\n");
		printf ("<p><em>The address below is only accepted if it is used under certain constraints.  You can copy the address to your clipboard.  Or you can edit parameters and click to ask the <code>makemail</code> microservice to update the address.</em></p>\n");
		printf ("<p><code>%s</code><br/>\n<button onclick=\"navigator.clipboard.writeText ('%s')\"> Copy &gt;&gt; </button><noscript><em> but that requires JavaScript</em></noscript><br/>\n<small><em>That's your email address for constrained use.</em></small></p>\n", rcpt.txt, rcpt.txt);
		printf ("<form method=\"GET\">\n");
		printf ("<p><input type=\"text\" name=\"sender\" value=\"%s\"><br/>\n<small><em>Set to constrain the address to either a sender <code>domain.name</code> or <code>userid@domain.name</code></em></small></p>\n", (rd.senderstr != NULL) ? rd.senderstr : "");
		printf ("<p><input type=\"text\" name=\"days\" value=\"%s\"><br/><small><em>Set to constrain the number of days in which this email address expires</em></small></p>\n", (rd.daystr != NULL) ? rd.daystr : "30");
		printf ("<p><input type=\"submit\" value=\" makemail &gt;&gt; \"><br/>\n<small><em>Click here to retrieve a new address for those constraints.</em></small></p>");
		printf ("</form>\n<br/><br/>");
		printf ("<p><b>Notes:</b></p><ul>\n");
		printf ("<li><em>If you make a query for the same constraints on the same day, you should find the same email address for yourself.  The addresses are not stored, but merely produced via a deterministic procedure.</em></li>\n");
		printf ("<li><em>These codes are based on a key stored on the mail server, which is used to validate the signatures upon arrival.  This <code>makemail</code> microservice runs on the mail server so the key does not have to leave the mail server.</em></li>\n");
		printf ("<li><em>The <code>makemail</code> microservice can be called to produce <code>application/json</code> for more interactive interfaces.  You still need to login with the recipient address, for privacy protection.  Authentication with single-signon will give the best user experience.</li>\n");
		printf ("</ul><br/><br/><p>This design follows the ideas of the <a href=\"http://internetwide.org/tag/architecture.html\">InternetWide Architecture</a>.  It is embedded in the <a href=\"https://gitlab.com/arpa2/AxeSMTP\">AxeSMTP</a> software package.</p>\n");
		printf ("</body>\n</html>\n");
	}
	//
	// Return success or failure
	return ok;
}


/* Main routine.  Accept request(s) and process each.
 * Before starting, this loads the mail server key from stdin.
 */
int main (int argc, char *argv []) {
	//
	// Try to open &3 or if it is absent try the default key file
	int kf = 3;
	struct stat st;
	if (fstat (kf, &st) < 0) {
		kf = open ("/var/lib/arpa2/identity/default.key", O_RDONLY);
	}
	if (kf >= 0) {
		if (!a2id_addkey (kf)) {
			log_errno ("Failed to load key file from &3 or default.key");
			exit (1);
		}
		close (kf);
	}
	//
	// Fetch the/a request
	while (FCGI_Accept () >= 0) {
		if (!process_request ()) {
			FCGI_SetExitStatus (500);
		}
	}
	//
	// Cleanup and return
	a2id_dropkeys ();
	exit (0);
}
