/* formmail.c -- FastCGI program for authenticated-sender mail.
 *
 * This program can handle input from a web form to construct email.
 * The program compiles to a binary named arpa2webmail.fcgi which
 * assumes an authenticated sender (such as via HTTP-SASL) and helps
 * to impose Access Control via ARPA2 Signed Identities.
 *
 * The input to the script is in the form of query parameters:
 * 
 * `?mailbox=userid` defines the destination mailbox without @domain.name
 * `&access=AB123LABC` recipient access code (added between + signs)
 * `&subject=Tral+la+la` for the email subject
 * `&topic=tralala` to prefix `[tralala]` to the subject
 * `&body=tra\nla\na\n` with the body (in text/plain; utf-8)
 *
 * The MAIL FROM header is filled with the authenticated identity
 * as found in a REMOTE_USER variable.  The RCPT TO domain as well
 * as the EHLO domain are taken from an ARPA2_DOMAIN variable.
 * Both must be sent by the web server as FastCGI variables.
 *
 * The recipient access code, if supplied, forms an ARPA2 Signed Identity
 * that can be required by ARPA2 Access Control.  This allows such things
 * as expiration days and restrictions on the sender domain or local-part.
 * The topic and subject may be constrained in future versions of AxeSMTP.
 * Topics may also be helpful to direct an email to a proper person.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl>
 */


#include <fcgi_stdio.h>

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <ctype.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#undef  LOG_STYLE
#define LOG_STYLE LOG_STYLE_SYSLOG

#include <arpa2/identity.h>
#include <arpa2/except.h>



/* Structure of parameters and variables, parsed with input validation.
 */
typedef struct reqdata {
	//
	// FastCGI variables, passed as environment variables
	char *cgivar_content_type;
	char *cgivar_query_string;
	char *cgivar_remote_user;
	char *cgivar_arpa2_domain;
	char *cgivar_server_name;
	//
	// Query string parameters, passed 
	char *param_mailbox;
	char *param_access;
	char *param_subject;
	char *param_topic;
	char *param_body;
	int paramlen_body;
	//
	// ARPA2 Identities for sender and recipient
	a2id_t sender;
	a2id_t recipient;
	//
	// MTA socket (or -1)
	int mta;
} reqdata_t;


/* Release a simple error string to the user, and add timing.
 * Return false in support of tail recursion.
 */
bool error (char *errstr) {
	time_t now_epoch = 0;
	struct tm now_tm;
	char now [101];
	time (&now_epoch);
	localtime_r (&now_epoch, &now_tm);
	memset (now, 0, sizeof (now));
	strftime (now, 100, "%a %F around %T", &now_tm);
	printf ("{\n  \"ok\": false,\n  \"error\": \"%s\",\n  \"time\": \"%s\"\n}\n", errstr, now);
	return false;
}


/* Release a Configuration Error to the user.
 * Return false in support of tail recursion.
 */
bool configuration_error (void) {
	return error ("Configuration Error (the webmaster may help if you tell them the time)");
}


/* Send a string and return true of success or false on failure.
 *
 * Upon failure, log an error and print one for the HTTP user.
 * This causes any further handling to be unneeded; output is done.
 */
bool mtasend (reqdata_t *rq, char *data, int datalen) {
	char *errstr = NULL;
	ssize_t written = write (rq->mta, data, datalen);
	if (written == datalen) {
		return true;
	} else if (written < 0) {
		log_errno ("Error writing to MTA: %.*s", datalen, data);
		errstr = "Failed to write to MTA";
	} else {
		log_error ("Partial write to MTA, %d out of %d", (int) written, datalen);
		errstr = "Partial write to MTA";
	}
	return error (errstr);
}


/* Receive from the MTA, expecting it to have a positive status.
 *
 * On success, return true.  Upon failure, log an error and
 * print one for the HTTP user.
 */
bool mtarecv (reqdata_t *rq, char *cmd, unsigned cmdlen) {
	char buf [1000+1];
	char *errstr = NULL;
	//
	// Receive the response
	ssize_t rdlen;
	rdlen = read (rq->mta, buf, sizeof (buf));
	if (rdlen < 0) {
		if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
			log_errno ("Timed out reading from MTA: %.*s", cmdlen, cmd);
			errstr = "Timed out reading from MTA";
		} else {
			log_errno ("Error reading from MTA: %.*s", cmdlen, cmd);
			errstr = "Partial read from MTA";
		}
		goto fail;
	} else if (rdlen == 0) {
		log_error ("Connection closed by MTA: %.*s", cmdlen, cmd);
		errstr = "Connection closed by MTA";
		goto fail;
	}
	buf [rdlen] = '\0';
	//
	// Skip intermediate response lines
	char *rdptr = buf;
	while ((rdlen >= 5) && (rdptr [3] == '-')) {
		char *newline = strchr (buf, '\n');
		if (newline == NULL) {
			newline = &buf [rdlen];
		} else {
			newline++;
		}
		rdlen -= (newline - rdptr);
		rdptr = newline; 
	}
	//
	// Require a complete response (even if suppressed by our buffer size)
	if (rdlen < 5) {
		log_error ("Incomplete response from MTA: %.*s", cmdlen, cmd);
		errstr = "Incomplete response from MTA";
		goto fail;
	}
	//
	// Classify the response code and switch behaviour
	switch (*rdptr) {
	case '2':
		//
		// Positive completion; continue
		break;
	case '3':
		//
		// Intermediate completion; continue
		break;
	case '4':
		//
		// Transient negative
		/* continue into the '5' case */;
	case '5':
		//
		// Permanent negative
		errstr = rdptr;
		char *newline = strchr (rdptr, '\n');
		if (newline != NULL) {
			*newline = '\0';
		}
		goto fail;
	default:
		log_error ("Unexpected MTA response \"%.*s\"", (int) rdlen, rdptr);
		return configuration_error ();
	}
	//
	// We had a success with our command
	return true;
fail:
	return error (errstr);
}


/* Transmit an SMTP command and collect the response.  If this is an
 * error code, we print it and return false ("no more").  When the
 * last SMTP command returns successfully, the default return value
 * would be produced by the request handler.
 */
bool smtp (reqdata_t *rq, char *cmd, unsigned cmdlen) {
	char *errstr = NULL;
	//
	// Write the command
	ssize_t written = write (rq->mta, cmd, cmdlen);
	if (written < 0) {
		if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
			log_errno ("Timed out writing to MTA: %.*s", cmdlen, cmd);
			errstr = "Timed out writing to MTA";
		} else {
			log_errno ("Error writing to MTA: %.*s", cmdlen, cmd);
			errstr = "Failed to write to MTA";
		}
		goto fail;
	} else if (written != cmdlen) {
		log_error ("Partial write to MTA, %d out of %d", (int) written, cmdlen);
		errstr = "Partial write to MTA";
		goto fail;
	}
	//
	// Written the command, hope for a positive response
	return mtarecv (rq, cmd, cmdlen);
	//
	// Return failure, as stated in errstr
fail:
	return error (errstr);
}


/* Await the welcome message from the MTA after connecting to it.
 *
 * Return true on success, false with an error otherwise.
 */
bool smtp_connected (reqdata_t *rq) {
	static char descr [] = "Connection welcome";
	return mtarecv (rq, descr, sizeof (descr) - 1);
}


/* Send the EHLO command, retrieving $ARPA2_DOMAIN as the sender domain
 *
 * Returns true for "continue" or false for "error reported, stop".
 */
bool smtp_ehlo (reqdata_t *rq) {
	char cmd [1000+1];
	int cmdlen;
	//
	// Produce the command to send
	cmdlen = snprintf (cmd, 1000, "EHLO %s\r\n", rq->cgivar_arpa2_domain);
	//
	// Verify that the command is not too long
	if (cmdlen > 1000) {
		log_error ("Refusing to send EHLO command in excess of 1000 bytes");
		return configuration_error ();
	}
	//
	// Pass the command into general SMTP handling
	return smtp (rq, cmd, cmdlen);
}


/* Send the MAIL FROM command, forming the address like this:
 *  - $REMOTE_USER is supplied from web server authentication
 *  - if no '@' is contained, an error is returned
 *
 * Returns true for "continue" or false for "error reported, stop".
 */
bool smtp_mailfrom (reqdata_t *rq) {
	char cmd [1000+2];
	int cmdlen;
	//
	// Produce the command to send
	cmdlen = snprintf (cmd, 1000+1, "MAIL FROM:<%s>\r\n", rq->cgivar_remote_user);
	//
	// Verify that the command is not too long
	if (cmdlen > 1000) {
		log_error ("Refusing to send MAIL FROM command in excess of 1000 bytes");
		return configuration_error ();
	}
	//
	// Pass the command into general SMTP handling
	return smtp (rq, cmd, cmdlen);
}


/* Send the RCPT TO command, forming the address like this:
 *  - mailbox forms the start of the address
 *  - +access+ follows if it is given
 *  - @ to separate local-part from domain
 *  - $ARPA2_DOMAIN taken is fixated via web server configuration
 *
 * Returns true for "continue" or false for "error reported, stop".
 */
bool smtp_rcptto (reqdata_t *rq) {
	char cmd [1000+2];
	int cmdlen;
	//
	// Produce the command to send
	cmdlen = snprintf (cmd, 1000+1, "RCPT TO:<%s>\r\n", rq->recipient.txt);
	//
	// Verify that the command is not too long
	if (cmdlen > 1000) {
		log_error ("Refusing to send RCPT TO command in excess of 1000 bytes");
		return configuration_error ();
	}
	//
	// Pass the command into general SMTP handling
	return smtp (rq, cmd, cmdlen);
}


/* Prepare the DATA command, to prepare the MTA for the email.
 *
 * Returns true for "continue" or false for "error reported, stop".
 */
bool smtp_data (reqdata_t *rq) {
	char cmd [1000+1];
	int cmdlen;
	//
	// Produce the command to send
	cmdlen = snprintf (cmd, 1000, "DATA\r\n");
	//
	// Pass the command into general SMTP handling
	return smtp (rq, cmd, cmdlen);
}


/* Send the email haeders.
 *
 * Returns true for "continue" or false for "error reported, stop".
 */
bool smtp_datahead (reqdata_t *rq) {
	char head [4096+2];
	int headlen;
	//
	// Retrieve the local hostname
	char hostname [512];
	memset (hostname, 0, sizeof (hostname));
	if (gethostname (hostname, sizeof (hostname)) != 0) {
		log_errno ("Failed to obtain mailer hostname");
		return configuration_error ();
	}
	//
	// Produce the headers to send
	bool has_topic = (rq->param_topic != NULL);
	headlen = snprintf (head, 4096+1,
				"Received: from %s by %s for %s\r\n"
				"From: WebForm Mailer <%s>\r\n"
				"To: <%s>\r\n"
				"Subject: %s%s%s%s\r\n"
				"\r\n",
				rq->cgivar_server_name,
				hostname,
				rq->cgivar_arpa2_domain,
				rq->cgivar_remote_user,
				rq->recipient.txt,
				has_topic ? "["             : "",
				has_topic ? rq->param_topic : "",
				has_topic ? "] "            : "",
				rq->param_subject);
	//
	// Verify that the command is not too long
	if (headlen > 4096) {
		log_error ("Refusing to send email header command in excess of 4096 bytes");
		return configuration_error ();
	}
	//
	// Pass the command into general SMTP handling
	return mtasend (rq, head, headlen);
}


/* Send the email body, doubling dots at the start of a line.
 * Also skip any '\r' characters but precede '\n' with '\r' to
 * have more robust handling for email-styled newlines.
 *
 * Returns true for "continue" or false for "error reported, stop".
 */
bool smtp_databody (reqdata_t *rq) {
	//
	// Pickup the request body to send
	char *body = rq->param_body;
	int bodylen = rq->paramlen_body;
	//
	// Loop sending lines of text
	while (bodylen > 0) {
		//
		// Prefix a dot if this is the start of the body
		if (*body == '.') {
			if (!mtasend (rq, ".", 1)) {
				return false;
			}
		}
		//
		// Find the end of the line
		char *newline = memchr (body, '\n', bodylen);
		char *retline = memchr (body, '\r', bodylen);
		if (retline > newline) {
			retline = NULL;
		} else if (newline > retline + 1) {
			newline = NULL;
		}
		//
		// Set lengths and optional trailing characters to send
		unsigned sendlen;
		unsigned skiplen;
		bool append_crlf;
		if (retline == NULL) {
			if (newline == NULL) {
				//
				// Last chunk, without line ending
				sendlen = bodylen;
				skiplen = bodylen;
				append_crlf = true;
			} else {
				//
				// Line ends in LF without CR
				sendlen = (int) (newline - body);
				skiplen = sendlen + 1;
				append_crlf = true;
			}
		} else {
			if (newline == NULL) {
				//
				// Ignore lone CR that is without LF following
				sendlen = (int) (retline - body);
				skiplen = sendlen + 1;
				append_crlf = false;
			} else {
				//
				// Proper CRLF sequence marks the line end
				sendlen = (int) (newline + 1 - body);
				skiplen = sendlen;
				append_crlf = false;
			}
		}
		//
		// Send the data line and, if so desired, add CRLF
		if (!mtasend (rq, body, sendlen)) {
			return false;
		}
		if (append_crlf && !mtasend (rq, "\r\n", 2)) {
			return false;
		}
		//
		// Update body sending progress
		body    += sendlen;
		bodylen -= skiplen;
	}
	//
	// Success
	return true;
}


/* Finish the DATA command with a one-liner dot.
 *
 * Returns true for "continue" or false for "error reported, stop".
 */
bool smtp_datadot (reqdata_t *rq) {
	char cmd [1000+1];
	int cmdlen;
	//
	// Produce the command to send
	cmdlen = snprintf (cmd, 1000, ".\r\n");
	//
	// Pass the command into general SMTP handling
	return smtp (rq, cmd, cmdlen);
}


/* Send the QUIT command, for proper close-down of the MTA connection.
 *
 * Returns true for "continue" or false for "error reported, stop".
 */
bool smtp_quit (reqdata_t *rq) {
	char cmd [1000+1];
	int cmdlen;
	//
	// Produce the command to send
	cmdlen = snprintf (cmd, 1000, "QUIT\r\n");
	//
	// Pass the command into general SMTP handling
	return smtp (rq, cmd, cmdlen);
}


/* Pass through the rites of SMTP submission, based on input data.
 * The MTA is assumed to listen on address ::1 port 25.
 * No TLS will be used, as this connection is made locally.
 * Running web plugins near services is a dramatic simplification :)
 */
bool smtp_exchange (reqdata_t *rq) {
	char *errstr = NULL;
	//
	// Allocate an MTA socket
	rq->mta = socket (AF_INET6, SOCK_STREAM, 0);
	if (rq->mta < 0) {
		errstr = "Could not allocate MTA connection socket";
		goto fail_errno;
	}
	//
	// Set timeout values for send and receive
	static const struct timeval tout = { .tv_sec = 30, .tv_usec = 0 };
	if (setsockopt (rq->mta, SOL_SOCKET, SO_SNDTIMEO, &tout, sizeof (tout)) < 0) {
		errstr = "Failed to set send timeout for MTA socket";
		goto fail_errno;
	}
	if (setsockopt (rq->mta, SOL_SOCKET, SO_RCVTIMEO, &tout, sizeof (tout)) < 0) {
		errstr = "Failed to set receive timeout for MTA socket";
		goto fail_errno;
	}
	//
	// Connect to ::1 port 25
	struct sockaddr_in6 sin6;
	memset (&sin6, 0, sizeof (sin6));
	sin6.sin6_family = AF_INET6;
	sin6.sin6_port = htons (25);
	sin6.sin6_addr.s6_addr [15] = 0x01;
	if (connect (rq->mta, (struct sockaddr *) &sin6, sizeof (sin6)) < 0) {
		errstr = "Failed to connect to MTA";
		goto fail_errno;
	}
	//
	// Await the welcoming message
	if (!smtp_connected (rq)) {
		goto failed;
	}
	//
	// Send the EHLO command
	if (!smtp_ehlo (rq)) {
		goto failed;
	}
	// Send the MAIL FROM command
	if (!smtp_mailfrom (rq)) {
		goto failed;
	}
	//
	// Send the RCPT TO command
	if (!smtp_rcptto (rq)) {
		goto failed;
	}
	//
	// Send the DATA command (setup the MTA for the email)
	if (!smtp_data (rq)) {
		goto failed;
	}
	//
	// Send the actual email
	if (!smtp_datahead (rq)) {
		goto failed;
	}
	if (!smtp_databody (rq)) {
		goto failed;
	}
	//
	// Finish the DATA command (with a single-dotted line)
	if (!smtp_datadot (rq)) {
		goto failed;
	}
	//
	// Send the QUIT command (ignoring any errors)
	(void) smtp_quit (rq);
	//
	// Close the MTA socket
	close (rq->mta);
	rq->mta = -1;
	//
	// Report success (fallthrough default response)
	printf ("{\n  \"ok\": true\n}\n");
	return true;
	//
	// Report errors and close down
fail_errno:
	log_errno ("%s", errstr);
	(void) error (errstr);
failed:
	if (rq->mta >= 0) {
		close (rq->mta);
		rq->mta = -1;
	}
	return false;
}


/* Parse a hexadecimal character to its value; return -1 if bad.
 */
int hex2value (char inchar) {
	if ((inchar >= '0') && (inchar <= '9')) {
		return inchar - '0';
	} else {
		inchar &= 0xdf;
		if ((inchar >= 'A') && (inchar <= 'F')) {
			return inchar - 'A' + 10;
		} else {
			return -1;
		}
	}
}


/* Parse the request_string to fill reqdata_t.  This will
 * overwrite characters in the query string, without making
 * it longer.
 */
bool parse_querystring (reqdata_t *rq) {
	//
	// Setup for parsing the query string
	char *qstr = rq->cgivar_query_string;
	bool miss_mailbox = true;
	bool miss_subject = true;
	bool miss_body    = true;
	//
	// Iterate over parameters in the query string;
	// reduce their format to normal ASCII strings
	while (*qstr != '\0') {
		/* Find skip length (or the initial parameter length) */
		unsigned skiplen = 0;
		int atpos = -1;
		while ((qstr [skiplen] != '\0') && (qstr [skiplen] != '&')) {
			skiplen++;
		}
		/* Process %xx escape and + characters to ASCII characters */
		unsigned parmlen = 0;
		unsigned rdpos = 0;
		while (rdpos < skiplen) {
			int ch;
			if (qstr [rdpos] == '%') {
				int ch1 = hex2value (qstr [rdpos+1]);
				int ch2 = hex2value (qstr [rdpos+2]);
				ch  = (ch1 << 4) | ch2;   /* maybe negative */
				if ((ch < 0x20) || (ch >= 0x7f)) {
					log_error ("Funny hex escape %%%c%c", qstr [rdpos+1], qstr [rdpos+2]);
					goto fail;
				}
				rdpos += 3;
			} else {
				if (qstr [rdpos] == '+') {
					qstr [rdpos] = ' ';
				}
				ch = qstr [rdpos++];
			}
			/* Find the position of '@' if any */
			if (ch == '@') {
				if (atpos != -1) {
					log_error ("Funny use of multiple @ in parameter");
					goto fail;
				}
				atpos = parmlen;
			}
			qstr [parmlen++] = ch;
		}
		/* Also skip any '&' found, then NUL-terminate the parameter */
		if (qstr [skiplen] == '&') {
			skiplen++;
		}
		qstr [parmlen] = '\0';
		/* Recognise parameter and handle appropriately */
		if (memcmp (qstr, "mailbox=", 8) == 0) {
			if (strchr (qstr + 8, '@') != NULL) {
				return error ("The mailbox must not have an @domain.name part");
			}
			rq->param_mailbox = qstr + 8;
			miss_mailbox = false;
		} else if (memcmp (qstr, "access=", 7) == 0) {
			if (parmlen < 7 + 15) {
				return error ("The access parameter is too short");
			}
			for (char *qp = qstr + 7; qp < qstr + parmlen; qp++) {
				char c = *qp;
				if ((c >= '0') && (c <= '9')) {
					continue;
				}
				if ((c >= 'A') && (c <= 'Z')) {
					continue;
				}
				return error ("The access parameter must be composed of digits and uppercase letters");
			}
			rq->param_access = qstr + 7;
		} else if (memcmp (qstr, "subject=", 8) == 0) {
			rq->param_subject = qstr + 8;
			miss_subject = false;
		} else if (memcmp (qstr, "topic=", 6) == 0) {
			rq->param_topic = qstr + 6;
		} else if (memcmp (qstr, "body=", 5) == 0) {
			rq->param_body = qstr + 5;
			rq->paramlen_body = parmlen - 5;
			miss_body = false;
		} else {
			/* Quietly ignore unknown parameter */
			;
		}
		/* Skip beyond the current parameter */
		qstr += skiplen;
	}
	//
	// Test that all required parameter were supplied
	if (miss_mailbox || miss_subject || miss_body) {
		return error ("Some required parameters missing; need ?mailbox=...&subject=...&body=...");
	}
	return true;
fail:
	return error ("Invalid query string");
}


/* Retrieve an environment variable, complaining if it is required.
 *
 * Return false on error, after having reported it already.
 */
bool load_fcgivar (char **outvar, char *name, bool require) {
	*outvar = getenv (name);
	if (require && (*outvar == NULL)) {
		log_error ("Missing required FastCGI variable %s", name);
		return configuration_error ();
	}
	return true;
}


/* Process a request.  Check the input and produce an error or
 * a user/pass combination for the requesting host.
 */
bool process_request (void) {
	bool ok = true;
	//
	// Produce the HTTP headers (we will at least produce an error)
	printf ("Content-Type: application/json; charset=UTF-8\n");
	printf ("\n");
	//
	// Initialise the request data structure
	reqdata_t rq;
	memset (&rq, 0, sizeof (rq));
	rq.mta = -1;
	//
	// Retrieve FastCGI parameters
	ok = ok && load_fcgivar (&rq.cgivar_content_type, "CONTENT_TYPE", false);
	ok = ok && load_fcgivar (&rq.cgivar_query_string, "QUERY_STRING", true );
	ok = ok && load_fcgivar (&rq.cgivar_remote_user,  "REMOTE_USER",  true );
	ok = ok && load_fcgivar (&rq.cgivar_server_name,  "SERVER_NAME",  true );
	ok = ok && load_fcgivar (&rq.cgivar_arpa2_domain, "ARPA2_DOMAIN", true );
	//
	// Sanity checks of grammar in FastCGI parameters
	if (ok && (strchr (rq.cgivar_arpa2_domain, '@') != NULL)) {
		log_error ("Found an @ in FastCGI-variable ARPA2_DOMAIN \"%s\"", rq.cgivar_arpa2_domain);
		return configuration_error ();
	}
	if (ok && (strchr (rq.cgivar_remote_user, '@') == NULL)) {
		log_error ("Found no @ in FastCGI-variable REMOTE_USER \"%s\"", rq.cgivar_remote_user);
		return configuration_error ();
	}
	//
	// Parse the query_string
	ok = ok && parse_querystring (&rq);
	//
	// Combine the fields of the recipient (too long will make it not parse below)
	char rcpt [A2ID_MAXLEN+4];
	int rcptlen;
	if (rq.param_access != NULL) {
		rcptlen = snprintf (rcpt, A2ID_MAXLEN+3,
					"%s+%s+@%s",
					rq.param_mailbox,
					rq.param_access,
					rq.cgivar_arpa2_domain);
	} else {
		rcptlen = snprintf (rcpt, A2ID_MAXLEN+3,
					"%s@%s",
					rq.param_mailbox,
					rq.cgivar_arpa2_domain);
	}
	//
	// Parse the formed rcpt as an ARPA2 Identity for the mail recipient
	if (ok && !a2id_parse (&rq.recipient, rcpt, rcptlen)) {
		log_errno ("Failed to parse recipient\"%.*s\" as an ARPA2 Identity", rcptlen, rcpt);
		ok = error ("Recipient was not recognised as an ARPA2 Identity");
	}
	//
	// Parse the REMOTE_USER as an ARPA2 Remote Identity for the mail sender
	if (ok && !a2id_parse_remote (&rq.sender, rq.cgivar_remote_user, 0)) {
		log_errno ("Failed to parse REMOTE_USER \"%s\" as ARPA2 Remote Identity", rq.cgivar_remote_user);
		ok = error ("Your identity was not recognised as an ARPA2 Remote Identity");
	}
	//
	// Engage in the requested SMTP exchange
	ok = ok && smtp_exchange (&rq);
	//
	// Return success or failure
	return ok;
}


/* Main routine.  Accept request(s) and process each.
 * Before starting, this loads the mail server key from stdin.
 */
int main (int argc, char *argv []) {
	//
	// Try to open &3 or if it is absent try the default key file
	int kf = 3;
	struct stat st;
	if (fstat (kf, &st) < 0) {
		kf = open ("/var/lib/arpa2/identity/default.key", O_RDONLY);
	}
	if (kf >= 0) {
		if (!a2id_addkey (kf)) {
			log_errno ("Failed to load key file from &3 or default.key");
			exit (1);
		}
		close (kf);
	}
	//
	// Fetch the/a request
	while (FCGI_Accept () >= 0) {
		if (!process_request ()) {
			FCGI_SetExitStatus (500);
		}
	}
	//
	// Cleanup and return
	a2id_dropkeys ();
	exit (0);
}
