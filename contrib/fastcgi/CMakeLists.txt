
if (CONTRIB_MAKEMAIL_FASTCGI OR CONTRIB_FORMMAIL_FASTCGI)

    find_library (FASTCGI_LIBRARY
        fcgi
        DOC "FastCGI library path"
        REQUIRED)

endif ()

if (CONTRIB_MAKEMAIL_FASTCGI)

    add_executable (makemail.fcgi
        makemail.c)

    # use_com_err_table(kip
    #     TARGETS makemail.fcgi)

    target_link_libraries (makemail.fcgi
        ${FASTCGI_LIBRARY}
        ARPA2::identity)

    install (TARGETS makemail.fcgi
        RUNTIME DESTINATION ${CMAKE_INSTALL_LIBEXECDIR})

endif ()

if (CONTRIB_FORMMAIL_FASTCGI)

    add_executable (formmail.fcgi
	formmail.c)

    target_link_libraries (formmail.fcgi
        ${FASTCGI_LIBRARY}
        ARPA2::identity)

    install (TARGETS formmail.fcgi
        RUNTIME DESTINATION ${CMAKE_INSTALL_LIBEXECDIR})

endif ()
