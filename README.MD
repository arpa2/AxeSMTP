# Axe SMTP: Chopping and Splitting your Mail

> *These filters edit email in transit, by sitting on the SMTP wire.
> This can be used to express a number of highly useful features
> that the Identity and Access models enable.*

The filtering and mapping described below is very useful for email,
but may be applied more widely.  It is likely to also make sense
in XMPP, SIP and presumably much more.


## Axe SMTP: Input filtering

This daemon is called `arpa2axe-in-smtpd`.

  * SMTP pass-through filter
  * Identity Parsing on remote and local/s
  * Signature Verification on local
  * Access Control on (remote,local)
  * Possible NAT.in for local address
  * Possibly decrypt for local

The input filter is an SMTP pass-through filter that works on the
addresses passed in.  Commands are literally passed through, with
some specific edits for addresses.

Identity Parsing is done liberally on the Remote Identity in `MAIL FROM`
and concisely on the Local Identity in `RCPT TO`, `VRFY` and `EXPN`.
The latter are also subjected to signature verification, where the only
reason for abolishment would be present but wrong signatures.  A prudent
message is generated, such as *this mailbox has gone* for expiration.

**Note:** *Signatures may involve elements from the email headers; we
cannot simply support that in a pass-through filter.  Arguably more
important in email is the local/remote combination and expiration.*

Every mention of a Local Identity is used against the Remote Identity
in `communication` access control.  Address translation will be used
when it is called for by what this finds.

**TODO:** *The same address mapping should be applied to headers
as they pass through.*


## Axe SMTP: Output Filtering

This daemon is called `arpa2axe-out-smtpd`.

  * SMTP pass-through filter
  * Identity Parsing on remote/s and local
  * Signature Expansion on local
  * Access Update on (remote,local)
  * Possible NAT.out for local address
  * Possibly encrypt to remote

The output filter is an SMTP pass-through filter that works on the
addresses passed out.  Commands are literally passed through, with
some specific edits for addresses.

In the outgoing direction, the destination address is parsed as the
liberal Remote Identity, whereas the source address is parsed as the
Local Identity.  Access Control is applied on those coordinates, using
the Communication logic, but when it is found that the combination is
not supported an entry is requested from a database update daemon.
When an entry calls for renaming the Local Identity, this will be done.
Signatures are then added when the Local Identity calls for it.

**TODO:** *The same address mapping should be applied to headers
as they pass through.*

**Note:** *The requested sender address may be different per destination,
which also impacts the renaming of headers passing through, so only one
destination address can be used per connection.  Perhaps this means it should
be done internally, so email can be fired off in groups from the queue.*

**Note:** *Signatures may involve elements from the email headers; we
cannot simply support that in a pass-through filter.  Arguably more
important in email is the local/remote combination and expiration.*

## Axe SMTP/LMTP: Group Processing

This daemon is called `arpa2axe-group-smtpd` and/or `arpa2axe-group-lmtpd`.

  * SMTP and/or LMTP pass-through filter
  * Identity Parsing on sender and recipient
  * If recipient is a group:
      - map recipient aliases to group recipients
      - map the sender

This is an SMTP/LMTP pass-through filter, so every delivery address can
fail independently.  It allows multiple outgoing connections for
one incoming address, as required for group logic.

The filter looks for a group ACL for the destination address,
after removing aliases.  Since the domain is incorporated
elsewhere, this means that only the userid is taken into
account.  When no entry is found, the filter quietly relays
the email to its backend, without change.  Otherwise, it gets
clever.

The destination aliases used are now parsed:

  * Entries starting with `.` are negative; these are taken out
    from the complete list, so `+.john` would talk to all
    active members but exclude John (to talk about him, or about
    his birthday present);
  * Other entries are positive; they may add to the complete
    list, by incorporating entries that are normally not
    active, but reachable at a member address in explicit calls.
    Think `+postmaster` or `+archive` aliases;
  * With only positive aliases, those addresses are only used;
    otherwise, the total list is iterated.

Triggers take the shape `^member+user+aliases@domain.name` with a
split at the first `+` because that never occurs; if the domain
matches it could be simplified to `^member+user+aliases` but in
general, triggers map the group `member` to `user+aliases@domain.name`.

When the `user+aliases@domain.name` matches the sender address,
replace it by `group+member@domain.name`, and ideally do that to
email-bearing headers too.  We are not intermediates for DKIM or SPF,
so this ought to be safe.  Note how an unregistered sender, so a
non-member, does not get the privacy support of local group members.
This is necessary to allow responses to their address.

When the `member` matches the destination address, or more precisely
the filter that it imposes, then the corresponding address
`user+aliases@domain.name` will be used as an envelop recipient
address, and relayed over a separate outgoing LMTP or SMTP connection.
External addresses are not matched in a similar manner, they always
get forwarded over a separate outgoing LMTP or SMTP connection.

**TODO:** *How do we mark the default delivery to some users, while
suppressing them from normal list forwarding?  List members should
be offered a choice to be proactive rather than reactive.*

**TODO:** *The same address mapping should be applied to headers
as they pass through.*


## Axe SMTP: OpenPGP encryption

  * Lookup the destination address in a local key ring
  * If it has pushed a PGP key into our domain, use it



## Thanks!

This work was funded by [SIDNfonds](https://www.sidnfonds.nl/)
with public money derived from `.nl` domain name registrations.
(Please note: You need JavaScript enabled if you want to see
anything on their page.)

SIDNfonds project number 173039 included a secondary part,
[ARPA2 Reservoir](https://gitlab.com/arpa2/reservoir),
which is an object store with metadata stored in LDAP, so it can
be searched online, possibly with SASL authentication and access control.

It is a future wish to strip extensions from uploaded files and
store them in Reservoir, while replacing them in the email with
a mere reference.  Among others, this will allow sharing of the
content between groups and bring email back to a message-sending
system that merely links to files/objects kept elsewhere.

Note that the word "cloud" does not appear here.  This is very
deliberately software that users or their hosting providers can
run under their own control.  Your data belongs to you.

